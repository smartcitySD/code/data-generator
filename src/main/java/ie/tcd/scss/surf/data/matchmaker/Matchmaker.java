package ie.tcd.scss.surf.data.matchmaker;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ie.tcd.scss.surf.data.entities.Comparison;
import ie.tcd.scss.surf.data.entities.ComposedStep;
import ie.tcd.scss.surf.data.entities.Domain;
import ie.tcd.scss.surf.data.entities.ParallelStep;
import ie.tcd.scss.surf.data.entities.Plan;
import ie.tcd.scss.surf.data.entities.PlanGraph;
import ie.tcd.scss.surf.data.entities.RequestDescription;
import ie.tcd.scss.surf.data.entities.SequentialStep;
import ie.tcd.scss.surf.data.entities.ServiceDescription;
import ie.tcd.scss.surf.data.entities.ServiceParameter;
import ie.tcd.scss.surf.data.entities.SimpleStep;
import ie.tcd.scss.surf.data.entities.Step;
import ie.tcd.scss.surf.data.entities.Vertex;
import ie.tcd.scss.surf.data.util.Util;
import ie.tcd.scss.surf.data.entities.Edge;

public class Matchmaker {
	private Map<String,OntModel> ontologies = new HashMap<String, OntModel>();
	private ArrayList<String> to_remove = new ArrayList<String>();
	
	public Matchmaker() {
		to_remove.add("output");
		to_remove.add("input");
		to_remove.add("service");
		to_remove.add("of");
		to_remove.add("a");
		to_remove.add("an");
		to_remove.add("at");
		to_remove.add("the");
		to_remove.add("on");
		to_remove.add("in");
		to_remove.add("is");
		to_remove.add("are");
		to_remove.add("within");
		to_remove.add("from");
		String ontologiesPath = "../../Data/ontologies/owls-tc4";
		File folder = new File(ontologiesPath);
		File[] files = folder.listFiles();
		for(int i=0;i<files.length;i++){
			try{
				if(!files[i].getName().equals("protont.owl") && !files[i].getName().equals("protonu.owl") && !files[i].getName().equals("protons.owl") && !files[i].getName().equals("catalog-v001.xml")){
					OntModel ontology = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
					FileInputStream inputStream = new FileInputStream(files[i]);
					ontology.read(inputStream,null);
					ontologies.put(files[i].getName(),ontology);
					
				}
			}catch(Exception ex){
				System.out.println("Corrupted ontology: " + files[i].getName());
				ex.printStackTrace();
			}
		}
	}

	public ArrayList<Plan> backwardPlanning(ArrayList<ServiceDescription> services,RequestDescription request, ArrayList<Plan> currentPlans) {
		ArrayList<Plan> plans = new ArrayList<>();
		if(currentPlans.size()==0){
			ArrayList<ServiceParameter> requestInputs = request.getInputs();
			ArrayList<ServiceParameter> requestOutputs = request.getOutputs();
			for (int i = 0; i < services.size(); i++) {
				ArrayList<ServiceParameter> serviceInputs = services.get(i).getInputs();
				ArrayList<ServiceParameter> serviceOutputs = services.get(i).getOutputs();
				ArrayList<ServiceParameter> comparedOutputs = compareParameters(requestOutputs, serviceOutputs);
				if (comparedOutputs.size() == 0) {
					Plan plan = new Plan();
					plan.setInputs(serviceInputs);
					plan.setOutputs(requestOutputs);
					ArrayList<ServiceParameter> comparedInputs = compareParameters(requestInputs, serviceInputs);
					if (comparedInputs.size() == 0 || comparedInputs.size() < requestInputs.size()) {
						SimpleStep step = new SimpleStep();
						step.setType("SimpleStep");
						step.setInputs(requestInputs);
						step.setOutputs(requestOutputs);
						step.setService(services.get(i));
						step.setOrder(0);
						plan.getSteps().add(step);
						plan.setState(1);
						for(int j=0; j<services.get(i).getDomains().size(); j++){
							if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
								plan.getDomains().add(services.get(i).getDomains().get(j));
						}
						plans.add(plan);
					}
					if (comparedInputs.size() == requestInputs.size()) {
						SequentialStep sequence = new SequentialStep();
						sequence.setType("SequentialStep");
						sequence.setOrder(0);
						sequence.setInputs(serviceInputs);
						sequence.setOutputs(serviceOutputs);
						SimpleStep step = new SimpleStep();
						step.setType("SimpleStep");
						step.setOrder(0);
						step.setInputs(serviceInputs);
						step.setOutputs(serviceOutputs);
						step.setService(services.get(i));
						sequence.getSteps().add(step);
						plan.getSteps().add(sequence);
						plan.setState(0);
						for(int j=0; j<services.get(i).getDomains().size(); j++){
							if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
								plan.getDomains().add(services.get(i).getDomains().get(j));
						}
						ArrayList<Plan> tempPlans = new ArrayList<>();
						tempPlans.add(plan);
						tempPlans = backwardPlanning(services, request, tempPlans);
						for (int j = 0; j < tempPlans.size(); j++) {
							plans.add(tempPlans.get(j));
						}
					}
				}
			}
		}
		else{
			for (int x = 0; x < currentPlans.size(); x++) {
				Plan p = new Plan();
				p.setState(currentPlans.get(x).getState());
				for (int k = 0; k < currentPlans.get(x).getInputs().size(); k++) {
					p.getInputs().add(currentPlans.get(x).getInputs().get(k));
				}
				for (int k = 0; k < currentPlans.get(x).getOutputs().size(); k++) {
					p.getOutputs().add(currentPlans.get(x).getOutputs().get(k));
				}
				for (int k = 0; k < currentPlans.get(x).getSteps().size(); k++) {
					p.getSteps().add(getSteps(currentPlans.get(x).getSteps().get(k)));
				}
				for (int k = 0; k < currentPlans.get(x).getDomains().size(); k++) {
					p.getDomains().add(currentPlans.get(x).getDomains().get(k));
				}
				plans.add(p);
			}
			for (int x = 0; x < currentPlans.size(); x++) {
				ArrayList<ServiceParameter> requestInputs = request.getInputs();
				ArrayList<ServiceParameter> requestOutputs = new ArrayList<>();
				
				if (currentPlans.get(x).getSteps().get(currentPlans.get(x).getSteps().size() - 1)
						.getClass() == SequentialStep.class) {
					SequentialStep s = (SequentialStep) currentPlans.get(x).getSteps()
							.get(currentPlans.get(x).getSteps().size() - 1);
					SequentialStep sequence = new SequentialStep();
					sequence.setType("SequentialStep");
					sequence.setOrder(s.getOrder());
					for (int k = 0; k < s.getInputs().size(); k++) {
						sequence.getInputs().add(s.getInputs().get(k));
						requestOutputs.add(s.getInputs().get(k));
					}

					for (int k = 0; k < s.getOutputs().size(); k++) {
						sequence.getOutputs().add(s.getOutputs().get(k));
					}
					for (int k = 0; k < s.getSteps().size(); k++) {
						sequence.getSteps().add((SimpleStep) getSteps(s.getSteps().get(k)));
					}
				}

				if (currentPlans.get(x).getSteps().get(currentPlans.get(x).getSteps().size() - 1)
						.getClass() == ParallelStep.class) {
					ParallelStep p = (ParallelStep) currentPlans.get(x).getSteps()
							.get(currentPlans.get(x).getSteps().size() - 1);
					ParallelStep parallel = new ParallelStep();
					parallel.setType("ParallelStep");
					parallel.setOrder(p.getOrder());
				
					for (int k = 0; k < p.getOutputs().size(); k++) {
						parallel.getOutputs().add(p.getOutputs().get(k));
					}
					for (int k = 0; k < p.getSteps().size(); k++) {
						parallel.getSteps().add((ComposedStep) getSteps(p.getSteps().get(k)));
					}

					if (p.getRemainingOutputs().size() > 0) {
						for (int k = 0; k < p.getRemainingOutputs().size(); k++) {
							parallel.getRemainingOutputs().add(p.getRemainingOutputs().get(k));
							requestOutputs.add(p.getRemainingOutputs().get(k));
						}
					} else {
						for (int k = 0; k < p.getInputs().size(); k++) {
							parallel.getInputs().add(p.getInputs().get(k));
							requestOutputs.add(p.getInputs().get(k));
						}

					}
				}

				for (int i = 0; i < services.size(); i++) {
					ArrayList<ServiceParameter> serviceInputs = services.get(i).getInputs();
					ArrayList<ServiceParameter> serviceOutputs = services.get(i).getOutputs();
					ArrayList<ServiceParameter> comparedOutputs = compareParameters(requestOutputs, serviceOutputs);
					if (comparedOutputs.size() == 0) {
						Plan plan = new Plan();
						plan.setState(currentPlans.get(x).getState());
						for (int k = 0; k < currentPlans.get(x).getInputs().size(); k++) {
							plan.getInputs().add(currentPlans.get(x).getInputs().get(k));
						}
						for (int k = 0; k < currentPlans.get(x).getOutputs().size(); k++) {
							plan.getOutputs().add(currentPlans.get(x).getOutputs().get(k));
						}
						for (int k = 0; k < currentPlans.get(x).getSteps().size(); k++) {
							plan.getSteps().add(getSteps(currentPlans.get(x).getSteps().get(k)));
						}
						for (int k = 0; k < currentPlans.get(x).getDomains().size(); k++) {
							plan.getDomains().add(currentPlans.get(x).getDomains().get(k));
						}

						if (plan.getSteps().get(plan.getSteps().size() - 1).getClass() == SequentialStep.class) {

							SequentialStep s = (SequentialStep) plan.getSteps().get(plan.getSteps().size() - 1);
							SequentialStep sequence = new SequentialStep();
							sequence.setType("SequentialStep");
							sequence.setOrder(s.getOrder());
							for (int k = 0; k < s.getInputs().size(); k++) {
								sequence.getInputs().add(s.getInputs().get(k));
								requestOutputs.add(s.getInputs().get(k));
							}

							for (int k = 0; k < s.getOutputs().size(); k++) {
								sequence.getOutputs().add(s.getOutputs().get(k));
							}
							for (int k = 0; k < s.getSteps().size(); k++) {
								sequence.getSteps().add((SimpleStep) getSteps(s.getSteps().get(k)));
							}

							SimpleStep step = new SimpleStep();
							step.setType("SimpleStep");
							step.setInputs(serviceInputs);
							step.setOutputs(serviceOutputs);
							step.setService(services.get(i));
							step.setOrder(sequence.getSteps().size());
							sequence.getSteps().add(step);
							sequence.setInputs(serviceInputs);
							ArrayList<ServiceParameter> comparedInputs = compareParameters(requestInputs,
									serviceInputs);

							if (comparedInputs.size() == 0 || comparedInputs.size() < requestInputs.size()) {
								plan.getSteps().remove(plan.getSteps().size() - 1);
								plan.getSteps().add(sequence);
								plan.setInputs(serviceInputs);
								plan.setState(1);
								for(int j=0; j<services.get(i).getDomains().size(); j++){
									if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
										plan.getDomains().add(services.get(i).getDomains().get(j));
								}
								plans.add(plan);
							}
							if (comparedInputs.size() == requestInputs.size()) {
								plan.getSteps().remove(plan.getSteps().size() - 1);
								plan.getSteps().add(sequence);
								plan.setInputs(serviceInputs);
								plan.setState(0);
								for(int j=0; j<services.get(i).getDomains().size(); j++){
									if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
										plan.getDomains().add(services.get(i).getDomains().get(j));
								}
								ArrayList<Plan> tempPlans = new ArrayList<>();
								tempPlans.add(plan);
								tempPlans = backwardPlanning(services, request, tempPlans);
								for (int j = 0; j < tempPlans.size(); j++) {
									plans.add(tempPlans.get(j));
								}
							}
						}

						if (plan.getSteps().get(plan.getSteps().size() - 1).getClass() == ParallelStep.class) {

							ParallelStep p = (ParallelStep) plan.getSteps().get(plan.getSteps().size() - 1);
							ParallelStep parallel = new ParallelStep();
							parallel.setType("ParallelStep");
							parallel.setOrder(p.getOrder());
							
							for (int k = 0; k < p.getOutputs().size(); k++) {
								parallel.getOutputs().add(p.getOutputs().get(k));
							}
							for (int k = 0; k < p.getInputs().size(); k++) {
								parallel.getInputs().add(p.getInputs().get(k));
							}
							for (int k = 0; k < p.getRemainingOutputs().size(); k++) {
								parallel.getRemainingOutputs().add(p.getRemainingOutputs().get(k));
							}
							for (int k = 0; k < p.getSolvedOutputs().size(); k++) {
								parallel.getSolvedOutputs().add(p.getSolvedOutputs().get(k));
							}
							for (int k = 0; k < p.getSteps().size(); k++) {
								parallel.getSteps().add((ComposedStep) getSteps(p.getSteps().get(k)));
							}

							SequentialStep sequence = new SequentialStep();
							sequence.setType("SequentialStep");
							sequence.setInputs(serviceInputs);
							sequence.setOutputs(serviceOutputs);
							SimpleStep step = new SimpleStep();
							step.setType("SimpleStep");
							step.setOrder(sequence.getSteps().size());
							step.setInputs(serviceInputs);
							step.setOutputs(serviceOutputs);
							step.setService(services.get(i));
							sequence.getSteps().add(step);
							if (parallel.getRemainingOutputs().size() > 0) {
								sequence.setOrder(parallel.getSteps().get(0).getOrder());
								parallel.getSteps().add(sequence);
								parallel.getSolvedOutputs().addAll(serviceOutputs);
								for (int j = 0; j < parallel.getRemainingOutputs().size(); j++) {
									int b = 0;
									for (int k = 0; k < serviceOutputs.size(); k++) {
										if (parallel.getRemainingOutputs().get(j).getName()
												.equals(serviceOutputs.get(k).getName())
												&& parallel.getRemainingOutputs().get(j).getName()
														.equals(serviceOutputs.get(k).getName())) {
											b = 1;
											break;
										}
									}
									if (b == 1) {
										parallel.getRemainingOutputs().remove(j);
									}
								}
								parallel.getInputs().addAll(serviceInputs);

								ArrayList<ServiceParameter> comparedInputs = compareParameters(requestInputs,
										parallel.getInputs());
								if (comparedInputs.size() == 0 || comparedInputs.size() < requestInputs.size()) {
									plan.getSteps().remove(plan.getSteps().size() - 1);
									plan.getSteps().add(parallel);
									if (parallel.getRemainingOutputs().size() > 0) {
										plan.setInputs(parallel.getRemainingOutputs());
									} else {
										plan.setInputs(new ArrayList<ServiceParameter>());
										for (int j = 0; j < parallel.getSteps().size(); j++) {
											for (int k = 0; k < parallel.getSteps().get(j).getInputs().size(); k++) {
												plan.getInputs().add(parallel.getSteps().get(j).getInputs().get(k));
											}
										}
									}
									plan.setState(1);
									for(int j=0; j<services.get(i).getDomains().size(); j++){
										if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
											plan.getDomains().add(services.get(i).getDomains().get(j));
									}
									plans.add(plan);
								}
								if (comparedInputs.size() == requestInputs.size()) {
									plan.getSteps().remove(plan.getSteps().size() - 1);
									plan.getSteps().add(parallel);
									plan.setInputs(serviceInputs);
									for(int j=0; j<services.get(i).getDomains().size(); j++){
										if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
											plan.getDomains().add(services.get(i).getDomains().get(j));
									}
									plan.setState(0);
									ArrayList<Plan> tempPlans = new ArrayList<>();
									tempPlans.add(plan);
									tempPlans = backwardPlanning(services, request, tempPlans);
									for (int j = 0; j < tempPlans.size(); j++) {
										plans.add(tempPlans.get(j));
									}
								}
							} else {
								sequence.setOrder(plan.getSteps().size());
								plan.setInputs(serviceInputs);
								plan.getSteps().add(sequence);

								ArrayList<ServiceParameter> comparedInputs = compareParameters(requestInputs,
										sequence.getInputs());
								if (comparedInputs.size() == 0 || comparedInputs.size() < requestInputs.size()) {
									for(int j=0; j<services.get(i).getDomains().size(); j++){
										if(!plan.getDomains().contains(services.get(i).getDomains().get(j)))
											plan.getDomains().add(services.get(i).getDomains().get(j));
									}
									plan.setState(1);
									plans.add(plan);
								}
								if (comparedInputs.size() == requestInputs.size()) {
									for(int j=0; j<services.get(i).getDomains().size(); j++){
										if(!plan.getDomains().contains(services.get(i).getDomains().get(j)))
											plan.getDomains().add(services.get(i).getDomains().get(j));
									}
									plan.setState(0);
									ArrayList<Plan> tempPlans = new ArrayList<>();
									tempPlans.add(plan);
									tempPlans = backwardPlanning(services, request, tempPlans);
									for (int j = 0; j < tempPlans.size(); j++) {
										plans.add(tempPlans.get(j));
									}
								}
							}
						}
					}
					if (comparedOutputs.size() < requestOutputs.size() && comparedOutputs.size() != 0) {
						Plan plan = new Plan();
						plan.setState(currentPlans.get(x).getState());
						for (int k = 0; k < currentPlans.get(x).getInputs().size(); k++) {
							plan.getInputs().add(currentPlans.get(x).getInputs().get(k));
						}
						for (int k = 0; k < currentPlans.get(x).getOutputs().size(); k++) {
							plan.getOutputs().add(currentPlans.get(x).getOutputs().get(k));
						}
						for (int k = 0; k < currentPlans.get(x).getSteps().size(); k++) {
							plan.getSteps().add(getSteps(currentPlans.get(x).getSteps().get(k)));
						}
						for (int k = 0; k < currentPlans.get(x).getDomains().size(); k++) {
							plan.getDomains().add(currentPlans.get(x).getDomains().get(k));
						}
						
						ParallelStep parallel = new ParallelStep();
						parallel.setType("ParallelStep");
						
						if(plan.getSteps().get(plan.getSteps().size()-1).getClass()==ParallelStep.class){
							ParallelStep oldParallel = (ParallelStep) plan.getSteps().get(plan.getSteps().size()-1);
							parallel.setOrder(oldParallel.getOrder());
							for(int y=0;y<oldParallel.getInputs().size();y++){
								parallel.getInputs().add(oldParallel.getInputs().get(y));
							}
							for(int y=0;y<oldParallel.getOutputs().size();y++){
								parallel.getOutputs().add(oldParallel.getOutputs().get(y));
							}
							for(int y=0;y<oldParallel.getRemainingOutputs().size();y++){
								parallel.getRemainingOutputs().add(oldParallel.getRemainingOutputs().get(y));
							}
							for(int y=0;y<oldParallel.getSolvedOutputs().size();y++){
								parallel.getSolvedOutputs().add(oldParallel.getSolvedOutputs().get(y));
							}
							for(int y=0;y<oldParallel.getSteps().size();y++){
								parallel.getSteps().add(oldParallel.getSteps().get(y));
							}
							plan.getSteps().remove(plan.getSteps().size()-1);
						}else{
							for(int j=0; j<services.get(i).getDomains().size(); j++){
								if(!plan.getDomains().contains(services.get(i).getDomains().get(j)))
									plan.getDomains().add(services.get(i).getDomains().get(j));
							}
							parallel.setOrder(plan.getSteps().size());
							parallel.setInputs(serviceInputs);
							parallel.setOutputs(serviceOutputs);
						}
						
						parallel.setRemainingOutputs(comparedOutputs);
						
						for (int j = 0; j < requestOutputs.size(); j++) {
							int b = 0;
							for (int k = 0; k < comparedOutputs.size(); k++) {
								if (requestOutputs.get(j) == comparedOutputs.get(k)) {
									b = 1;
									break;
								}
							}
							if (b == 0){
								parallel.getSolvedOutputs().add(requestOutputs.get(j));
							}
						}
						
						SequentialStep sequence = new SequentialStep();
						sequence.setType("SequentialStep");
						for(int y=0; y<serviceInputs.size();y++){
							sequence.getInputs().add(serviceInputs.get(y));
							if(!parallel.getInputs().contains(serviceInputs.get(y))){
								parallel.getInputs().add(serviceInputs.get(y));
							}
						}
						for(int y=0; y<serviceOutputs.size();y++){
							sequence.getOutputs().add(serviceOutputs.get(y));
							if(!parallel.getOutputs().contains(serviceOutputs.get(y))){
								parallel.getOutputs().add(serviceOutputs.get(y));
							}
						}
						
						sequence.setOrder(parallel.getSteps().size());
						SimpleStep step = new SimpleStep();
						step.setType("SimpleStep");
						step.setInputs(serviceInputs);
						step.setOutputs(serviceOutputs);
						step.setService(services.get(i));
						sequence.getSteps().add(step);
						parallel.getSteps().add(sequence);
						plan.getSteps().add(parallel);
						plan.setInputs(serviceInputs);
						for(int j=0; j<services.get(i).getDomains().size(); j++){
							if(!plan.getDomains().contains(services.get(i).getDomains().get(j)))
								plan.getDomains().add(services.get(i).getDomains().get(j));
						}
						ArrayList<Plan> tempPlans = new ArrayList<>();
						tempPlans.add(plan);
						tempPlans = backwardPlanning(services, request, tempPlans);
						for (int j = 0; j < tempPlans.size(); j++) {
							plans.add(tempPlans.get(j));
						}
					}
				}
			}
		}
		plans = removeDuplicated(plans);
		return plans;
	}

	private boolean domainIncluded(ArrayList<Domain> domains, Domain domain) {
		for(int i = 0; i<domains.size();i++){
			if(domains.get(i).getName().equals(domain.getName()) && domains.get(i).getUrl().equals(domain.getUrl()))
				return true;
		}
		return false;
	}

	/*public ArrayList<Plan> checkDomains(ArrayList<Plan> plans, RequestDescription request) {
		ArrayList<Plan> res = new ArrayList<Plan>(); 
		for(int i=0; i<plans.size();i++){
			if(allDomainsIncluded(request.getDomains(), plans.get(i).getDomains())){
				res.add(plans.get(i));
			}
		}
		return res;
	}*(

	/*private boolean allDomainsIncluded(ArrayList<Domain> domainsRequest, ArrayList<Domain> domainsPlan) {
		for(int i = 0; i<domainsRequest.size(); i++){
			if(!domainIncluded(domainsPlan,domainsRequest.get(i)))
				return false;
		}
		return true;
	}*/

	private Step getSteps(Step step) {

		if (step.getClass() == SequentialStep.class) {
			SequentialStep oldStep = (SequentialStep) step;
			SequentialStep newStep = new SequentialStep();
			newStep.setType("SequentialStep");
			newStep.setOrder(oldStep.getOrder());
			for (int i = 0; i < oldStep.getInputs().size(); i++) {
				newStep.getInputs().add(oldStep.getInputs().get(i));
			}
			for (int i = 0; i < oldStep.getOutputs().size(); i++) {
				newStep.getOutputs().add(oldStep.getOutputs().get(i));
			}
			for (int i = 0; i < oldStep.getSteps().size(); i++) {
				newStep.getSteps().add((SimpleStep) getSteps(oldStep.getSteps().get(i)));
			}
			return newStep;

		} else if (step.getClass() == ParallelStep.class) {
			ParallelStep oldStep = (ParallelStep) step;
			ParallelStep newStep = new ParallelStep();
			newStep.setType("ParallelStep");
			newStep.setOrder(oldStep.getOrder());
			for (int i = 0; i < oldStep.getInputs().size(); i++) {
				newStep.getInputs().add(oldStep.getInputs().get(i));
			}
			for (int i = 0; i < oldStep.getOutputs().size(); i++) {
				newStep.getOutputs().add(oldStep.getOutputs().get(i));
			}
			for (int i = 0; i < oldStep.getSolvedOutputs().size(); i++) {
				newStep.getSolvedOutputs().add(oldStep.getSolvedOutputs().get(i));
			}
			for (int i = 0; i < oldStep.getRemainingOutputs().size(); i++) {
				newStep.getRemainingOutputs().add(oldStep.getRemainingOutputs().get(i));
			}
			for (int i = 0; i < oldStep.getSteps().size(); i++) {
				newStep.getSteps().add((ComposedStep) getSteps(oldStep.getSteps().get(i)));
			}
			return newStep;
		} else if (step.getClass() == SimpleStep.class) {
			SimpleStep oldStep = (SimpleStep) step;
			SimpleStep newStep = new SimpleStep();
			newStep.setType("SimpleStep");
			newStep.setOrder(oldStep.getOrder());
			newStep.setService(oldStep.getService());
			for (int i = 0; i < oldStep.getInputs().size(); i++) {
				newStep.getInputs().add(oldStep.getInputs().get(i));
			}
			for (int i = 0; i < oldStep.getOutputs().size(); i++) {
				newStep.getOutputs().add(oldStep.getOutputs().get(i));
			}
			return newStep;
		}
		else {
			return null;
		}
	}


	private ArrayList<ServiceParameter> compareParameters(ArrayList<ServiceParameter> requestOutputs,
			ArrayList<ServiceParameter> serviceOutputs) {
		ArrayList<ServiceParameter> parameters = new ArrayList<>();
		if (requestOutputs.size() == 0)
			parameters.addAll(serviceOutputs);
		else if (serviceOutputs.size() == 0)
			parameters.addAll(requestOutputs);
		else {
			for (int i = 0; i < requestOutputs.size(); i++) {
				int b = 0;
				for (int j = 0; j < serviceOutputs.size(); j++) {
					if (requestOutputs.get(i).getName().equals(serviceOutputs.get(j).getName())
							&& requestOutputs.get(i).getType().equals(serviceOutputs.get(j).getType())) {
						b = 1;
						break;
					}
				}
				if (b == 0)
					parameters.add(requestOutputs.get(i));
			}
		}
		return parameters;
	}

	public ArrayList<Plan> removeDuplicated(ArrayList<Plan> plans) {
		if(plans.size()>1){
			ArrayList<Plan> newPlans = new ArrayList<>();
			for(int i=0;i<plans.size();i++){
				Plan plan = plans.get(i);
				if(newPlans.size()==0)newPlans.add(plan);
				else{
					int add = 1;
					for(int j=0;j<newPlans.size();j++){
						Plan newPlan = newPlans.get(j);
						if(sameSteps(plan.getSteps(),newPlan.getSteps())){
							add = 0;
							break;
						}
					}
					if(add==1)newPlans.add(plan);
				}
			}
			return newPlans;
		}else return plans;
	}

	private boolean sameSteps(ArrayList<Step> steps, ArrayList<Step> steps2) {
		boolean res = true;
		if(steps.size()==steps2.size()){
			for(int i=0; i<steps.size();i++){
				if(!sameSteps(steps.get(i),steps2.get(i))){
					res = false;
					break;
				}
			}
		}else{
			res = false;
		}
		return res;
	}

	private boolean sameSteps(Step step, Step step2) {
		boolean res = true;
		if(step.getClass().equals(step2.getClass())){
			if(step.getClass().equals(SimpleStep.class)){
				SimpleStep sstep = (SimpleStep) step;
				SimpleStep sstep2 = (SimpleStep) step2;
				if(!sstep.getService().getName().equals(sstep2.getService().getName()))res = false;
			}
			if(step.getClass().equals(SequentialStep.class)){
				SequentialStep sstep = (SequentialStep) step;
				SequentialStep sstep2 = (SequentialStep) step2;
				if(!sameSteps(sstep.getSteps(),sstep2.getSteps()))res=false;
			}
			if(step.getClass().equals(ParallelStep.class)){
				ParallelStep pstep = (ParallelStep) step;
				ParallelStep pstep2 = (ParallelStep) step2;
				if(pstep.getSteps().size()==pstep2.getSteps().size()){
					int b = 0;
					for(int i=0; i<pstep.getSteps().size();i++){
						for(int j=0; j<pstep2.getSteps().size();j++){
							if(sameSteps(pstep.getSteps().get(i),pstep2.getSteps().get(j))){
								b = 1;
								break;
							}
						}
						if(b==0){
							res = false;
							break;
						}
					}
				}else res = false;
			}
		}else{
			res = false;
		}
		return res;
	}
	
	/***
	Backward planning algorithm reducing search space and using knowledge about previous discovery processes.
	@jsonRequest: Original request in JSON format.
	@previousPlans: Collection of plans previously discovered in other gateways.
	@matchingType: Matchmaking type:
	    5 - Semantic matchmaker (Logic).
	    6 - Cosine Similarity (Non-logic).
	    7 - Jaccard Index (Non-Logic).
	    8 - Semantic matchmaker and Cosine Similarity (Hybrid).
	    9 - Semantic matchmaker and Jaccard Index (Hybrid).
	@similarityThreshold : Threshold for syntactic matching.
	@feedbackThreshold: Threshold to check in discovered edges.
	@top : K plans to be choosen
	@functionalThreshold: Threshold to check to add a discovered plan in the list of top K plans
	@return Discovered plans according to functional requirements.
	***/
	@SuppressWarnings("unchecked")
	public MatchmakerOutput backwardPlanningHeuristic(JSONArray services,String jobId, JSONObject jsonRequest, Map<String,PlanGraph> previousPlans,  int matchingType,
			int top, double similarityThreshold, double feedbackThreshold, double functionalThreshold, int counter) {
		MatchmakerOutput output = new MatchmakerOutput();
		counter ++;
		System.out.println("Backward planning: " + jobId);
		final Map<String,PlanGraph> plans = new HashMap<String,PlanGraph>();
		// Create new plan or use previous
		if(previousPlans.size()==0) {
			PlanGraph plan = new PlanGraph();
			Vertex initialVertex = new Vertex(0,"initial",true,new JSONObject(),new JSONArray(),new JSONArray(),(JSONArray)jsonRequest.get("inputs"),(JSONArray)jsonRequest.get("domains"));
			plan.getVertices().put(initialVertex.getName(), initialVertex);
			Vertex finalVertex = new Vertex(0,"final",false,new JSONObject(),(JSONArray)jsonRequest.get("outputs"),(JSONArray)jsonRequest.get("outputs"),new JSONArray(),(JSONArray)jsonRequest.get("domains"));
			plan.getVertices().put(finalVertex.getName(), finalVertex);
			plan.setInputs((JSONArray) jsonRequest.get("inputs"));
			plan.setOutputs((JSONArray)jsonRequest.get("outputs"));
			plan.setCurrentInputs((JSONArray)jsonRequest.get("outputs"));
			plan.setState(false);
			
			JSONArray requestDomains = (JSONArray)jsonRequest.get("domains");
			for(int i=0; i<requestDomains.size(); i++) {
				int b = 0;
				JSONArray planDomains = plan.getDomains();
				for(int j=0; j<planDomains.size();j++) {
					JSONObject requestDomain =(JSONObject) requestDomains.get(i);
					JSONObject planDomain =(JSONObject) planDomains.get(j);
					if(requestDomain.get("url").equals(planDomain.get("url"))){
						b=1;
						break;
					}
				}
				if(b==0)
					plan.getDomains().add(requestDomains.get(i));
			}
			previousPlans.put(""+previousPlans.size()+1, plan);
		}
		
		// Graph creation
		Map<String,PlanGraph> tempPlans = new HashMap<String,PlanGraph>();
		boolean planChanged = false;
		
		for(Map.Entry<String, PlanGraph> entry:previousPlans.entrySet()){
			
			PlanGraph plan = Util.copyPlan(entry.getValue());
			JSONArray requestOutputs = plan.getCurrentInputs();
			
			// Defining search space
			JSONArray searchSpace = defineSearchSpace(services,requestOutputs);
			
			//Compare each output of each service with each remaining input on each remaining vertex
			for(int i =0; i<searchSpace.size(); i++){
				JSONObject service = (JSONObject) searchSpace.get(i);
				PlanGraph tempPlan = Util.copyPlan(plan);
				
				boolean addTemp = false;
				Vertex discoveredVertex = new Vertex();
				if(tempPlan.getVertices().containsKey(service.get("id"))){
					discoveredVertex = tempPlan.getVertices().get(service.get("id"));
				}else{
					discoveredVertex.setType(2);
					discoveredVertex.setName((String) service.get("id"));
					discoveredVertex.setState(false);
					discoveredVertex.setService(service);
					discoveredVertex.setInputs((JSONArray) service.get("inputs"));
					discoveredVertex.setRemainingInputs((JSONArray) service.get("inputs"));
					discoveredVertex.setOutputs((JSONArray) service.get("outputs"));
					discoveredVertex.setDomains((JSONArray) service.get("domains"));
				}
				Map<String, Vertex> remainingVertices =  tempPlan.getRemainingVertices();
				Comparison comparison = hybridMatchmaker(remainingVertices, discoveredVertex, matchingType, similarityThreshold);
				if(comparison.isMatch()){
					ArrayList<Edge> edges = comparison.getEdges();
					for(int j = 0; j<edges.size(); j++){
						Edge edge = edges.get(j);
						JSONObject link = edge.getLink();
						double hs = 1.0;
						if(hs>=feedbackThreshold){
							tempPlan.getVertices().put(((String) service.get("id")).replace(" ", ""), Util.copyVertex(discoveredVertex));
							tempPlan.getEdges().add(edge);
							tempPlan.update(edge.getDestination(),edge.getSource(),(JSONObject)link.get("matchedInput"),(JSONObject)link.get("matchedOutput"));
							
							JSONArray domains = (JSONArray) service.get("domains");
							for(int k=0; k<domains.size(); k++){
								int b = 0;
								JSONArray planDomains = tempPlan.getDomains();
								for(int x = 0; x<planDomains.size();x++){
									JSONObject domain = (JSONObject) domains.get(k);
									JSONObject planDomain = (JSONObject) planDomains.get(x);
									if(domain.get("url").equals(planDomain.get("url"))){
										b=1;
										break;
									}
								}
								if(b==0)
									tempPlan.getDomains().add(domains.get(k));
							}
							addTemp = true;
						}
					}
				}
				if(addTemp){
					calculateMark(tempPlan);
					if(tempPlan.getMark()>=functionalThreshold){
						tempPlans.put(""+tempPlans.size()+1, Util.copyPlan(tempPlan));
						planChanged = true;
					}
				}
				if(tempPlans.size()==top)
					break;
			}
			if(tempPlans.size()==top)
				break;
		}
		
		
		// Comparison of discovered plans with request inputs
		for(Map.Entry<String, PlanGraph> entry:tempPlans.entrySet()){
			PlanGraph plan = entry.getValue();
			Vertex initial = plan.getVertices().get("initial");
			boolean init = false;
			Map<String, Vertex> remainingVertices = plan.getRemainingVertices();
			Comparison comparison = hybridMatchmaker(remainingVertices, initial, matchingType, similarityThreshold);
			if(comparison.isMatch()){
				init = true;
				ArrayList<Edge> edges = comparison.getEdges();
				for(int i = 0; i<edges.size();i++){
					Edge edge = edges.get(i);
					JSONObject link = edge.getLink();
					double hs = 1.0;
					if(hs>=feedbackThreshold){ 
						plan.update(edge.getDestination(), edge.getSource(), (JSONObject)link.get("matchedInput"), (JSONObject)link.get("matchedOutput"));
						plan.getEdges().add(edge);
					}
				}
			}
			if (plan.getRemainingVertices().size()==0 && init){
				plan.setState(true);
				plans.put("" + plans.size() + 1, plan);
			}
		}
		
		Map<String,PlanGraph> tmpPlans = new HashMap<String,PlanGraph>();
		Map<String,PlanGraph> rankedPlans = new HashMap<String,PlanGraph>();
		if(plans.size()==0 && planChanged){
			// Recursion for not solved Plans and get solved plans after recursion
			rankedPlans=calculateMarks(tempPlans);
			rankedPlans = rankPlans(rankedPlans);
			rankedPlans = topK(rankedPlans,top);
			MatchmakerOutput out = backwardPlanningHeuristic(services, jobId, jsonRequest, rankedPlans, matchingType, top, similarityThreshold, feedbackThreshold, functionalThreshold,counter);
			counter = counter + out.getCounter();
			Map<String,PlanGraph> recursivePlans = out.getPlans();
			recursivePlans.forEach((key, plan) -> {
				tmpPlans.put("" + tmpPlans.size() + 1, plan);
			});
			tmpPlans.forEach((key, plan) -> {
				if(plan.isState()){
					plans.put("" + plans.size() + 1, plan);
				}
			});
		}
		
		Map<String,PlanGraph> plansToReturn = new HashMap<String,PlanGraph>();
		rankedPlans = new HashMap<String,PlanGraph>();
		
		if(plans.size()>0)
			plansToReturn = calculateMarks(plans);
		else if(tmpPlans.size()>0)
			plansToReturn = calculateMarks(tmpPlans);
		else {
			output.setCounter(counter);
			output.setPlans(plansToReturn);
			return output;
		}
		
		plansToReturn = rankPlans(plansToReturn);
		plansToReturn = topK(plansToReturn,top);
		output.setCounter(counter);
		output.setPlans(plansToReturn);
		return output;
	}

	/***
	Get top K plans.
	@plans: List of plans to be ranked.
	@return List of ranked plans.
	***/
	private Map<String, PlanGraph> topK(Map<String, PlanGraph> plans, int k) {
		Map<String, PlanGraph> topk = new HashMap<>();
		int i=0;
		if(k<plans.size()) {
			while(i<k) {
				topk.put("" + (i+1), plans.get(""+(i+1)));
				i++;
			}
			return topk;
		} else
			return plans;
	}
	
	/***
	Rank plans according to mark.
	@plans: List of plans to be ranked.
	@return List of ranked plans.
	***/  
	private Map<String, PlanGraph> rankPlans(Map<String, PlanGraph> plans) {
		Map<String, PlanGraph> rankedPlans = new HashMap<>();
		for(Map.Entry<String, PlanGraph> entry : plans.entrySet()){
			PlanGraph plan = entry.getValue();
			if (rankedPlans.size()==0) {
				rankedPlans.put("" + (rankedPlans.size()+1), plan);
			}else {
				int i = 1;
				Iterator<String> it2 = rankedPlans.keySet().iterator();
				while(it2.hasNext()) {
					String key2 = (String) it2.next();
					PlanGraph ranked = rankedPlans.get(key2);
					if(plan.getMark()>ranked.getMark()) {
						int newK = rankedPlans.size();
						while(newK>i) {
							rankedPlans.put("" + newK, rankedPlans.get("" + (newK-1)));
							newK = newK - 1;
						}
						rankedPlans.put("" + i, plan);
						break;
					}
					i++;
				}
				if(i==(rankedPlans.size()+1)) {
					rankedPlans.put("" + i, plan);
				}
			}
		}
		return rankedPlans;
	}
	
	/***
	Compute marks for the discovered plans.
	@plans: List of plans without marks.
	@return List of plans with marks.
	***/
	private Map<String, PlanGraph> calculateMarks(Map<String, PlanGraph> tempPlans) {
		Map<String, PlanGraph> plans = new HashMap<>();
		for(Map.Entry<String, PlanGraph> entry : tempPlans.entrySet()){
			PlanGraph plan = entry.getValue();
			double mark = 0.0;
			double totalPossible = plan.getEdges().size() * 4;
			double totalActual = 0.0;
			for(int i=0; i<plan.getEdges().size(); i++){
				totalActual = totalActual+plan.getEdges().get(i).getDegree();
			}
			mark = totalActual/totalPossible;
			plan.setMark(mark);
			plans.put("" + (plans.size() + 1), plan);
		}
		return plans;
	}
	
	/***
	Compute mark for a discovered plans.
	@plan: Plans without mark.
	@return Plans with marks.
	***/
	private PlanGraph calculateMark(PlanGraph plan) {
		double mark = 0.0;
		double totalPossible = plan.getEdges().size() * 4;
		double totalActual = 0.0;
		for(int i=0; i<plan.getEdges().size(); i++){
			totalActual = totalActual+plan.getEdges().get(i).getDegree();
		}
		mark = totalActual/totalPossible;
		plan.setMark(mark);
		return plan;
	}
	
	/***
	Hybrid matchmaker between list of I/O parameters.
	@remainingInputs: List of input parameters of the plan to be solved.
	@serviceOutputs: Output parameters of the service that can solve the inputs.
	@matchingtype: Matchmaking type:
	    0 - Semantic matchmaker (Logic).
	    1 - Cosine Similarity (Non-logic).
	    2 - Jaccard Index (Non-Logic).
	    3 - Semantic matchmaker and Cosine Similarity (Hybrid).
	    4 - Semantic matchmaker and Jaccard Index (Hybrid).
	@simThreshold: Similarity threshold. 
	@return Comparison output.
	    match - True if match, False if not.
	    edges - Identified matches
	        link - Source and destination of the relation.        
	        degree - Degree of matching, used to rank the plans.
	            4 - Exact semantic matching.
	            3 - Plugin semantic matching.
	            2 - Subsume semantic matching.
	            1 - Syntactic matching.
	***/
	@SuppressWarnings("unchecked")
	private Comparison hybridMatchmaker(Map<String, Vertex> remainingVertices, Vertex discoveredVertex, int matchingType, double similarityThreshold) {
		Comparison comparison = new Comparison();
		final boolean[] match = {false};
		ArrayList<Edge> edges = new ArrayList<Edge>();
		for(Map.Entry<String, Vertex> entry : remainingVertices.entrySet()){
			Vertex remainingVertex = entry.getValue();
			JSONArray remainingInputs = remainingVertex.getRemainingInputs();
			for(int j=0; j<remainingInputs.size(); j++) {
				((JSONObject)remainingInputs.get(j)).put("state","notSolved");
			}
			JSONArray discoveredVertexOutputs = discoveredVertex.getOutputs();
			for(int j=0; j<discoveredVertexOutputs.size(); j++) {
				((JSONObject)discoveredVertexOutputs.get(j)).put("state","notUsed");
			}
			if(matchingType == 0 || matchingType == 3 || matchingType == 4) {
				for(int j=0; j<remainingInputs.size(); j++) {
					if(((JSONObject)remainingInputs.get(j)).get("state").equals("notSolved")) {
						for(int k=0; k<discoveredVertexOutputs.size();k++) {
						if(exactMatch((JSONObject)remainingInputs.get(j),(JSONObject)discoveredVertexOutputs.get(k)) && ((JSONObject)remainingInputs.get(j)).get("state").equals("notSolved") && ((JSONObject)discoveredVertexOutputs.get(k)).get("state").equals("notUsed")) {
								match[0] = true;
								((JSONObject)remainingInputs.get(j)).put("state", "solved");
								((JSONObject)discoveredVertexOutputs.get(k)).put("state", "used");
								JSONObject link = new JSONObject();
								link.put("matchedOutput", discoveredVertexOutputs.get(k));
								link.put("matchedInput", remainingInputs.get(j));
								Edge edge = new Edge(discoveredVertex,remainingVertex,link,4);
								edges.add(edge);
							}
						}
					}
				}
				for(int j=0; j<remainingInputs.size(); j++) {
					if(((JSONObject)remainingInputs.get(j)).get("state").equals("notSolved")) {
						for(int k=0; k<discoveredVertexOutputs.size();k++) {
							if(pluginMatch((JSONObject)remainingInputs.get(j),(JSONObject)discoveredVertexOutputs.get(k)) && ((JSONObject)remainingInputs.get(j)).get("state").equals("notSolved") && ((JSONObject)discoveredVertexOutputs.get(k)).get("state").equals("notUsed")) {
								match[0] = true;
								((JSONObject)remainingInputs.get(j)).put("state", "solved");
								((JSONObject)discoveredVertexOutputs.get(k)).put("state", "used");
								JSONObject link = new JSONObject();
								link.put("matchedOutput", (JSONObject)discoveredVertexOutputs.get(k));
								link.put("matchedInput", (JSONObject)remainingInputs.get(j));
								Edge edge = new Edge(discoveredVertex,remainingVertex,link,3);
								edges.add(edge);
							}
						}
					}
				}
				for(int j=0; j<remainingInputs.size(); j++) {
					if(((JSONObject)remainingInputs.get(j)).get("state").equals("notSolved")) {
						for(int k=0; k<discoveredVertexOutputs.size();k++) {
							if(subsumeMatch((JSONObject)remainingInputs.get(j),(JSONObject)discoveredVertexOutputs.get(k)) && ((JSONObject)remainingInputs.get(j)).get("state").equals("notSolved") && ((JSONObject)discoveredVertexOutputs.get(k)).get("state").equals("notUsed")) {
								match[0] = true;
								((JSONObject)remainingInputs.get(j)).put("state", "solved");
								((JSONObject)discoveredVertexOutputs.get(k)).put("state", "used");
								JSONObject link = new JSONObject();
								link.put("matchedOutput", (JSONObject)discoveredVertexOutputs.get(k));
								link.put("matchedInput", (JSONObject)remainingInputs.get(j));
								Edge edge = new Edge(discoveredVertex,remainingVertex,link,2);
								edges.add(edge);
							}
						}
					}
				}
			}
			if(matchingType == 1 || matchingType == 2 || matchingType == 3 || matchingType == 4) {
				for(int j=0; j<remainingInputs.size(); j++) {
					if(((JSONObject)remainingInputs.get(j)).get("state").equals("notSolved")) {
						for(int k=0; k<discoveredVertexOutputs.size();k++) {
							if(syntaticMatch((JSONObject)remainingInputs.get(j),(JSONObject)discoveredVertexOutputs.get(k),matchingType,similarityThreshold) && ((JSONObject)remainingInputs.get(j)).get("state").equals("notSolved") && ((JSONObject)discoveredVertexOutputs.get(k)).get("state").equals("notUsed")) {
								match[0] = true;
								((JSONObject)remainingInputs.get(j)).put("state", "solved");
								((JSONObject)discoveredVertexOutputs.get(k)).put("state", "used");
								JSONObject link = new JSONObject();
								link.put("matchedOutput", (JSONObject)discoveredVertexOutputs.get(k));
								link.put("matchedInput", (JSONObject)remainingInputs.get(j));
								Edge edge = new Edge(discoveredVertex,remainingVertex,link,1);
								edges.add(edge);
							}
						}
					}
				}
			}
		}
		comparison.setMatch(match[0]);
		comparison.setEdges(edges);
		return comparison;
	}
	
	/***
	Exact match between two I/O parameters.
	@parameterRequest: Request parameter.
	@parameterService: Service parameter.
	@return True if @parameterRequest EQUIVALENT TO @parameterService.
	***/
	private boolean exactMatch(JSONObject parameterRequest, JSONObject parameterService) {
		boolean match = false;
		if(parameterRequest.get("type").equals(parameterService.get("type")))
			match=true;
		return match;
	}
	
	/***
	Plugin match between two I/O parameters.
	@parameterRequest: Request parameter.
	@parameterService: Service parameter.
	@return True if @parameterRequest SUBCONCEPT OF @parameterService.
	***/
	private boolean pluginMatch(JSONObject parameterRequest, JSONObject parameterService) {
		String serviceType = (String) parameterService.get("type");
		String parameterType = (String) parameterRequest.get("type");
		String ontologyName = serviceType.split("#")[0];
		String key = "";
		for(Map.Entry<String, OntModel> entry : ontologies.entrySet()){
			if(ontologyName.contains(entry.getKey())){
				key = entry.getKey();
				break;
			}
		}
		OntModel ontology = ontologies.get(key);
		if(ontology==null)
			return false;
		OntClass serviceClass = ontology.getOntClass(serviceType);
		OntClass parameterClass = ontology.getOntClass(parameterType);
		if(parameterClass == null || serviceClass == null)
			return false;
		ExtendedIterator<OntClass> serviceSubClasses = serviceClass.listSubClasses();
		while(serviceSubClasses.hasNext()){
			OntClass subClass = serviceSubClasses.next();
			if(parameterClass.equals(subClass))
				return true;
		}
		return false;
	}
	
	/***
	Subsume match between two I/O parameters.
	@parameterRequest: Request parameter.
	@parameterService: Service parameter.
	@return True if @parameterRequest SUPERCONCEPT OF @parameterService
	***/
	private boolean subsumeMatch(JSONObject parameterRequest, JSONObject parameterService) {
		String serviceType = (String) parameterService.get("type");
		String parameterType = (String) parameterRequest.get("type");
		String ontologyName = serviceType.split("#")[0];
		String key = "";
		for(Map.Entry<String, OntModel> entry : ontologies.entrySet()){
			if(ontologyName.contains(entry.getKey())){
				key = entry.getKey();
				break;
			}
		}
		OntModel ontology = ontologies.get(key);
		if(ontology==null)
			return false;
		OntClass serviceClass = ontology.getOntClass(serviceType);
		OntClass parameterClass = ontology.getOntClass(parameterType);
		if(parameterClass == null || serviceClass == null)
			return false;
		ExtendedIterator<OntClass> parameterSubClasses = parameterClass.listSubClasses();
		while(parameterSubClasses.hasNext()){
			OntClass subClass = parameterSubClasses.next();
			if(serviceClass.equals(subClass))
				return true;
		}
		return false;
	}
	
	/***
	Syntactic matchmaker between I/O parameters.
	@parametersRequest: List of request parameters.
	@parametersService: List of service parameters.
	@matchingtype: Matchmaking type:
	    1 or 3 - Cosine Similarity (Non-logic).
	    2 or 4 - Jaccard Index (Non-Logic).
	@simThreshold: Similarity threshold. 
	@return True if similarity(@parameterRequest,@parameterService)==1 or similarity(@parameterRequest,@parameterService)>=distance.
	***/
	private boolean syntaticMatch(JSONObject parameterRequest, JSONObject parameterService, int matchingType, double similarityThreshold) {
		String [] requestDescriptionAll = ((String)parameterRequest.get("desc")).split(" ");
		String [] serviceDescriptionAll = ((String)parameterService.get("desc")).split(" ");
		
		ArrayList<String> requestDescription = new ArrayList<>();
		ArrayList<String> serviceDescription = new ArrayList<>();
		
		for(int i = 0; i<requestDescriptionAll.length; i++){
			String word = requestDescriptionAll[i];
			if(!to_remove.contains(word.toLowerCase()))
				requestDescription.add(word.toLowerCase());
		}
		
		for(int i = 0; i<serviceDescriptionAll.length; i++){
			String word = serviceDescriptionAll[i];
			if(!to_remove.contains(word.toLowerCase()))
				serviceDescription.add(word.toLowerCase());
		}
		
		double similarity = 0.0;
		switch(matchingType){
			case 1:
			case 3:
				similarity = cosineSimilarity(requestDescription,serviceDescription);
		        if (similarity == 1 || similarity >= similarityThreshold)
		            return true;
		        break;
			case 2:
			case 4:
				similarity = jaccardIndex(requestDescription,serviceDescription);
		        if (similarity == 1 || similarity >= similarityThreshold)
		            return true;
		        break;
		}
		return false;
	}
	
	/***
	Jaccard index between two vectors.
	@requestDescription: Vector of the words of the request parameter.
	@serviceDescription: Vector of the words of the service parameter.
	@return Jaccard index between vectors.
	***/
	private double jaccardIndex(ArrayList<String> requestDescription, ArrayList<String> serviceDescription) {
		double jaccardIndex = 0.0;
		ArrayList<String> intersection = new ArrayList<>();
		for(int i=0;i<requestDescription.size();i++){
			if(serviceDescription.contains(requestDescription.get(i)))
				if(!intersection.contains(requestDescription.get(i)))
					intersection.add(requestDescription.get(i));
		}
		
		ArrayList<String> union = new ArrayList<>();
		for(int i=0;i<requestDescription.size();i++){
			if(!union.contains(requestDescription.get(i)))
				union.add(requestDescription.get(i));
		}
		for(int i=0;i<serviceDescription.size();i++){
			if(!union.contains(serviceDescription.get(i)))
				union.add(serviceDescription.get(i));
		}
		
		jaccardIndex=intersection.size()/union.size();
		return jaccardIndex;
	}

	/***
	Cosine similarity between two vectors.
	@requestDescription: Vector of the words of the request parameter.
	@serviceDescription: Vector of the words of the service parameter.
	@return Cosine similarity between vectors.
	***/
	private double cosineSimilarity(ArrayList<String> requestDescription, ArrayList<String> serviceDescription) {
		ArrayList<String> words = new ArrayList<String> ();
		for(int i=0; i<requestDescription.size();i++){
			words.add(requestDescription.get(i));
		}
		for(int i=0; i<serviceDescription.size();i++){
			if(!words.contains(serviceDescription.get(i)))
				words.add(serviceDescription.get(i));
		}
		
		ArrayList<Integer> vectorRequest = new ArrayList<>();
		ArrayList<Integer> vectorService = new ArrayList<>();
		
		for(int i=0; i<words.size();i++){
			if(requestDescription.contains(words.get(i)))
				vectorRequest.add(1);
			else
				vectorRequest.add(0);
		}
		
		for(int i=0; i<words.size();i++){
			if(serviceDescription.contains(words.get(i)))
				vectorService.add(1);
			else
				vectorService.add(0);
		}
		
		double cosineSimilarity = 0.0;
		int dotProduct = 0;
		double magVectorRequest = 0.0;
		double magVectorService = 0.0;
		int i = 0;
		while(i<vectorRequest.size()){
			dotProduct = dotProduct + (vectorRequest.get(i)*vectorService.get(i));
			magVectorRequest = magVectorRequest + (vectorRequest.get(i)*vectorRequest.get(i));
			magVectorService = magVectorService + (vectorService.get(i)*vectorService.get(i));
			i++;
		}
		
		magVectorRequest = Math.sqrt(magVectorRequest);
		magVectorService = Math.sqrt(magVectorService);
		cosineSimilarity = dotProduct/(magVectorRequest*magVectorService);
		
		return cosineSimilarity;
	}

	/***
	Search space definition according to request outputs
	@services: List of services in the gateway.
	@requestOutputs: Requested outputs.
	@return List of services that composes the search space.
	***/
	private JSONArray defineSearchSpace(JSONArray services, JSONArray requestOutputs) {
		JSONArray searchSpace = new JSONArray();
		for(int i=0; i<services.size(); i++){
			JSONObject service = (JSONObject) services.get(i);
			if(((boolean)service.get("state")) == true){
				JSONArray serviceOutputs = (JSONArray) service.get("outputs");
				boolean add = false;
				for(int j=0;j<requestOutputs.size();j++){
					JSONObject requestOutput = (JSONObject) requestOutputs.get(j);
					for(int x=0;x<serviceOutputs.size();x++){
						JSONObject serviceOutput = (JSONObject) serviceOutputs.get(x);
						if(exactMatch(requestOutput, serviceOutput) || subsumeMatch(requestOutput, serviceOutput) || pluginMatch(requestOutput, serviceOutput))
							add = true;
							break;
					}
					if(add)
						break;
				}
				if(add)
					searchSpace.add(service);
			}
		}
		return searchSpace;
	}

	/***
	Check domains of a set of plans according to request
	@plans: List of plans.
	@request: Request.
	@return List of plans that has the request domains.
	***/
	public Map<String, PlanGraph> checkDomains(Map<String, PlanGraph> plans, JSONObject request) {
		Map<String, PlanGraph> res = new HashMap<>();
		for(Map.Entry<String, PlanGraph> entry : plans.entrySet()){
			JSONArray planDomains = entry.getValue().getDomains();
			JSONArray requestDomains = (JSONArray) request.get("domains");
			boolean allIncluded = true;
			for(int i=0;i<requestDomains.size();i++){
				JSONObject requestDomain = (JSONObject) requestDomains.get(i);
				boolean included = false;
				for(int j=0;j<planDomains.size();j++){
					JSONObject planDomain = (JSONObject) planDomains.get(j);
					if(planDomain.get("url").equals(requestDomain.get("url"))){
						included = true;
						break;
					}
				}
				if(!included){
					allIncluded = false;
					break;
				}
			}
			if(allIncluded)
				res.put(entry.getKey(), entry.getValue());
		}
		return res;
	}

	/***
	Check if all the domains of the request are included in the plan domains
	@domainsRequest: Requests domains.
	@domainsPlan: Plan domains.
	@return True if of domains are included.
	***/
	private boolean allDomainsIncluded(JSONArray domainsRequest, JSONArray domainsPlan) {
		for (int i = 0; i < domainsRequest.size(); i++) {
			if (!domainIncluded(domainsPlan, (JSONObject)domainsRequest.get(i)))
				return false;
		}
		return true;
	}
	
	/***
	Check if a domain is in an array of domains
	@domains: List of domains.
	@domain: Domain.
	@return True if of @domain is in @domain.
	***/
	private boolean domainIncluded(JSONArray domains, JSONObject domain) {
		for (int i = 0; i < domains.size(); i++) {
			if (((JSONObject)domains.get(i)).get("name").equals(domain.get("name")) && ((JSONObject)domains.get(i)).get("url").equals(domain.get("url")))
				return true;
		}
		return false;
	}
}
