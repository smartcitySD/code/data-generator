package ie.tcd.scss.surf.data.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class PlanGraph{
	
	private boolean state;
	private Map<String,Vertex> vertices;
	private ArrayList<Edge> edges;
	private Double mark;
	private JSONArray inputs;
	private JSONArray currentInputs;
	private JSONArray outputs;
	private JSONArray domains;
	
	public PlanGraph() {
		this.setState(false);
		this.setVertices(new HashMap<String,Vertex>());
		this.setEdges(new ArrayList<Edge>());
		this.setMark(0.0);
		this.setInputs(new JSONArray());
		this.setCurrentInputs(new JSONArray());
		this.setOutputs(new JSONArray());
		this.setDomains(new JSONArray());
	}
	
	public PlanGraph(boolean state, Map<String,Vertex> vertices, ArrayList<Edge> edges, double mark, JSONArray inputs,
			JSONArray currentInputs, JSONArray outputs, JSONArray domains) {
		this.setState(state);
		this.setVertices(vertices);
		this.setEdges(edges);
		this.setMark(mark);
		this.setInputs(inputs);
		this.setCurrentInputs(currentInputs);
		this.setOutputs(outputs);
		this.setDomains(domains);
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}
	
	public Map<String,Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(Map<String,Vertex> vertices) {
		this.vertices = vertices;
	}

	public ArrayList<Edge> getEdges() {
		return edges;
	}

	public void setEdges(ArrayList<Edge> edges) {
		this.edges = edges;
	}

	public Double getMark() {
		return mark;
	}

	public void setMark(Double mark) {
		this.mark = mark;
	}

	public JSONArray getInputs() {
		return inputs;
	}

	public void setInputs(JSONArray inputs) {
		this.inputs = inputs;
	}

	public JSONArray getCurrentInputs() {
		return currentInputs;
	}

	public void setCurrentInputs(JSONArray currentInputs) {
		this.currentInputs = currentInputs;
	}

	public JSONArray getOutputs() {
		return outputs;
	}

	public void setOutputs(JSONArray outputs) {
		this.outputs = outputs;
	}

	public JSONArray getDomains() {
		return domains;
	}

	public void setDomains(JSONArray domains) {
		this.domains = domains;
	}

	public Map<String,Vertex> getRemainingVertices() {
		Map<String,Vertex> remainingVertices = new HashMap<>();
		for(Map.Entry<String, Vertex> entry : this.getVertices().entrySet()){
			if(!entry.getValue().isState())
				remainingVertices.put(entry.getKey(), entry.getValue());
		}
		return remainingVertices;
	}

	@SuppressWarnings("unchecked")
	public void update(Vertex vertex, Vertex discoveredVertex, JSONObject input, JSONObject output) {
		JSONObject inp = input;
		inp.remove("state");
		
		for(Map.Entry<String, Vertex> entry : this.getRemainingVertices().entrySet()){
			Vertex v = entry.getValue();
			if(vertex.getName().equals(v.getName()))
				v.delRemainingInput(inp);
			if(v.getRemainingInputs().size()==0)
				v.setState(true);
		}
		
		if(discoveredVertex.getType()!=0) {
			for(int i=0; i<discoveredVertex.getInputs().size();i++) {
				JSONObject in = (JSONObject) discoveredVertex.getInputs().get(i);
				boolean b=false;
				for(int j=0; j<this.getCurrentInputs().size(); j++) {
					JSONObject cin = (JSONObject) this.getCurrentInputs().get(j);
					if(cin.equals(in)) {
						b = true;
						break;
					}
				}
				if(!b)
					this.getCurrentInputs().add(in);
			}
		}
		
		final JSONArray remainingInputs = new JSONArray();
		this.getRemainingVertices().forEach((k, v) -> {
			for(int i = 0; i<v.getRemainingInputs().size();i++)
				remainingInputs.add(v.getRemainingInputs().get(i));
		});
		
		boolean b = false;
		for(int i=0; i<remainingInputs.size(); i++) {
			JsonParser parser = new JsonParser();
			JsonElement o1 = parser.parse(remainingInputs.get(i).toString());
			JsonElement o2 = parser.parse(inp.toString());
			if(o1.equals(o2)) {
				b = true;
				break;
			}
		}
		if(!b) {
			for(int i=0; i<this.getCurrentInputs().size(); i++) {
				JsonParser parser = new JsonParser();
				JsonElement o1 = parser.parse(this.getCurrentInputs().get(i).toString());
				JsonElement o2 = parser.parse(inp.toString());
				if(o1.equals(o2)) {
					this.getCurrentInputs().remove(i);
				}
			}
		}
	}
}
