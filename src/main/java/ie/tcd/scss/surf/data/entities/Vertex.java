package ie.tcd.scss.surf.data.entities;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class Vertex {
	
	private int type;
	private String name;
	private boolean state;
	private JSONObject service;
	private JSONArray inputs;
	private JSONArray remainingInputs;
	private JSONArray outputs;
	private JSONArray domains;
	
	public Vertex() {
		this.setType(-1);
		this.setName("");
		this.setState(false);
		this.setService(new JSONObject());
		this.setInputs(new JSONArray());
		this.setRemainingInputs(new JSONArray());
		this.setOutputs(new JSONArray());
		this.setDomains(new JSONArray());
	}
	
	public Vertex(int type, String name, boolean state, JSONObject service, JSONArray inputs, 
			JSONArray remainingInputs, JSONArray outputs, JSONArray domains) {
		this.setType(type);
		this.setName(name);
		this.setState(state);
		this.setService(service);
		this.setInputs(inputs);
		this.setRemainingInputs(remainingInputs);
		this.setOutputs(outputs);
		this.setDomains(domains);
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public JSONObject getService() {
		return service;
	}

	public void setService(JSONObject service) {
		this.service = service;
	}

	public JSONArray getInputs() {
		return inputs;
	}

	public void setInputs(JSONArray inputs) {
		this.inputs = inputs;
	}

	public JSONArray getRemainingInputs() {
		return remainingInputs;
	}

	public void setRemainingInputs(JSONArray remainingInputs) {
		this.remainingInputs = remainingInputs;
	}

	public JSONArray getOutputs() {
		return outputs;
	}

	public void setOutputs(JSONArray outputs) {
		this.outputs = outputs;
	}

	public JSONArray getDomains() {
		return domains;
	}

	public void setDomains(JSONArray domains) {
		this.domains = domains;
	}

	public void delRemainingInput(JSONObject inp) {
		for(int i=0; i<this.remainingInputs.size();i++) {
			JsonParser parser = new JsonParser();
			JsonElement o1 = parser.parse(remainingInputs.get(i).toString());
			JsonElement o2 = parser.parse(inp.toString());
			if(o1.equals(o2)) {
				this.remainingInputs.remove(i);
			}
		}		
	}
}
