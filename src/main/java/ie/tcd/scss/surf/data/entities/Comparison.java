package ie.tcd.scss.surf.data.entities;

import java.util.ArrayList;

public class Comparison {
	
	private boolean match;
	private ArrayList<Edge> edges;
	
	public Comparison(){
		setMatch(false);
		setEdges(new ArrayList<Edge>());
	}
	
	public Comparison(boolean match, ArrayList<Edge> edges){
		this.setMatch(match);
		this.setEdges(edges);
	}

	public boolean isMatch() {
		return match;
	}

	public void setMatch(boolean match) {
		this.match = match;
	}

	public ArrayList<Edge> getEdges() {
		return edges;
	}

	public void setEdges(ArrayList<Edge> edges) {
		this.edges = edges;
	}
}
