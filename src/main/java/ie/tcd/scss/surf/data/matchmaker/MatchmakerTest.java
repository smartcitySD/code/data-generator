package ie.tcd.scss.surf.data.matchmaker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import ie.tcd.scss.surf.data.entities.Domain;
import ie.tcd.scss.surf.data.entities.Plan;
import ie.tcd.scss.surf.data.entities.RequestDescription;
import ie.tcd.scss.surf.data.entities.ServiceDescription;
import ie.tcd.scss.surf.data.entities.ServiceParameter;


public class MatchmakerTest {

	public static void main(String[] args) {
		Matchmaker matchmaker = new Matchmaker();
		ArrayList<ServiceDescription> services = loadServices("../../Data/right-place-dataset/experiments-dataset/IoT-services/");
		ArrayList<RequestDescription> requests = loadRequests("../../Data/right-place-dataset/experiments-dataset/good-requests/");
		int solved=0;
		for(int i=0;i<requests.size();i++){
			ArrayList<Plan> plans = matchmaker.backwardPlanning(services, requests.get(i), new ArrayList<Plan>());
			ArrayList<Plan> completePlans = new ArrayList<>();
			for(int j=0;j<plans.size();j++){
				if(plans.get(j).getState()==1)completePlans.add(plans.get(j));
			}
			if(completePlans.size()>0)solved ++;
			if(completePlans.size()==2){
				System.out.println(requests.get(i).getJobId());
				System.out.println("Plans: " + completePlans.size());
				
			}
		}
		System.out.println("Solved: " + solved);
		
	}
	
	
	/**
	 * 
	 * Load all the services in a given path It is used to calculate search
	 * accuracy with the registered services in the experiments
	 */
	private static ArrayList<ServiceDescription> loadServices(String path) {
		ArrayList<ServiceDescription> services = new ArrayList<>();
		File servicesFolder = new File(path);
		File[] servicesFiles = servicesFolder.listFiles();
		JSONParser parser = new JSONParser();
		for (int i = 0; i < servicesFiles.length; i++) {
			try {
				FileReader reader = new FileReader(servicesFiles[i].getAbsolutePath());
				Object obj = parser.parse(reader);
				JSONObject jsonObject = (JSONObject) obj;
				reader.close();
				ServiceDescription service = new ServiceDescription();

				JSONArray domainsList = (JSONArray) jsonObject.get("domains");
				ArrayList<Domain> domains = new ArrayList<>();
				for(int j=0;j<domainsList.size();j++){
					JSONObject d = (JSONObject) domainsList.get(j);
					Domain domain = new Domain();
					domain.setName((String)d.get("name"));
					domain.setUrl((String)d.get("url"));
					domains.add(domain);
				}
				service.setDomains(domains);

				JSONArray inputsList = (JSONArray) jsonObject.get("inputs");
				ArrayList<ServiceParameter> inputs = new ArrayList<>();
				for(int j=0;j<inputsList.size();j++){
					JSONObject inp = (JSONObject)inputsList.get(j);
					ServiceParameter input = new ServiceParameter();
					input.setName((String) inp.get("name"));
					input.setType((String) inp.get("type"));
				}
				service.setInputs(inputs);
				
				JSONArray outputsList = (JSONArray) jsonObject.get("outputs");
				ArrayList<ServiceParameter> outputs = new ArrayList<>();
				for(int j=0;j<outputsList.size();j++){
					JSONObject out = (JSONObject)outputsList.get(j);
					ServiceParameter output = new ServiceParameter();
					output.setName((String) out.get("name"));
					output.setType((String) out.get("type"));
				}
				service.setOutputs(outputs);
				services.add(service);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return services;
	}
	
	/**
	 * 
	 * Gets a request from our data set according to a place
	 */
	public static ArrayList<RequestDescription> loadRequests(String path) {
		ArrayList <RequestDescription> requests = new ArrayList<>();
		File requestsFolder = new File(path);
		File[] requestsFiles = requestsFolder.listFiles();
		JSONParser parser = new JSONParser();
		for (int i = 0; i < requestsFiles.length; i++) {
			try {
				FileReader reader = new FileReader(requestsFiles[i].getAbsolutePath());
				Object obj = parser.parse(reader);
				JSONObject jsonObject = (JSONObject) obj;
				reader.close();
				RequestDescription request = new RequestDescription();

				JSONArray domainsList = (JSONArray) jsonObject.get("domains");
				ArrayList<Domain> domains = new ArrayList<>();
				for(int j=0;j<domainsList.size();j++){
					JSONObject d = (JSONObject) domainsList.get(j);
					Domain domain = new Domain();
					domain.setName((String)d.get("name"));
					domain.setUrl((String)d.get("url"));
					domains.add(domain);
				}
				request.setDomains(domains);

				JSONArray inputsList = (JSONArray) jsonObject.get("inputs");
				ArrayList<ServiceParameter> inputs = new ArrayList<>();
				for(int j=0;j<inputsList.size();j++){
					JSONObject inp = (JSONObject)inputsList.get(j);
					ServiceParameter input = new ServiceParameter();
					input.setName((String) inp.get("name"));
					input.setType((String) inp.get("type"));
				}
				request.setInputs(inputs);
				
				JSONArray outputsList = (JSONArray) jsonObject.get("outputs");
				ArrayList<ServiceParameter> outputs = new ArrayList<>();
				for(int j=0;j<outputsList.size();j++){
					JSONObject out = (JSONObject)outputsList.get(j);
					ServiceParameter output = new ServiceParameter();
					output.setName((String) out.get("name"));
					output.setType((String) out.get("type"));
				}
				request.setOutputs(outputs);
				request.setJobId(requestsFiles[i].getName());
				requests.add(request);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return requests;
	}

}
