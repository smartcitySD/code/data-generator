package ie.tcd.scss.surf.data.poi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class PointsOfInterest {

	public static void createPOIFiles(){
				
		File folder = new File("../Data/dublin-osm/parts");
		File[] files = folder.listFiles();
		JSONObject poi = new JSONObject();
		JSONObject nd = new JSONObject();
		JSONArray elements = new JSONArray();
		JSONArray nds = new JSONArray();
		for(int i=0; i<files.length; i++){
			String file = files[i].getPath();
			System.out.println("Reading File: " + files[i].getName());
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;
			try {
				db = dbf.newDocumentBuilder();
				Document document = db.parse(file);
				NodeList nodes = document.getElementsByTagName("node");
				for(int j=0; j<nodes.getLength(); j++){
					System.out.println("Reading File: " + files[i].getName() + " Processing node: " + j + " of " + nodes.getLength() + " Number of Elements: " + elements.size() + " Number of NDs: " + nds.size());
					JSONObject node = new JSONObject();
					NodeList childs = nodes.item(j).getChildNodes();
					JSONObject tagsJson = new JSONObject();
					for(int k=0; k<childs.getLength();k++){
						if(childs.item(k).getNodeName().equals("tag")){
							NamedNodeMap attributes = childs.item(k).getAttributes();
							String key = "";
							String value = "";
							for(int x=0;x<attributes.getLength();x++){
								Node nm = attributes.item(x);
								if(nm.getNodeName().equals("k")){
									key = nm.getNodeValue();
								}
								if(nm.getNodeName().equals("v")){
									value = nm.getNodeValue();
								}
							}
							tagsJson.put(key, value);
						}
					}
					if(	tagsJson.size()>0 
							&& tagsJson.containsKey("name")
							&&(tagsJson.containsKey("leisure")
									||tagsJson.containsKey("waterway")
									||tagsJson.containsKey("aeroway")
									||tagsJson.containsKey("office")
									||tagsJson.containsKey("natural")
									||tagsJson.containsKey("amenity") 
									||tagsJson.containsKey("shop") 
									||tagsJson.containsKey("tourism") 
									||tagsJson.containsKey("emergency")
									||tagsJson.containsKey("building")
									||tagsJson.containsKey("historic")
									||tagsJson.containsKey("mand_made")
									||tagsJson.containsKey("public_transport")
									||tagsJson.containsKey("railway")
									||tagsJson.containsKey("route")
									||tagsJson.containsKey("highway"))){
						
						ArrayList<String> tags = new ArrayList<String>();
						if(tagsJson.containsKey("leisure"))tags.add((String) tagsJson.get("leisure"));
						if(tagsJson.containsKey("waterway"))tags.add((String) tagsJson.get("waterway"));
						if(tagsJson.containsKey("aeroway"))tags.add((String) tagsJson.get("aeroway"));
						if(tagsJson.containsKey("office"))tags.add((String) tagsJson.get("office"));
						if(tagsJson.containsKey("natural"))tags.add((String) tagsJson.get("natural"));
						if(tagsJson.containsKey("amenity"))tags.add((String) tagsJson.get("amenity"));
						if(tagsJson.containsKey("shop"))tags.add((String) tagsJson.get("shop"));
						if(tagsJson.containsKey("tourism"))tags.add((String) tagsJson.get("tourism"));
						if(tagsJson.containsKey("emergency"))tags.add((String) tagsJson.get("emergency"));
						if(tagsJson.containsKey("building"))tags.add((String) tagsJson.get("building"));
						if(tagsJson.containsKey("historic")){
							if(tagsJson.get("historic").equals("building")){
								tags.add("historic_building");
							}
							else{
								tags.add((String) tagsJson.get("historic"));
							}
						}
						if(tagsJson.containsKey("man_made"))tags.add((String) tagsJson.get("mand_made"));
						if(tagsJson.containsKey("public_transport"))tags.add((String) tagsJson.get("public_transport"));
						if(tagsJson.containsKey("railway"))tags.add((String) tagsJson.get("railway"));
						if(tagsJson.containsKey("route"))tags.add((String) tagsJson.get("route"));
						if(tagsJson.containsKey("highway")&&!tagsJson.get("highway").toString().toLowerCase().equals("residential"))tags.add((String) tagsJson.get("highway"));
						
						OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
						FileInputStream inputStream;
						inputStream = new FileInputStream("../Data/ontologies/surfOntology-1-0-0surfOntology-1.0.0.owl");
						m.read(inputStream,null);
						inputStream.close();
						OntClass place = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Place");
						ExtendedIterator<OntClass> ps = place.listSubClasses();
						List<OntClass> places = ps.toList();
						ArrayList<String> individualTypes=new ArrayList<String>();
						ArrayList<String> classes=new ArrayList<String>();
						ArrayList<String> domains=new ArrayList<String>();
						ArrayList<String> superDs = new ArrayList<String>();
						boolean tagsFound = false;
						for(int x=0; x<tags.size(); x++){
							String tag = tags.get(x);
							for(int y=0;y<places.size();y++){
								OntClass p = places.get(y);
								ExtendedIterator<OntResource> is = (ExtendedIterator<OntResource>) p.listInstances();
								List<OntResource> instances = is.toList();
								for(int k=0;k<instances.size();k++){
									String s = instances.get(k).getLocalName();
									if(s!=null&&tag!=null){
										if(s.toLowerCase().equals(tag.toLowerCase())){
											if(!individualTypes.contains(instances.get(k).getURI())){
												System.out.println("/////Identified new individual: " + instances.get(k).getURI());
												individualTypes.add(instances.get(k).getURI());
												tagsFound = true;
												classes.add(instances.get(k).asIndividual().getOntClass().getURI());
												ExtendedIterator<OntClass> sc = instances.get(k).asIndividual().getOntClass().listSuperClasses();
												List<OntClass> superClasses = sc.toList();
												for(int z=0;z<superClasses.size();z++){
													if(!superClasses.get(z).getLocalName().toLowerCase().equals("place")&&!superClasses.get(z).getLocalName().toLowerCase().equals("resource")){
														System.out.println("/////Identified new class: " + superClasses.get(z).getURI());
														classes.add(superClasses.get(z).getURI());
													}
												}
												StmtIterator statIt = instances.get(k).listProperties();
												List<Statement> statements = statIt.toList();
												for(int z = 0;z<statements.size();z++){
													Statement statement = statements.get(z);
													if(statement.getPredicate().getLocalName().equals("hasDomain")){
														System.out.println("/////Identified new domain: " + statement.getResource().getURI());
														domains.add(statement.getResource().getURI());
														OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
														ExtendedIterator<OntClass> ds = domain.listSubClasses();
														List<OntClass> domainsOntology = ds.toList();
														for(int w=0;w<domainsOntology.size();w++){
															OntClass d = domainsOntology.get(w);
															ExtendedIterator<OntResource> ins = (ExtendedIterator<OntResource>) d.listInstances();
															List<OntResource> domainInstances = ins.toList();
															for(int h=0;h<domainInstances.size();h++){
																if(domainInstances.get(h).getURI().equals(statement.getResource().getURI())){
																	if(!domains.contains(d.getURI())){
																		System.out.println("/////Identified new domain: " + d.getURI());
																		superDs.add(d.getURI());
																	}
																	ExtendedIterator<OntClass> sd = domainInstances.get(h).asIndividual().getOntClass().listSuperClasses();
																	List<OntClass> superDomains = sd.toList();
																	for(int g=0; g< superDomains.size();g++){
																		if(!superDomains.get(g).getLocalName().toLowerCase().equals("domain")&&!superDomains.get(g).getLocalName().toLowerCase().equals("resource")){
																			if(!domains.contains(superDomains.get(g).getURI())){
																				System.out.println("/////Identified new domain: " + superDomains.get(g).getURI());
																				superDs.add(superDomains.get(g).getURI());
																			}
																		}
																	}
																}
															}
														}
														
													}
												}
											}
										}
									}
								}
							}
						}
						if(tagsFound){
							JSONObject placesJson = new JSONObject();
							JSONArray individualPlaces = new JSONArray();
							for(int x=0;x<individualTypes.size();x++)individualPlaces.add(individualTypes.get(x));
							JSONArray typesPlaces = new JSONArray();
							for(int x=0;x<classes.size();x++)typesPlaces.add(classes.get(x));
							JSONArray domainsPlaces = new JSONArray();
							for(int x=0;x<domains.size();x++)domainsPlaces.add(domains.get(x));
							JSONArray superDomainsPlaces = new JSONArray();
							for(int x=0;x<superDs.size();x++)superDomainsPlaces.add(superDs.get(x));
							System.out.println("*****Adding places******");
							placesJson.put("individuals", individualPlaces);
							placesJson.put("types", typesPlaces);
							placesJson.put("domains", domainsPlaces);
							placesJson.put("superDomains", superDomainsPlaces);
							node.put("places", placesJson);
							node.put("type", "nd");
							NamedNodeMap attributes = nodes.item(j).getAttributes();
							for(int x=0;x<attributes.getLength();x++){
								Node nm = attributes.item(x);
								if(nm.getNodeName().equals("id")){
									node.put(nm.getNodeName(), Long.parseLong(nm.getNodeValue()));
								}
								if(nm.getNodeName().equals("lat") || nm.getNodeName().equals("lon")){
									node.put(nm.getNodeName(), Double.parseDouble(nm.getNodeValue()));
								}
							}
							tagsJson.put("name-real", tagsJson.get("name"));
							tagsJson.put("name", node.get("id") + "/" + tagsJson.get("name"));
							node.put("tags", tagsJson);
							elements.add(node);
						}
					}
					else if(tagsJson.size()==0){
						NamedNodeMap attributes = nodes.item(j).getAttributes();
						for(int x=0;x<attributes.getLength();x++){
							Node nm = attributes.item(x);
							if(nm.getNodeName().equals("id")){
								node.put(nm.getNodeName(), Long.parseLong(nm.getNodeValue()));
							}
							if(nm.getNodeName().equals("lat") || nm.getNodeName().equals("lon")){
								node.put(nm.getNodeName(), Double.parseDouble(nm.getNodeValue()));
							}
						}
						nds.add(node);
					}
				}
				
				NodeList ways = document.getElementsByTagName("way");
				for(int j=0; j<ways.getLength(); j++){
					System.out.println("Reading File: " + files[i].getName() + " Processing way: " + j + " of " + ways.getLength()+ " Number of Elements: " + elements.size() + " Number of NDs: " + nds.size());
					JSONObject node = new JSONObject();
					NodeList childs = ways.item(j).getChildNodes();
					JSONObject tagsJson = new JSONObject();
					JSONArray ndsJson = new JSONArray();
					double lat=0; 
					double lon=0;
					for(int k=0; k<childs.getLength();k++){
						if(childs.item(k).getNodeName().equals("tag")){
							NamedNodeMap attributes = childs.item(k).getAttributes();
							String key = "";
							String value = "";
							for(int x=0;x<attributes.getLength();x++){
								Node nm = attributes.item(x);
								if(nm.getNodeName().equals("k")){
									key = nm.getNodeValue();
								}
								if(nm.getNodeName().equals("v")){
									value = nm.getNodeValue();
								}
							}
							tagsJson.put(key, value);
						}
						
						if(childs.item(k).getNodeName().equals("nd")){
							NamedNodeMap attributes = childs.item(k).getAttributes();
							String key = "";
							String value = "";
							for(int x=0;x<attributes.getLength();x++){
								Node nm = attributes.item(x);
								if(nm.getNodeName().equals("ref")){
									ndsJson.add(nm.getNodeValue());
									if(lat==0 && lon == 0){
										for(int y=0; y<nds.size();y++){
											JSONObject ndj = (JSONObject)nds.get(y);
											long id = (long) ndj.get("id");
											if(id==Long.parseLong(nm.getNodeValue())){
												lat = (double) ndj.get("lat");
												lon = (double) ndj.get("lon");
											}
										}
									}
								}
							}
						}
					}
					if(	tagsJson.size()>0 
							&& tagsJson.containsKey("name")
							&&(tagsJson.containsKey("leisure")
									||tagsJson.containsKey("waterway")
									||tagsJson.containsKey("aeroway")
									||tagsJson.containsKey("office")
									||tagsJson.containsKey("natural")
									||tagsJson.containsKey("amenity") 
									||tagsJson.containsKey("shop") 
									||tagsJson.containsKey("tourism") 
									||tagsJson.containsKey("emergency")
									||tagsJson.containsKey("building")
									||tagsJson.containsKey("historic")
									||tagsJson.containsKey("mand_made")
									||tagsJson.containsKey("public_transport")
									||tagsJson.containsKey("railway")
									||tagsJson.containsKey("route")
									||tagsJson.containsKey("highway"))){
						
						ArrayList<String> tags = new ArrayList<String>();
						if(tagsJson.containsKey("leisure"))tags.add((String) tagsJson.get("leisure"));
						if(tagsJson.containsKey("waterway"))tags.add((String) tagsJson.get("waterway"));
						if(tagsJson.containsKey("aeroway"))tags.add((String) tagsJson.get("aeroway"));
						if(tagsJson.containsKey("office"))tags.add((String) tagsJson.get("office"));
						if(tagsJson.containsKey("natural"))tags.add((String) tagsJson.get("natural"));
						if(tagsJson.containsKey("amenity"))tags.add((String) tagsJson.get("amenity"));
						if(tagsJson.containsKey("shop"))tags.add((String) tagsJson.get("shop"));
						if(tagsJson.containsKey("tourism"))tags.add((String) tagsJson.get("tourism"));
						if(tagsJson.containsKey("emergency"))tags.add((String) tagsJson.get("emergency"));
						if(tagsJson.containsKey("building"))tags.add((String) tagsJson.get("building"));
						if(tagsJson.containsKey("historic")){
							if(tagsJson.get("historic").equals("building")){
								tags.add("historic_building");
							}
							else{
								tags.add((String) tagsJson.get("historic"));
							}
						}
						
						if(tagsJson.containsKey("man_made"))tags.add((String) tagsJson.get("mand_made"));
						if(tagsJson.containsKey("public_transport"))tags.add((String) tagsJson.get("public_transport"));
						if(tagsJson.containsKey("railway"))tags.add((String) tagsJson.get("railway"));
						if(tagsJson.containsKey("route"))tags.add((String) tagsJson.get("route"));
						if(tagsJson.containsKey("highway")&&!tagsJson.get("highway").toString().toLowerCase().equals("residential"))tags.add((String) tagsJson.get("highway"));
						
						OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
						FileInputStream inputStream;
						inputStream = new FileInputStream("../Data/ontologies/surfOntology-1-0-0surfOntology-1.0.0.owl");
						m.read(inputStream,null);
						inputStream.close();
						OntClass place = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Place");
						ExtendedIterator<OntClass> ps = place.listSubClasses();
						List<OntClass> places = ps.toList();
						
						ArrayList<String> individualTypes=new ArrayList<String>();
						ArrayList<String> classes=new ArrayList<String>();
						ArrayList<String> domains=new ArrayList<String>();
						ArrayList<String> superDs = new ArrayList<String>();
						boolean tagsFound = false;
						for(int x=0; x<tags.size(); x++){
							String tag = tags.get(x);
							for(int y=0;y<places.size();y++){
								OntClass p = places.get(y);
								ExtendedIterator<OntResource> is = (ExtendedIterator<OntResource>) p.listInstances();
								List<OntResource> instances = is.toList();
								for(int k=0;k<instances.size();k++){
									String s = instances.get(k).getLocalName();
									if(s!=null&&tag!=null){
										if(s.toLowerCase().equals(tag.toLowerCase())){
											if(!individualTypes.contains(instances.get(k).getURI())){
												System.out.println("/////Identified new individual: " + instances.get(k).getURI());
												individualTypes.add(instances.get(k).getURI());
												tagsFound = true;
												classes.add(instances.get(k).asIndividual().getOntClass().getURI());
												ExtendedIterator<OntClass> sc = instances.get(k).asIndividual().getOntClass().listSuperClasses();
												List<OntClass> superClasses = sc.toList();
												for(int z=0;z<superClasses.size();z++){
													System.out.println("/////Identified new class: " + superClasses.get(z).getURI());
													classes.add(superClasses.get(z).getURI());
												}
												StmtIterator statIt = instances.get(k).listProperties();
												List<Statement> statements = statIt.toList();
												for(int z = 0;z<statements.size();z++){
													Statement statement = statements.get(z);
													if(statement.getPredicate().getLocalName().equals("hasDomain")){
														System.out.println("/////Identified new domain: " + statement.getResource().getURI());
														domains.add(statement.getResource().getURI());
														OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
														ExtendedIterator<OntClass> ds = domain.listSubClasses();
														List<OntClass> domainsOntology = ds.toList();
														for(int w=0;w<domainsOntology.size();w++){
															OntClass d = domainsOntology.get(w);
															ExtendedIterator<OntResource> ins = (ExtendedIterator<OntResource>) d.listInstances();
															List<OntResource> domainInstances = ins.toList();
															for(int h=0;h<domainInstances.size();h++){
																if(domainInstances.get(h).getURI().equals(statement.getResource().getURI())){
																	if(!domains.contains(d.getURI())){
																		System.out.println("/////Identified new domain: " + d.getURI());
																		superDs.add(d.getURI());
																	}
																	ExtendedIterator<OntClass> sd = domainInstances.get(h).asIndividual().getOntClass().listSuperClasses();
																	List<OntClass> superDomains = sd.toList();
																	for(int g=0; g< superDomains.size();g++){
																		if(!superDomains.get(g).getLocalName().toLowerCase().equals("domain")&&!superDomains.get(g).getLocalName().toLowerCase().equals("resource")){
																			if(!domains.contains(superDomains.get(g).getURI())){
																				System.out.println("/////Identified new domain: " + superDomains.get(g).getURI());
																				superDs.add(superDomains.get(g).getURI());
																			}
																		}
																	}
																}
															}
														}
														
													}
												}
											}
										}
									}
								}
							}
						}
						
						if(tagsFound){
							JSONObject placesJson = new JSONObject();
							JSONArray individualPlaces = new JSONArray();
							for(int x=0;x<individualTypes.size();x++)individualPlaces.add(individualTypes.get(x));
							JSONArray typesPlaces = new JSONArray();
							for(int x=0;x<classes.size();x++)typesPlaces.add(classes.get(x));
							JSONArray domainsPlaces = new JSONArray();
							for(int x=0;x<domains.size();x++)domainsPlaces.add(domains.get(x));
							JSONArray superDomainsPlaces = new JSONArray();
							for(int x=0;x<superDs.size();x++)superDomainsPlaces.add(superDs.get(x));
							System.out.println("*****Adding places******");
							placesJson.put("individuals", individualPlaces);
							placesJson.put("types", typesPlaces);
							placesJson.put("domains", domainsPlaces);
							placesJson.put("superDomains", superDomainsPlaces);
							node.put("places", placesJson);
							node.put("nds", ndsJson);
							node.put("type", "way");
							node.put("lat", lat);
							node.put("lon", lon);
							NamedNodeMap attributes = nodes.item(j).getAttributes();
							for(int x=0;x<attributes.getLength();x++){
								Node nm = attributes.item(x);
								if(nm.getNodeName().equals("id")){
									node.put(nm.getNodeName(), Long.parseLong(nm.getNodeValue()));
								}
							}
							tagsJson.put("name-real", tagsJson.get("name"));
							tagsJson.put("name", node.get("id") + "/" + tagsJson.get("name"));
							node.put("tags", tagsJson);
							elements.add(node);
						}
					}
				}
			} catch (ParserConfigurationException | SAXException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//Deleting repetead
		System.out.println("Deleting repeted from: " + elements.size() + " elements");
		JSONArray distinctElements = new JSONArray();
		for(int i=0; i<elements.size();i++){
			JSONObject element = (JSONObject) elements.get(i);
			JSONObject etags = (JSONObject) element.get("tags");
			String ename = (String) etags.get("name");
			boolean alreadyExist=false;
			for(int j=0;j<distinctElements.size();j++){
				JSONObject delement = (JSONObject) distinctElements.get(j);
				JSONObject dtags = (JSONObject) delement.get("tags");
				String dname = (String) dtags.get("name");
				if(dname.equals(ename)){
					alreadyExist=true;
				}				
			}
			if(!alreadyExist)distinctElements.add(element);
		}
		
		System.out.println("Adding unique pois: " + distinctElements.size());
		poi.put("elements", distinctElements);
		JSONArray ndsCleaned = new JSONArray();
		for(int j=0; j< distinctElements.size(); j++){
			JSONObject e = (JSONObject) distinctElements.get(j);
			if(e.get("type").equals("way")){
				JSONArray ndselement = (JSONArray)e.get("nds");
				for(int x=0; x<ndselement.size(); x++){
					Long id = Long.parseLong(ndselement.get(x).toString());
					for(int y=0; y<nds.size();y++){
						JSONObject ndsfile = (JSONObject)nds.get(y);
						Long idf = Long.parseLong(ndsfile.get("id").toString());
						if(idf.equals(id)){
							ndsCleaned.add(ndsfile);
							break;
						}
					}
				}
			}
		}
		System.out.println("Adding nds: " + ndsCleaned.size() + " of " + nds.size());
		nd.put("nds", ndsCleaned);
		try{
			FileWriter f1 = new FileWriter("../Data/dublin-osm/poi-osm/dublinPOIs.json");
			//FileWriter f1 = new FileWriter("G:/poi.json");
			try (Writer writer = f1) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(poi, writer);
			}
			System.out.println("Successfully written poi file...");
			
			FileWriter f2 = new FileWriter("../Data/dublin-osm/poi-osm/dubliNDs.json");
				try (Writer writer = f2) {
					Gson gson = new GsonBuilder().setPrettyPrinting().create();
					gson.toJson(nd, writer);
				}
				System.out.println("Successfully written nds file...");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public static void cleanAndAnnotate(){
		String path = "../../Data/dublin-osm/poi-osm/dublinpoisome.json";
		JSONParser parser = new JSONParser();
		try {
			System.out.println("+++++++++Reading File+++++++++");
			
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String lijn = br.readLine();
			while (lijn != null) {
				sb.append(lijn);
				lijn = br.readLine();
			}
			
			
			/*FileInputStream fis = new FileInputStream(new File(path));
			UnicodeReader ur = new UnicodeReader(fis, "UTF-8");
			BufferedReader in = new BufferedReader(ur);*/
			Object obj = parser.parse(new FileReader(path));
			JSONObject oldJsonObject = (JSONObject) obj ;
			
			JSONObject newJsonObject = new JSONObject();
			System.out.println("*********JSON Object Ready*********");
			JSONArray newElements = (JSONArray) oldJsonObject.get("elements");
			System.out.println("*********New JSON Object Ready to Add Ontology values*********");
			System.out.println("*********JSON Object Ready*********");
			newJsonObject.put("version", "0.6");
			newJsonObject.put("generator", "Overpass API");
			OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2000/01/rdf-schema#");
			m.read("http://www.disit.org/km4city/schema");
			OntClass service = m.getOntClass("http://www.disit.org/km4city/schema#Service");
			ExtendedIterator<OntClass> subServices = service.listSubClasses();
			List<OntClass> cityServices = subServices.toList();
			JSONArray annotatedElements = new JSONArray();
			System.out.println("*********Annotating elements*********");
			for (int j = 0; j < newElements.size(); j++) {
				JSONObject e = (JSONObject) newElements.get(j);
				JSONObject tags = (JSONObject) e.get("tags");
				String amenity = (String) tags.get("amenity");
				amenity = amenity + "," + (String) tags.get("shop");
				amenity = amenity + "," + (String) tags.get("tourism");
				amenity = amenity + "," + (String) tags.get("emergency");
				if (tags.containsKey("highway")) {
					String s = (String) tags.get("highway");
					if (s.equals("bus_stop")) {
						amenity = amenity + ",bus stop";
					}
				}
				String tagsString = amenity.replace("_", " ");
				JSONArray domains = new JSONArray();
				for (int i = 0; i < cityServices.size(); i++) {
					if (tagsString.contains("book"))
						tagsString = tagsString + ",bookshop";
					if (tagsString.contains("clinic"))
						tagsString = tagsString + ",hospital";
					if (tagsString.contains("post_box"))
						tagsString = tagsString + ",post,postal";
					if (tagsString.contains("fire_hydrant"))
						tagsString = tagsString + ",fire";
						String[] wordsTags = tagsString.split(",");
						for (int x = 0; x < wordsTags.length; x++) {
							String wordTag = wordsTags[x].trim();
							if (wordTag.trim().length() > 2 && !wordTag.trim().toLowerCase().equals("ga")
									&& !wordTag.trim().toLowerCase().equals("en")
									&& !wordTag.trim().toLowerCase().equals("yes")
									&& !wordTag.trim().toLowerCase().equals("is")
									&& !wordTag.trim().toLowerCase().equals("in")
									&& !wordTag.trim().toLowerCase().equals("ei")
									&& !wordTag.trim().toLowerCase().equals("ie")
									&& !wordTag.trim().toLowerCase().equals("eu")
									&& !wordTag.trim().toLowerCase().equals("an")
									&& !wordTag.trim().toLowerCase().equals("and")
									&& !wordTag.trim().toLowerCase().equals("or")
									&& !wordTag.trim().toLowerCase().equals("the")
									&& !wordTag.trim().toLowerCase().equals("at")
									&& !wordTag.trim().toLowerCase().equals("on")
									&& !wordTag.trim().toLowerCase().equals("out")
									&& !wordTag.trim().toLowerCase().equals("off")
									&& !wordTag.trim().toLowerCase().equals("these")
									&& !wordTag.trim().toLowerCase().equals("this")
									&& !wordTag.trim().toLowerCase().equals("nga")
									&& !wordTag.trim().toLowerCase().equals("min")
									&& !wordTag.trim().toLowerCase().equals("age")) {
								int b = 0;
								if (wordTag.toLowerCase().equals(cityServices.get(i).getLabel("EN").toLowerCase())) {
									b = 1;
								} 
								else {
									String[] wordsOntology = cityServices.get(i).getLabel("EN").toLowerCase().split(" ");
									for (int y = 0; y < wordsOntology.length; y++) {
										if (wordsOntology[y].toLowerCase().equals(wordTag.toLowerCase())) {
											if (!cityServices.get(i).getLabel("EN").toLowerCase().contains("italian")
													&& !cityServices.get(i).getLabel("EN").toLowerCase().contains("_lost_property_office")) {
												b = 1;
												break;
											}
										}
									}
								}
								if (b == 1) {
									System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
									System.out.println("City service: " + cityServices.get(i).getURI());
									System.out.println("Adding domain: " + cityServices.get(i).getLabel("EN") + " to "
											+ newElements.get(j).toString());
									JSONObject d = new JSONObject();
									d.put("class", cityServices.get(i).getURI());
									ExtendedIterator<OntClass> supServices = cityServices.get(i).listSuperClasses(true);
									List<OntClass> citySuperServices = supServices.toList();
									JSONArray sd = new JSONArray();
									for (int g = 0; g < citySuperServices.size(); g++) {
										if (citySuperServices.get(g).getURI()!=null) {
											sd.add(citySuperServices.get(g).getURI());
											System.out.println("Adding domain: " + citySuperServices.get(g).getLabel("EN")
													+ " to " + newElements.get(j).toString());
										}
									}
									if (sd.size() > 0)
										d.put("superClasses", sd);
									domains.add(d);
								}
							}
						}
					}
					if (domains.size() > 0) {
						JSONObject annotatedElement = (JSONObject) newElements.get(j);
						annotatedElement.put("domains", domains);
						annotatedElements.add(annotatedElement);
					}
					System.out.println("---------------------------------------------");
				}
				newJsonObject.put("elements", annotatedElements);
				System.out.println("*********JSON Object Writing*********");
				FileWriter file = new FileWriter(
						"../../Data/dublin-osm/poi-osm/distinctdublinpoi.json");
				try (Writer writer = file) {
					Gson gson = new GsonBuilder().create();
					gson.toJson(newJsonObject, writer);
				}
				System.out.println("Successfully Copied JSON Object to File...");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
