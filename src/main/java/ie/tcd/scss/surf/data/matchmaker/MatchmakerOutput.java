package ie.tcd.scss.surf.data.matchmaker;

import java.util.Map;

import ie.tcd.scss.surf.data.entities.PlanGraph;

public class MatchmakerOutput {

	private int counter;
	private Map<String,PlanGraph> plans;

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public Map<String,PlanGraph> getPlans() {
		return plans;
	}

	public void setPlans(Map<String,PlanGraph> plans) {
		this.plans = plans;
	}

}
