/**
 * Author: Christian Cabrera
 * This entity represents a QoS parameter in the service description
 */

package ie.tcd.scss.surf.data.entities;

public class QoSParameter {
	private String name;
	private String metric;
	private double value;
	private String unit;
	private String negotiable;
	private String expression;
	private double weight;
	private NegotiationConstraint negotiationConstraints;

	public QoSParameter() {
		this.setName("");
		this.setMetric("");
		this.setValue(0);
		this.setUnit("");
		this.setNegotiable("");
		this.setExpression("");
		this.setWeight(0.0);
	}

	public QoSParameter(String name, String metric, double value, String unit, String negotiable, String expression,
			double weight, NegotiationConstraint negotiationConstraints) {
		this.setName(name);
		this.setMetric(metric);
		this.setValue(value);
		this.setUnit(unit);
		this.setNegotiable(negotiable);
		this.setExpression(expression);
		this.setWeight(weight);
		this.setNegotiationConstraints(negotiationConstraints);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getMetric() {
		return metric;
	}

	public void setMetric(String metric) {
		this.metric = metric;
	}

	public String getNegotiable() {
		return negotiable;
	}

	public void setNegotiable(String negotiable) {
		this.negotiable = negotiable;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public NegotiationConstraint getNegotiationConstraints() {
		return negotiationConstraints;
	}

	public void setNegotiationConstraints(NegotiationConstraint negotiationConstraints) {
		this.negotiationConstraints = negotiationConstraints;
	}
}
