/**
 * Author: Christian Cabrera
 * Abstract class that represents a composed step in a service plan
 */

package ie.tcd.scss.surf.data.entities;

import java.util.ArrayList;

public abstract class ComposedStep extends Step {
	private ArrayList<Step> steps = new ArrayList<Step>();

	public ComposedStep() {
		super();
	}

	public ComposedStep(ArrayList<ServiceParameter> inputs, ArrayList<ServiceParameter> outputs, int order, String type, ArrayList<Step> steps) {
		super(inputs, outputs, order, type);
		this.setSteps(steps);
	}

	public ArrayList<Step> getSteps() {
		return steps;
	}

	public void setSteps(ArrayList<Step> steps) {
		this.steps = steps;
	}
}
