/**
 * Author: Christian Cabrera
 * Class that represents a negotiation constraint
 */

package ie.tcd.scss.surf.data.entities;

public abstract class NegotiationConstraint {
	private String type;

	public NegotiationConstraint() {
		this.setType("");
	}

	public NegotiationConstraint(String type) {
		this.setType(type);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
