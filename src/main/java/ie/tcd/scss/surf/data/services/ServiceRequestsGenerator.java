package ie.tcd.scss.surf.data.services;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ie.tcd.scss.surf.data.entities.Domain;
import ie.tcd.scss.surf.data.entities.Edge;
import ie.tcd.scss.surf.data.entities.Plan;
import ie.tcd.scss.surf.data.entities.PlanGraph;
import ie.tcd.scss.surf.data.entities.RequestDescription;
import ie.tcd.scss.surf.data.entities.ServiceDescription;
import ie.tcd.scss.surf.data.matchmaker.Matchmaker;
import ie.tcd.scss.surf.data.matchmaker.MatchmakerOutput;
import ie.tcd.scss.surf.data.util.Util;

public class ServiceRequestsGenerator {

	@SuppressWarnings({ "unchecked", "unused" })
	public static void generateRequestsAndResponses(){
		File folder = new File("../Data/right-place-dataset/experiments-dataset/IoT-services");
		File[] files = folder.listFiles();
		int req=0;
		Matchmaker m = new Matchmaker();
		for(int i=0; i<files.length; i++){
			ArrayList<JSONObject> requests = new ArrayList<JSONObject>();
			JSONParser parser = new JSONParser();
			try {
				
				System.out.println("+++++++++Reading File+++++++++");
				System.out.println(files[i].toString());	
				Object obj = parser.parse(new FileReader(files[i].toString()));
				JSONObject jsonService = (JSONObject) obj ;
				String name = (String)jsonService.get("name");
				JSONObject request = new JSONObject();
				req++;
				request.put("id", req);
				request.put("inputs", jsonService.get("inputs"));
				request.put("outputs", jsonService.get("outputs"));
				request.put("domains", jsonService.get("domains"));
				request.put("latitude", 0.0);
				request.put("longitude", 0.0);
				JSONArray qoss = new JSONArray();
				JSONObject qos1 = new JSONObject();
				qos1.put("name","availability");				
				qos1.put("value",88);
				qoss.add(qos1);
				JSONObject qos2 = new JSONObject();
				qos2.put("name","accuracy");				
				qos2.put("value",0.9);
				qoss.add(qos2);
				request.put("qos", qoss);
				try{
		    		String requestName = "Request_"+req + "_" + name + ".json";
					FileWriter f1 = new FileWriter("../Data/right-place-dataset/experiments-dataset/requests/"+requestName);
										
					try (Writer writer = f1) {
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
						gson.toJson(request , writer);
					}
					String responseName = "Response_Request_"+req + "_" + name + ".json";
					FileWriter f2 = new FileWriter("../Data/right-place-dataset/experiments-dataset/responses/"+responseName);
					f2.write(name);
					f2.close();
					System.out.println("Successfully written service description file: " + requestName);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public static void generateAllRequestsAndResponses(){
		File folder = new File("../../Data/right-place-dataset/experiments-dataset/IoT-services");
		ArrayList<ServiceDescription> services = new ArrayList<>();
		File[] files = folder.listFiles();
		int req=0;
		for(int i=0; i<files.length; i++){
			JSONParser parser = new JSONParser();
			try {
				
				Object obj = parser.parse(new FileReader(files[i].toString()));
				JSONObject jsonService = (JSONObject) obj ;
				ServiceDescription service = Util.parseJsonToServiceDescription(jsonService);
				services.add(service);
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		ArrayList<Plan> allPlans = new ArrayList<>();
		ArrayList<Plan> requestPlans = new ArrayList<>();
		for(int i=0;i<services.size();i++){
			System.out.println("SERVICE: "+services.get(i).getName());
			Matchmaker m = new Matchmaker();
			RequestDescription request = new RequestDescription();
			ArrayList<Domain> firstDomains = services.get(i).getDomains(); 
			request.setQosParameters(services.get(i).getQosParameters());
			request.setInputs(services.get(i).getInputs());
			for(int j=0;j<services.size();j++){
				ArrayList<Domain> requestDomains = new ArrayList<>();
				requestDomains.addAll(firstDomains);
				/*for(int k=0;k<services.get(j).getDomains().size();k++){
					if(!domainIncluded(requestDomains,services.get(j).getDomains().get(k)))
						requestDomains.add(services.get(j).getDomains().get(k));
				}
				for(int k=0;k<requestDomains.size();k++){
					if(!domainIncluded(request.getDomains(),requestDomains.get(k)))
						request.getDomains().add(requestDomains.get(k));
				}*/
				request.setDomains(requestDomains);
				request.setOutputs(services.get(j).getOutputs());
				requestPlans = m.backwardPlanning(services, request, new ArrayList<Plan>());
				//ArrayList<Plan> checkedPlans = m.checkDomains(requestPlans,request);
				ArrayList<Plan> checkedPlans = new ArrayList<Plan>();
				ArrayList<Plan> noCheckedPlans = new ArrayList<Plan>();
				for(int x=0;x<requestPlans.size();x++){
					if(!checkedPlans.contains(requestPlans.get(x))){
						noCheckedPlans.add(requestPlans.get(x));
					}
				}
				int b = 0;
				for(int x=0;x<checkedPlans.size();x++){
					if(checkedPlans.get(x).getState()==1){
						b=1;
						break;
					}
				}
				if(b==1){
					try{
						JSONObject jsonRequest = new JSONObject();
						JSONObject location = new JSONObject();
						jsonRequest.put("location", location);
						JSONArray jsonDomains = new JSONArray();
						for(int x = 0; x<request.getDomains().size();x++){
							JSONObject domain = new JSONObject();
							domain.put("name", request.getDomains().get(x).getName());
							domain.put("url", request.getDomains().get(x).getUrl());
							jsonDomains.add(domain);
						}
						jsonRequest.put("domains", jsonDomains);
						
						JSONArray jsonInputs = new JSONArray();
						for(int x = 0; x<request.getInputs().size();x++){
							JSONObject input = new JSONObject();
							input.put("name", request.getInputs().get(x).getName());
							input.put("type", request.getInputs().get(x).getType());
							jsonInputs.add(input);
						}
						jsonRequest.put("inputs", jsonInputs);
						
						JSONArray jsonOutputs = new JSONArray();
						for(int x = 0; x<request.getOutputs().size();x++){
							JSONObject output = new JSONObject();
							output.put("name", request.getOutputs().get(x).getName());
							output.put("type", request.getOutputs().get(x).getType());
							jsonOutputs.add(output);
						}
						jsonRequest.put("outputs", jsonOutputs);
						
						JSONArray jsonQoS = new JSONArray();
						for(int x = 0; x<request.getQosParameters().size();x++){
							JSONObject qs = new JSONObject();
							qs.put("name", request.getQosParameters().get(x).getName());
							qs.put("value", request.getQosParameters().get(x).getValue());
							jsonQoS.add(qs);
						}
						jsonRequest.put("qos", jsonQoS);
						String requestName = "Request_"+services.get(i).getId().split("/")[0]+"_"+services.get(j).getId().split("/")[0]+".json";
						FileWriter f1 = new FileWriter("../../Data/right-place-dataset/experiments-dataset/good-requests/"+requestName);
						try (Writer writer = f1) {
							Gson gson = new GsonBuilder().setPrettyPrinting().create();
							gson.toJson(jsonRequest , writer);
						}
						System.out.println("Successfully written service request file: " + requestName);
						f1.close();
						JSONObject plansResponse = new JSONObject();
						JSONArray plans = new JSONArray();
						String responseName = "Request_"+services.get(i).getId().split("/")[0]+"_"+services.get(j).getId().split("/")[0]+".json";
						FileWriter f2 = new FileWriter("../../Data/right-place-dataset/experiments-dataset/responses-json/"+responseName);
						for(int x=0;x<checkedPlans.size();x++){
							if(checkedPlans.get(x).getState()==1){
								JSONObject plan = Util.parsePlanToJson(checkedPlans.get(x));
								plans.add(plan);
							}
						}
						plansResponse.put("plans", plans);
						try (Writer writer = f2) {
							Gson gson = new GsonBuilder().setPrettyPrinting().create();
							gson.toJson(plansResponse , writer);
						}
						f2.close();
						System.out.println("Successfully written response file: " + responseName);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
				else{
					b = 0;
					for(int x=0;x<noCheckedPlans.size();x++){
						if(noCheckedPlans.get(x).getState()==1){
							b=1;
							break;
						}
					}
					if(b==1){
						try{
							JSONObject jsonRequest = new JSONObject();
							JSONObject location = new JSONObject();
							jsonRequest.put("location", location);
							JSONArray jsonDomains = new JSONArray();
							for(int x = 0; x<request.getDomains().size();x++){
								JSONObject domain = new JSONObject();
								domain.put("name", request.getDomains().get(x).getName());
								domain.put("url", request.getDomains().get(x).getUrl());
								jsonDomains.add(domain);
							}
							jsonRequest.put("domains", jsonDomains);
							
							JSONArray jsonInputs = new JSONArray();
							for(int x = 0; x<request.getInputs().size();x++){
								JSONObject input = new JSONObject();
								input.put("name", request.getInputs().get(x).getName());
								input.put("type", request.getInputs().get(x).getType());
								jsonInputs.add(input);
							}
							jsonRequest.put("inputs", jsonInputs);
							
							JSONArray jsonOutputs = new JSONArray();
							for(int x = 0; x<request.getOutputs().size();x++){
								JSONObject output = new JSONObject();
								output.put("name", request.getOutputs().get(x).getName());
								output.put("type", request.getOutputs().get(x).getType());
								jsonOutputs.add(output);
							}
							jsonRequest.put("outputs", jsonOutputs);
							
							JSONArray jsonQoS = new JSONArray();
							for(int x = 0; x<request.getQosParameters().size();x++){
								JSONObject qs = new JSONObject();
								qs.put("name", request.getQosParameters().get(x).getName());
								qs.put("value", request.getQosParameters().get(x).getValue());
								jsonQoS.add(qs);
							}
							jsonRequest.put("qos", jsonQoS);
							String requestName = "Request_"+services.get(i).getId().split("/")[0]+"_"+services.get(j).getId().split("/")[0]+".json";
							FileWriter f1 = new FileWriter("../../Data/right-place-dataset/experiments-dataset/bad-requests/"+requestName);
							try (Writer writer = f1) {
								Gson gson = new GsonBuilder().setPrettyPrinting().create();
								gson.toJson(jsonRequest , writer);
							}
							System.out.println("Successfully written service request file: " + requestName);
							f1.close();
							/*JSONObject plansResponse = new JSONObject();
							JSONArray plans = new JSONArray();
							String responseName = "Request_"+services.get(i).getId().split("/")[0]+"_"+services.get(j).getId().split("/")[0]+".json";
							FileWriter f2 = new FileWriter("../../Data/right-place-dataset/experiments-dataset/responses-json/"+responseName);
							for(int x=0;x<noCheckedPlans.size();x++){
								if(noCheckedPlans.get(x).getState()==1){
									JSONObject plan = Util.parsePlanToJson(noCheckedPlans.get(x));
									plans.add(plan);
								}
							}
							plansResponse.put("plans", plans);
							try (Writer writer = f2) {
								Gson gson = new GsonBuilder().setPrettyPrinting().create();
								gson.toJson(plansResponse , writer);
							}
							f2.close();
							System.out.println("Successfully written response file: " + responseName);*/
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}else{
						try{
							JSONObject jsonRequest = new JSONObject();
							JSONObject location = new JSONObject();
							jsonRequest.put("location", location);
							JSONArray jsonDomains = new JSONArray();
							for(int x = 0; x<request.getDomains().size();x++){
								JSONObject domain = new JSONObject();
								domain.put("name", request.getDomains().get(x).getName());
								domain.put("url", request.getDomains().get(x).getUrl());
								jsonDomains.add(domain);
							}
							jsonRequest.put("domains", jsonDomains);
							
							JSONArray jsonInputs = new JSONArray();
							for(int x = 0; x<request.getInputs().size();x++){
								JSONObject input = new JSONObject();
								input.put("name", request.getInputs().get(x).getName());
								input.put("type", request.getInputs().get(x).getType());
								jsonInputs.add(input);
							}
							jsonRequest.put("inputs", jsonInputs);
							
							JSONArray jsonOutputs = new JSONArray();
							for(int x = 0; x<request.getOutputs().size();x++){
								JSONObject output = new JSONObject();
								output.put("name", request.getOutputs().get(x).getName());
								output.put("type", request.getOutputs().get(x).getType());
								jsonOutputs.add(output);
							}
							jsonRequest.put("outputs", jsonOutputs);
							
							JSONArray jsonQoS = new JSONArray();
							for(int x = 0; x<request.getQosParameters().size();x++){
								JSONObject qs = new JSONObject();
								qs.put("name", request.getQosParameters().get(x).getName());
								qs.put("value", request.getQosParameters().get(x).getValue());
								jsonQoS.add(qs);
							}
							jsonRequest.put("qos", jsonQoS);
							String requestName = "Request_"+services.get(i).getId().split("/")[0]+"_"+services.get(j).getId().split("/")[0]+".json";
							FileWriter f1 = new FileWriter("../../Data/right-place-dataset/experiments-dataset/impossible-requests/"+requestName);
							try (Writer writer = f1) {
								Gson gson = new GsonBuilder().setPrettyPrinting().create();
								gson.toJson(jsonRequest , writer);
							}
							System.out.println("Successfully written service request file: " + requestName);
							f1.close();
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
				}
			}
		}
		System.out.println("Number of plans:" + allPlans.size());
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public static void generateAllRequestsAndResponsesFull(){
		File folder = new File("../../Data/services-dataset/full-dataset/all");
		JSONArray services1 = new JSONArray();
		JSONArray services2 = new JSONArray();
		JSONArray services3 = new JSONArray();
		File[] files = folder.listFiles();
		int req=0;
		for(int i=0; i<files.length; i++){
			JSONParser parser = new JSONParser();
			try {
				
				Object obj = parser.parse(new FileReader(files[i].toString()));
				JSONObject jsonService = (JSONObject) obj ;
				ServiceDescription service = Util.parseJsonToServiceDescription(jsonService);
				services1.add(jsonService);
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		for(int i=0; i<files.length; i++){
			JSONParser parser = new JSONParser();
			try {
				
				Object obj = parser.parse(new FileReader(files[i].toString()));
				JSONObject jsonService = (JSONObject) obj ;
				ServiceDescription service = Util.parseJsonToServiceDescription(jsonService);
				services2.add(jsonService);
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		for(int i=0; i<files.length; i++){
			JSONParser parser = new JSONParser();
			try {
				
				Object obj = parser.parse(new FileReader(files[i].toString()));
				JSONObject jsonService = (JSONObject) obj ;
				ServiceDescription service = Util.parseJsonToServiceDescription(jsonService);
				services3.add(jsonService);
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		ArrayList<Plan> allPlans = new ArrayList<>();
		Map<String,PlanGraph> requestPlans = new HashMap<String,PlanGraph>();
		for(int i=0;i<services1.size();i++){
			JSONObject service1 = (JSONObject) services1.get(i);
			System.out.println("SERVICE: "+service1.get("id"));
			Matchmaker m = new Matchmaker();
			JSONObject request = new JSONObject();
			JSONArray firstDomains = (JSONArray) service1.get("domains");
			if(service1.containsKey("qos"))
				request.put("qos", service1.get("qos"));
			request.put("inputs", service1.get("inputs"));
			for(int j=0;j<services2.size();j++){
				JSONObject service2 = (JSONObject) services2.get(j);
				JSONArray requestDomains = new JSONArray();
				requestDomains.addAll(firstDomains);
				JSONArray service2Domains = (JSONArray) service2.get("domains");
				
				for(int k=0;k<service2Domains.size();k++){
					if(!domainIncluded(requestDomains,(JSONObject)service2Domains.get(k)))
						requestDomains.add(service2Domains.get(k));
				}
				request.put("domains", requestDomains);
				request.put("outputs", service2.get("outputs"));
				MatchmakerOutput output = new MatchmakerOutput();
				output = m.backwardPlanningHeuristic(services3, "id_" + service1.get("name") + "_" + service2.get("name"), request, new HashMap<String,PlanGraph>(), 3, 5, 0.8, 1.0, 1.0, 0);
				requestPlans = output.getPlans();
				
				Map<String, PlanGraph> completedPlans = new HashMap<>();
				for(Map.Entry<String, PlanGraph> entry : requestPlans.entrySet()){
					if(entry.getValue().isState())
						completedPlans.put(entry.getKey(), entry.getValue());
				}
				
				Map<String, PlanGraph> checkedPlans = m.checkDomains(completedPlans,request);
				Map<String, PlanGraph> noCheckedPlans = new HashMap<>();
				
				for(Map.Entry<String, PlanGraph> entry : checkedPlans.entrySet()){
					if(!checkedPlans.containsKey(entry.getKey()))
						noCheckedPlans.put(entry.getKey(), requestPlans.get(entry.getKey()));
				}
				
				if(checkedPlans.size()>0){
					String reqName = "";
					if(service1.get("id").equals(service2.get("id"))) 
						reqName = "req-"+service1.get("id").toString().replace("/","-");
					else
						reqName = "req-" + service1.get("id").toString().replace("/","-") + "-" + service2.get("id").toString().replace("/","-");
					request.put("id", reqName);
					
					JSONObject response = new JSONObject();
					JSONArray offers = new JSONArray();
				
					for(Map.Entry<String, PlanGraph> entry : checkedPlans.entrySet()){
						JSONObject offer = new JSONObject();
						offer.put("relevant", 1);
						JSONArray graph = new JSONArray();
						for(int x=0; x<entry.getValue().getEdges().size(); x++) {
							Edge e = entry.getValue().getEdges().get(x);
							graph.add("from " + e.getSource().getName() + " to " + e.getDestination().getName());
						}
						offer.put("graph", graph);
						offers.add(offer);
					}
					
					for(Map.Entry<String, PlanGraph> entry : noCheckedPlans.entrySet()){
						JSONObject offer = new JSONObject();
						offer.put("relevant", 0);
						JSONArray graph = new JSONArray();
						for(int x=0; x<entry.getValue().getEdges().size(); x++) {
							Edge e = entry.getValue().getEdges().get(x);
							graph.add("from " + e.getSource().getName() + " to " + e.getDestination().getName());
						}
						offer.put("graph", graph);
						offers.add(offer);
					}
					response.put("requestId", reqName);
					response.put("offers", offers);
					
					try {
						FileWriter f1 = new FileWriter("../../Data/requests-dataset/full-dataset/requests/"+reqName+".json");
						try (Writer writer = f1) {
							Gson gson = new GsonBuilder().setPrettyPrinting().create();
							gson.toJson(request , writer);
						}
						System.out.println("Successfully written service request file: " + reqName);
						f1.close();
					}catch(Exception ex) {
						
					}
					
					try {
						FileWriter f1 = new FileWriter("../../Data/requests-dataset/full-dataset/responses/"+reqName+".json");
						try (Writer writer = f1) {
							Gson gson = new GsonBuilder().setPrettyPrinting().create();
							gson.toJson(response , writer);
						}
						System.out.println("Successfully written service response file: " + reqName);
						f1.close();
					}catch(Exception ex) {
						
					}
				}
			}
		}
		System.out.println("Number of plans:" + allPlans.size());
	}
	
	private static boolean domainIncluded(JSONArray domains, JSONObject domain) {
		for(int i = 0; i<domains.size();i++){
			JSONObject d = (JSONObject) domains.get(i);
			if(d.get("name").equals(domain.get("name")) && d.get("url").equals(domain.get("url")))
				return true;
		}
		return false;
	}
	
	/**
	 * 
	 * Creates the file names that is used to read the requests at discovery time
	 */
	@SuppressWarnings({ "unchecked" })
	public static void generateFileNames() {
		
		File folderServices = new File("../../Data/right-place-dataset/experiments-dataset/bad-requests");
		File[] files = folderServices.listFiles();
		JSONArray names = new JSONArray();
		JSONParser parser = new JSONParser();
		for (int i = 0; i < files.length; i++){
			try{
				FileReader f = new FileReader(files[i].getAbsolutePath());
				Object obj = parser.parse(f);
				f.close();
				JSONObject request = (JSONObject) obj;
				JSONArray domains = (JSONArray) request.get("domains");
				String var = files[i].getName();
				for(int j=0;j<domains.size();j++){
					JSONObject domain = (JSONObject) domains.get(j); 
					var = var + "," + domain.get("url");
				}
				names.add(var);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		JSONObject allNames = new JSONObject();
		try {
			FileWriter f = new FileWriter("../../Data/right-place-dataset/experiments-dataset/bad-requests-names.json");
			allNames.put("names", names);
			try (Writer writer = f) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(allNames, writer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		folderServices = new File("../../Data/right-place-dataset/experiments-dataset/good-requests");
		files = folderServices.listFiles();
		names = new JSONArray();
		for (int i = 0; i < files.length; i++) {
			try{
				FileReader f = new FileReader(files[i].getAbsolutePath());
				Object obj = parser.parse(f);
				f.close();
				JSONObject request = (JSONObject) obj;
				JSONArray domains = (JSONArray) request.get("domains");
				String var = files[i].getName();
				for(int j=0;j<domains.size();j++){
					JSONObject domain = (JSONObject) domains.get(j); 
					var = var + "," + domain.get("url");
				}
				names.add(var);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		allNames = new JSONObject();
		try {
			FileWriter f = new FileWriter("../../Data/right-place-dataset/experiments-dataset/good-requests-names.json");
			allNames.put("names", names);
			try (Writer writer = f) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(allNames, writer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		folderServices = new File("../../Data/right-place-dataset/experiments-dataset/impossible-requests");
		files = folderServices.listFiles();
		names = new JSONArray();
		for (int i = 0; i < files.length; i++) {
			try{
				FileReader f = new FileReader(files[i].getAbsolutePath());
				Object obj = parser.parse(f);
				f.close();
				JSONObject request = (JSONObject) obj;
				JSONArray domains = (JSONArray) request.get("domains");
				String var = files[i].getName();
				for(int j=0;j<domains.size();j++){
					JSONObject domain = (JSONObject) domains.get(j); 
					var = var + "," + domain.get("url");
				}
				names.add(var);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		allNames = new JSONObject();
		try {
			FileWriter f = new FileWriter("../../Data/right-place-dataset/experiments-dataset/impossible-requests-names.json");
			allNames.put("names", names);
			try (Writer writer = f) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(allNames, writer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** Annotates IoT requests with the I/O information
	 **/
	@SuppressWarnings({ "unchecked" })
	public static void fixIORequests() {
		File folder = new File("../../Data/right-place-dataset/experiments-dataset/good-requests/");
		File[] files = folder.listFiles();
		System.out.println("Number of Files: " + files.length);
		for(int j=0;j<files.length;j++){
			File file = files[j];
			try {
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(new FileReader(file));
				JSONObject serviceDescription = (JSONObject) obj;
				
				JSONArray inputs = (JSONArray) serviceDescription.get("inputs");
				JSONArray newInputs = new JSONArray();
				for(int x=0; x<inputs.size();x++) {
					JSONObject input = (JSONObject) inputs.get(x);
					if(!input.containsKey("desc")) {
						String name = (String) input.get("name");
						input.put("type", "http://www.surf.scss.tcd.ie/IoTservices.owl#" + name);
						input.put("desc", "Input of service " + serviceDescription.get("name"));
						input.put("name", "_"+name.toUpperCase());
					}
					newInputs.add(input);
				}
				serviceDescription.put("inputs", newInputs);
				
				
				JSONArray outputs = (JSONArray) serviceDescription.get("outputs");
				JSONArray newOutputs = new JSONArray();
				for(int x=0; x<outputs.size();x++) {
					JSONObject output = (JSONObject) outputs.get(x);
					if(!output.containsKey("desc")) {
						String name = (String) output.get("name");
						output.put("type", "http://www.surf.scss.tcd.ie/IoTservices.owl#" + name);
						output.put("desc", "Output of service " + serviceDescription.get("name"));
						output.put("name", "_"+name.toUpperCase());
					}
					newOutputs.add(output);
				}
				serviceDescription.put("outputs", newOutputs);
				
				File fl = new File("../../Data/requests-dataset/requests/"+file.getName());
				fl.getParentFile().mkdirs();
				FileWriter f = new FileWriter(fl.getAbsoluteFile());
				try (Writer writer = f) {
					Gson gson = new GsonBuilder().setPrettyPrinting().create();
					gson.toJson(serviceDescription, writer);
				}
				System.out.println("Successfully written service description file: " + file.getName());
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void generateAtomicRequestsFromFiles() {
		File atomicResponses = new File("../../Data/requests-dataset/owl-responses/responses_1.json");
		JSONObject responses = new JSONObject();
		JSONParser parser = new JSONParser();
		Object obj;
		try {
			obj = parser.parse(new FileReader(atomicResponses.toString()));
			JSONObject responsesFromFile = (JSONObject) obj ;
			Iterator it = responsesFromFile.keySet().iterator();
			while(it.hasNext()){
				String key = (String) it.next();
				JSONObject responseFromFile = (JSONObject) responsesFromFile.get(key);
				String service = (String) responseFromFile.get("request");
				String request = "";
				service = service.replace("http://127.0.0.1/queries/1.1/", "");
				service = service.split(".owls")[0];
				request = service + "_request.json";
				service = service + ".json";
				int b = 0;
				
				JSONObject response = new JSONObject();
				response.put("type", "atomic");
				JSONArray offers = new JSONArray();
				JSONArray offersFromFile = (JSONArray) responseFromFile.get("offers");
				for(int i=0; i<offersFromFile.size();i++){
					JSONObject offerFromFile = (JSONObject) offersFromFile.get(i);
					if(offerFromFile.get("relevant").equals("1")){
						JSONObject offer = new JSONObject();
						offer.put("relevant", 1);
						String serviceOffer = (String) offerFromFile.get("offer");
						serviceOffer = serviceOffer.replace("http://127.0.0.1/services/1.1/", "");
						serviceOffer = serviceOffer.split(".owls")[0];
						serviceOffer = serviceOffer + ".json";
						try{
							System.out.println("Processing service offer" + serviceOffer + "...");
							File serviceFile = new File("../../Data/services-dataset/services/"+serviceOffer);
							obj = parser.parse(new FileReader(serviceFile));
							JSONObject jsonService = (JSONObject) obj; 
							JSONArray graph = new JSONArray();
							graph.add("from initial to " + jsonService.get("name"));
							graph.add("from " + jsonService.get("name") + " to final");
							offer.put("graph", graph);
							offers.add(offer);
							System.out.println("Successfully processed service offer file: " + request);
							b++;
							
						}catch(Exception ex){
							System.out.println("Problems with service: " + service);
						}
					}
					
				}
				response.put("offers", offers);
				if(b>0){
					try{
						System.out.println("Processing service " + service + "...");
						File serviceFile = new File("../../Data/services-dataset/services/"+service);
						obj = parser.parse(new FileReader(serviceFile));
						JSONObject jsonService = (JSONObject) obj; 
						JSONObject jsonRequest = new JSONObject();
						jsonRequest.put("id", request.replace(".json", ""));
						jsonRequest.put("type", "atomic");
						jsonRequest.put("expectedPlanLength", 1);
						jsonRequest.put("domains", jsonService.get("domains"));
						jsonRequest.put("inputs", jsonService.get("inputs"));
						jsonRequest.put("outputs", jsonService.get("outputs"));
						File requestFile = new File("../../Data/requests-dataset/atomic-requests/"+request);
						requestFile.getParentFile().mkdirs();
						FileWriter f = new FileWriter(requestFile.getAbsoluteFile());
						try (Writer writer = f) {
							Gson gson = new GsonBuilder().setPrettyPrinting().create();
							gson.toJson(jsonRequest, writer);
						}
						System.out.println("Successfully written service request file: " + request);
						
					}catch(Exception ex){
						System.out.println("Problems with service: " + service);
					}
				}
				responses.put(request, response);
			}
			
			File folder = new File("../../Data/services-dataset/iot-services/");
			File[] files = folder.listFiles();
			System.out.println("Number of Files: " + files.length);
			for(int j=0;j<files.length;j++){
				File file = files[j];
				String request = file.getName().replace(".json", "") + "_request.json";
				try {
					parser = new JSONParser();
					obj = parser.parse(new FileReader(file));
					JSONObject jsonService = (JSONObject) obj;
					JSONObject jsonRequest = new JSONObject();
					jsonRequest.put("id", request.replace(".json", ""));
					jsonRequest.put("type", "atomic");
					jsonRequest.put("expectedPlanLength", 1);
					jsonRequest.put("domains", jsonService.get("domains"));
					jsonRequest.put("inputs", jsonService.get("inputs"));
					jsonRequest.put("outputs", jsonService.get("outputs"));
					File requestFile = new File("../../Data/requests-dataset/atomic-requests/"+request);
					requestFile.getParentFile().mkdirs();
					FileWriter f = new FileWriter(requestFile.getAbsoluteFile());
					try (Writer writer = f) {
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
						gson.toJson(jsonRequest, writer);
					}
					System.out.println("Successfully written service request file: " + request);
					
					JSONObject response = new JSONObject();
					response.put("type", "atomic");
					JSONArray offers = new JSONArray();
					JSONObject offer = new JSONObject();
					offer.put("relevant", 1);
					JSONArray graph = new JSONArray();
					graph.add("from initial to " + jsonService.get("name"));
					graph.add("from " + jsonService.get("name") + " to final");
					offer.put("graph", graph);
					offers.add(offer);
					response.put("offers", offers);
					System.out.println("Successfully processed service offer file: " + request);
					responses.put(request.replace(".json", ""), response);
					
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
			File responsesFile = new File("../../Data/requests-dataset/atomic-responses/responses_1.json");
			responsesFile.getParentFile().mkdirs();
			FileWriter f = new FileWriter(responsesFile.getAbsoluteFile());
			try (Writer writer = f) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(responses, writer);
			}
			System.out.println("Successfully written service request file: responses_1.json");
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addingDomains() {
		File[] folders = new File("../../Data/requests-dataset/all-requests/").listFiles();
		for(int i=0; i<folders.length; i++){
			File[] files = folders[i].listFiles();
			for(int j=0; j<files.length; j++) {
				try {
					JSONParser parser = new JSONParser();
					Object obj = parser.parse(new FileReader(files[j]));
					JSONObject jsonRequest = (JSONObject) obj;
					String id = (String) jsonRequest.get("id");
					
					
					if(Integer.parseInt(folders[i].getName())>1) {
						String file1 = id.split("_x_")[0];
						String file2 = id.split("_x_")[1];
					
						File[] servicesFiles = new File("../../Data/services-dataset/iot-services/").listFiles();
						for(int x=0; x<servicesFiles.length;x++) {
							String serviceFile = servicesFiles[x].getName();
							if(serviceFile.contains(file1)) {
								parser = new JSONParser();
								obj = parser.parse(new FileReader(servicesFiles[x]));
								JSONObject jsonService = (JSONObject) obj;
								JSONArray domains = (JSONArray) jsonService.get("domains");
								JSONArray requestInputs = (JSONArray) jsonRequest.get("inputs");
								for(int y=0; y<requestInputs.size();y++) {
									JSONObject requestInput = (JSONObject) requestInputs.get(y);
									requestInput.put("domains", domains);
								}
							}
						}
						for(int x=0; x<servicesFiles.length;x++) {
							String serviceFile = servicesFiles[x].getName();
							if(serviceFile.contains(file2)) {
								parser = new JSONParser();
								obj = parser.parse(new FileReader(servicesFiles[x]));
								JSONObject jsonService = (JSONObject) obj;
								JSONArray domains = (JSONArray) jsonService.get("domains");
								JSONArray requestOutputs = (JSONArray) jsonRequest.get("outputs");
								for(int y=0; y<requestOutputs.size();y++) {
									JSONObject requestOutput = (JSONObject) requestOutputs.get(y);
									requestOutput.put("domains", domains);
								}
							}
						}
					}else {
						File[] servicesFiles = new File("../../Data/services-dataset/iot-services/").listFiles();
						for(int x=0; x<servicesFiles.length;x++) {
							String serviceFile = servicesFiles[x].getName();
							String file1=id.replace("_request", "");
							if(serviceFile.contains(file1)) {
								parser = new JSONParser();
								obj = parser.parse(new FileReader(servicesFiles[x]));
								JSONObject jsonService = (JSONObject) obj;
								JSONArray domains = (JSONArray) jsonService.get("domains");
								JSONArray requestInputs = (JSONArray) jsonRequest.get("inputs");
								JSONArray requestOutputs = (JSONArray) jsonRequest.get("outputs");
								for(int y=0; y<requestInputs.size();y++) {
									JSONObject requestInput = (JSONObject) requestInputs.get(y);
									requestInput.put("domains", domains);
								}
								for(int y=0; y<requestOutputs.size();y++) {
									JSONObject requestOutput = (JSONObject) requestOutputs.get(y);
									requestOutput.put("domains", domains);
								}
							}
						}
					}
					
					FileWriter f = new FileWriter(files[j].getAbsoluteFile());
					try (Writer writer = f) {
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
						gson.toJson(jsonRequest, writer);
					}
					System.out.println("Successfully written service request file: " + files[j].getAbsoluteFile());
					
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
	}
}
