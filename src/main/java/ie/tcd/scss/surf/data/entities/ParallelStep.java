/**
 * Author: Christian Cabrera
 * This class represents a parallel step in a plan
 */

package ie.tcd.scss.surf.data.entities;

import java.util.ArrayList;

public class ParallelStep extends ComposedStep {
	private ArrayList<ServiceParameter> solvedOutputs = new ArrayList<ServiceParameter>();
	private ArrayList<ServiceParameter> remainingOutputs = new ArrayList<ServiceParameter>();

	public ParallelStep() {
		super();
	}

	public ParallelStep(ArrayList<ServiceParameter> inputs, ArrayList<ServiceParameter> outputs, int order, ArrayList<Step> steps) {
		super(inputs, outputs, order, "ParallelStep", steps);
		this.setSteps(steps);
	}

	public ArrayList<ServiceParameter> getSolvedOutputs() {
		return solvedOutputs;
	}

	public void setSolvedOutputs(ArrayList<ServiceParameter> solvedOutputs) {
		this.solvedOutputs = solvedOutputs;
	}

	public ArrayList<ServiceParameter> getRemainingOutputs() {
		return remainingOutputs;
	}

	public void setRemainingOutputs(ArrayList<ServiceParameter> remainingOutputs) {
		this.remainingOutputs = remainingOutputs;
	}
}
