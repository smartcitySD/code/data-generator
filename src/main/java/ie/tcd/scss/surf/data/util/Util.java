/**
 * Author: Christian Cabrera
 * This class implements different functionalities/utilities that can be used across the experiment
 */

package ie.tcd.scss.surf.data.util;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ie.tcd.scss.surf.data.entities.Aggregation;
import ie.tcd.scss.surf.data.entities.Condition;
import ie.tcd.scss.surf.data.entities.Coordinate;
import ie.tcd.scss.surf.data.entities.DataReporting;
import ie.tcd.scss.surf.data.entities.Domain;
import ie.tcd.scss.surf.data.entities.Edge;
import ie.tcd.scss.surf.data.entities.Enumeration;
import ie.tcd.scss.surf.data.entities.Location;
import ie.tcd.scss.surf.data.entities.NegotiationConstraint;
import ie.tcd.scss.surf.data.entities.ParallelStep;
import ie.tcd.scss.surf.data.entities.Plan;
import ie.tcd.scss.surf.data.entities.PlanGraph;
import ie.tcd.scss.surf.data.entities.Provider;
import ie.tcd.scss.surf.data.entities.QoSParameter;
import ie.tcd.scss.surf.data.entities.Range;
import ie.tcd.scss.surf.data.entities.RequestDescription;
import ie.tcd.scss.surf.data.entities.SamplingPeriod;
import ie.tcd.scss.surf.data.entities.SequentialStep;
import ie.tcd.scss.surf.data.entities.ServiceDescription;
import ie.tcd.scss.surf.data.entities.ServiceParameter;
import ie.tcd.scss.surf.data.entities.SimpleStep;
import ie.tcd.scss.surf.data.entities.Step;
import ie.tcd.scss.surf.data.entities.Temporality;
import ie.tcd.scss.surf.data.entities.Type;
import ie.tcd.scss.surf.data.entities.Vertex;

public class Util {
	
	public Util() {

	}

	/**
	 * Parse a json object to a Provider object
	 * 
	 * @param JSONObject
	 * @return Provider
	 */
	public static Provider parseJsonToProvider(JSONObject jsonObject) {
		Provider provider = new Provider();
		if (jsonObject.containsKey("providerId"))
			provider.setProviderId((String) jsonObject.get("providerId"));
		if (jsonObject.containsKey("name"))
			provider.setName((String) jsonObject.get("name"));
		if (jsonObject.containsKey("url"))
			provider.setUrl((String) jsonObject.get("url"));
		if (jsonObject.containsKey("type"))
			provider.setType((String) jsonObject.get("type"));
		return provider;
	}

	/**
	 * Parse a json object to a Type object
	 * 
	 * @param JSONObject
	 * @return Type
	 */
	public static Type parseJsonToType(JSONObject jsonObject) {
		Type type = new Type();
		if (jsonObject.containsKey("physicalProcess"))
			type.setPhysicalProcess((String) jsonObject.get("physicalProcess"));
		if (jsonObject.containsKey("direction"))
			type.setDirection((String) jsonObject.get("direction"));
		if (jsonObject.containsKey("dataType"))
			type.setDataType((String) jsonObject.get("dataType"));
		if (jsonObject.containsKey("unit"))
			type.setUnit((String) jsonObject.get("unit"));
		return type;
	}

	/**
	 * Parse a json object to a NegotiationConstraint object
	 * 
	 * @param JSONObject
	 * @return NegotiationConstraint
	 */
	public static NegotiationConstraint parseJsonToNegotiationConstraint(JSONObject jsonObject) {
		if (jsonObject.containsKey("min")) {
			Range range = new Range();
			range.setMin((String) jsonObject.get("min"));
			if (jsonObject.containsKey("type"))
				range.setType((String) jsonObject.get("type"));
			if (jsonObject.containsKey("max"))
				range.setType((String) jsonObject.get("max"));
			return range;
		} else {
			Enumeration enumeration = new Enumeration();
			if (jsonObject.containsKey("type"))
				enumeration.setType((String) jsonObject.get("type"));
			if (jsonObject.containsKey("values")) {
				JSONArray v = (JSONArray) jsonObject.get("values");
				String[] values = new String[v.size()];
				for (int j = 0; j < values.length; j++) {
					values[j] = (String) v.get(j);
				}
				enumeration.setValues(values);
			}
			return enumeration;
		}
	}

	/**
	 * Parse a json object to a SamplingPeriod object
	 * 
	 * @param JSONObject
	 * @return SamplingPeriod
	 */
	public static SamplingPeriod parseJsonToSamplingPeriod(JSONObject jsonObject) {
		SamplingPeriod samplingPeriod = new SamplingPeriod();
		if (jsonObject.containsKey("value"))
			samplingPeriod.setValue((long) jsonObject.get("value"));
		if (jsonObject.containsKey("description"))
			samplingPeriod.setDescription((String) jsonObject.get("description"));
		if (jsonObject.containsKey("unit"))
			samplingPeriod.setUnit((String) jsonObject.get("unit"));
		if (jsonObject.containsKey("negotiable"))
			samplingPeriod.setNegotiable((String) jsonObject.get("negotiable"));
		if (jsonObject.containsKey("negotiationConstraints")) {
			samplingPeriod.setNegotiationConstraints(
					parseJsonToNegotiationConstraint((JSONObject) jsonObject.get("negotiationConstraints")));
		}
		return samplingPeriod;
	}

	/**
	 * Parse a json array to an array list of SamplingPeriod objects
	 * 
	 * @param JSONArray
	 * @return ArrayList<SamplingPeriod>
	 */
	public static ArrayList<SamplingPeriod> parseArrayToSamplingPeriodList(JSONArray jsonArray) {
		ArrayList<SamplingPeriod> samplingPeriods = new ArrayList<SamplingPeriod>();
		for (int i = 0; i < jsonArray.size(); i++) {
			samplingPeriods.add(parseJsonToSamplingPeriod((JSONObject) jsonArray.get(i)));
		}
		return samplingPeriods;
	}

	/**
	 * Parse a json object to a Temporality object
	 * 
	 * @param JSONObject
	 * @return Temporality
	 */
	public static Temporality parseJsonToTemporality(JSONObject jsonObject) {
		Temporality temporality = new Temporality();
		if (jsonObject.containsKey("value"))
			temporality.setStart((String) jsonObject.get("start"));
		if (jsonObject.containsKey("description"))
			temporality.setDuration((Long) jsonObject.get("duration"));
		if (jsonObject.containsKey("unit"))
			temporality.setUnit((String) jsonObject.get("unit"));
		if (jsonObject.containsKey("negotiable"))
			temporality.setNegotiable((String) jsonObject.get("negotiable"));
		if (jsonObject.containsKey("negotiationConstraints")) {
			temporality.setNegotiationConstraints(
					parseJsonToNegotiationConstraint((JSONObject) jsonObject.get("negotiationConstraints")));
		}
		return temporality;
	}

	/**
	 * Parse a json array to an array list of Temporality objects
	 * 
	 * @param JSONArray
	 * @return ArrayList<Temporality>
	 */
	public static ArrayList<Temporality> parseArrayToTemporalityList(JSONArray jsonArray) {
		ArrayList<Temporality> temporalities = new ArrayList<Temporality>();
		for (int i = 0; i < jsonArray.size(); i++) {
			temporalities.add(parseJsonToTemporality((JSONObject) jsonArray.get(i)));
		}
		return temporalities;
	}

	/**
	 * Parse a json object to a Coordinate object
	 * 
	 * @param JSONObject
	 * @return Coordinate
	 */
	public static Coordinate parseJsonToCoordinate(JSONObject jsonObject) {
		Coordinate coordinate = new Coordinate();
		if (jsonObject.containsKey("x"))
			coordinate.setX((double) jsonObject.get("x"));
		if (jsonObject.containsKey("y"))
			coordinate.setY((double) jsonObject.get("y"));
		if (jsonObject.containsKey("z"))
			coordinate.setZ((double) jsonObject.get("z"));
		return coordinate;
	}

	/**
	 * Parse a json object to a Location object
	 * 
	 * @param JSONObject
	 * @return Location
	 */
	public static Location parseJsonToLocation(JSONObject jsonObject) {
		Location location = new Location();
		if (jsonObject.containsKey("description"))
			location.setDescription((String) jsonObject.get("description"));
		if (jsonObject.containsKey("region"))
			location.setRegion((String) jsonObject.get("region"));
		if (jsonObject.containsKey("negotiable"))
			location.setNegotiable((String) jsonObject.get("negotiable"));
		if (jsonObject.containsKey("negotiationConstraints")) {
			location.setNegotiationConstraints(
					parseJsonToNegotiationConstraint((JSONObject) jsonObject.get("negotiationConstraints")));
		}
		if (jsonObject.containsKey("coordinates"))
			location.setCoordinate(parseJsonToCoordinate((JSONObject) jsonObject.get("coordinates")));
		return location;
	}

	/**
	 * Parse a json object to a Domain object
	 * 
	 * @param JSONObject
	 * @return Domain
	 */
	public static Domain parseJsonToDomain(JSONObject jsonObject) {
		Domain domain = new Domain();
		if (jsonObject.containsKey("name"))
			domain.setName((String) jsonObject.get("name"));
		if (jsonObject.containsKey("url"))
			domain.setUrl((String) jsonObject.get("url"));
		return domain;
	}

	/**
	 * Parse a json array to an array list of Domain objects
	 * 
	 * @param JSONArray
	 * @return ArrayList<Domain>
	 */
	public static ArrayList<Domain> parseArrayToDomainList(JSONArray jsonArray) {
		ArrayList<Domain> domains = new ArrayList<Domain>();
		for (int i = 0; i < jsonArray.size(); i++) {
			domains.add(parseJsonToDomain((JSONObject) jsonArray.get(i)));
		}
		return domains;
	}

	/**
	 * Parse a json object to a QoSParameter object
	 * 
	 * @param JSONObject
	 * @return QoSParameter
	 */
	public static QoSParameter parseJsonToQoSParameter(JSONObject jsonObject) {
		QoSParameter qosParameter = new QoSParameter();
		if (jsonObject.containsKey("name"))
			qosParameter.setName((String) jsonObject.get("name"));
		if (jsonObject.containsKey("metric"))
			qosParameter.setMetric((String) jsonObject.get("metric"));
		if (jsonObject.containsKey("value"))
			qosParameter.setValue((double) jsonObject.get("value"));
		if (jsonObject.containsKey("unit"))
			qosParameter.setUnit((String) jsonObject.get("unit"));
		if (jsonObject.containsKey("negotiable"))
			qosParameter.setNegotiable((String) jsonObject.get("negotiable"));
		if (jsonObject.containsKey("expression"))
			qosParameter.setExpression((String) jsonObject.get("expression"));
		if (jsonObject.containsKey("weight"))
			qosParameter.setWeight((double) jsonObject.get("weight"));
		if (jsonObject.containsKey("negotiationConstraints")) {
			qosParameter.setNegotiationConstraints(
					parseJsonToNegotiationConstraint((JSONObject) jsonObject.get("negotiationConstraints")));
		}
		return qosParameter;
	}

	/**
	 * Parse a json array to an array list of QoSParameter objects
	 * 
	 * @param JSONArray
	 * @return ArrayList<QoSParameter>
	 */
	public static ArrayList<QoSParameter> parseArrayToQoSParameterList(JSONArray jsonArray) {
		ArrayList<QoSParameter> qosParameters = new ArrayList<QoSParameter>();
		for (int i = 0; i < jsonArray.size(); i++) {
			qosParameters.add(parseJsonToQoSParameter((JSONObject) jsonArray.get(i)));
		}
		return qosParameters;
	}

	/**
	 * Parse a json object to a ServiceParameter object
	 * 
	 * @param JSONObject
	 * @return ServiceParameter
	 */
	public static ServiceParameter parseJsonToServiceParameter(JSONObject jsonObject) {
		ServiceParameter serviceParameter = new ServiceParameter();
		if (jsonObject.containsKey("name"))
			serviceParameter.setName((String) jsonObject.get("name"));
		if (jsonObject.containsKey("type"))
			serviceParameter.setType((String) jsonObject.get("type"));
		if (jsonObject.containsKey("description"))
			serviceParameter.setDescription((String) jsonObject.get("description"));
		if (jsonObject.containsKey("unit"))
			serviceParameter.setUnit((String) jsonObject.get("unit"));
		if (jsonObject.containsKey("value"))
			serviceParameter.setValue((String) jsonObject.get("value"));
		return serviceParameter;
	}

	/**
	 * Parse a json array to an array list of ServiceParameter objects
	 * 
	 * @param JSONArray
	 * @return ArrayList<ServiceParameter>
	 */
	public static ArrayList<ServiceParameter> parseArrayToServiceParameterList(JSONArray jsonArray) {
		ArrayList<ServiceParameter> serviceParameters = new ArrayList<ServiceParameter>();
		for (int i = 0; i < jsonArray.size(); i++) {
			serviceParameters.add(parseJsonToServiceParameter((JSONObject) jsonArray.get(i)));
		}
		return serviceParameters;
	}

	/**
	 * Parse a json object to a Condition object
	 * 
	 * @param JSONObject
	 * @return Condition
	 */
	public static Condition parseJsonToCondition(JSONObject jsonObject) {
		Condition condition = new Condition();
		if (jsonObject.containsKey("type"))
			condition.setType((String) jsonObject.get("type"));
		if (jsonObject.containsKey("threshold"))
			condition.setThreshold((String) jsonObject.get("threshold"));
		return condition;
	}

	/**
	 * Parse a json object to a DataReporting object
	 * 
	 * @param JSONObject
	 * @return DataReporting
	 */
	public static DataReporting parseJsonToDataReporting(JSONObject jsonObject) {
		DataReporting dataReport = new DataReporting();
		if (jsonObject.containsKey("dataPeriod"))
			dataReport.setDataPeriod((String) jsonObject.get("dataPeriod"));
		if (jsonObject.containsKey("condition")) {
			dataReport.setCondition(parseJsonToCondition((JSONObject) jsonObject.get("condition")));
		}
		return dataReport;
	}

	/**
	 * Parse a json array to an array list of DataReporting objects
	 * 
	 * @param JSONArray
	 * @return ArrayList<DataReporting>
	 */
	public static ArrayList<DataReporting> parseArrayToDataReportingList(JSONArray jsonArray) {
		ArrayList<DataReporting> dataReportings = new ArrayList<DataReporting>();
		for (int i = 0; i < jsonArray.size(); i++) {
			dataReportings.add(parseJsonToDataReporting((JSONObject) jsonArray.get(i)));
		}
		return dataReportings;
	}

	/**
	 * Parse a json object to a Aggregation object
	 * 
	 * @param JSONObject
	 * @return Aggregation
	 */
	public static Aggregation parseJsonToAggregation(JSONObject jsonObject) {
		Aggregation aggregation = new Aggregation();
		if (jsonObject.containsKey("function"))
			aggregation.setFunction((String) jsonObject.get("function"));
		if (jsonObject.containsKey("perNode"))
			aggregation.setPerNode((String) jsonObject.get("perNode"));
		if (jsonObject.containsKey("window"))
			aggregation.setWindow((String) jsonObject.get("window"));
		return aggregation;
	}

	/**
	 * Parse a json array to an array list of Step objects
	 * 
	 * @param JSONArray
	 * @return ArrayList<DataReporting>
	 */
	public static ArrayList<Step> parseArrayToStepList(JSONArray jsonArray) {
		ArrayList<Step> steps = new ArrayList<Step>();
		for (int i = 0; i < jsonArray.size(); i++) {
			steps.add(parseJsonToStep((JSONObject) jsonArray.get(i)));
		}
		return steps;
	}

	/**
	 * Parse a json object to a Aggregation object
	 * 
	 * @param JSONObject
	 * @return Aggregation
	 */
	public static Step parseJsonToStep(JSONObject jsonObject) {
		String type = (String) jsonObject.get("type");
		if (type.equals("SimpleStep")) {
			SimpleStep step = new SimpleStep();
			if (jsonObject.containsKey("order"))
				step.setOrder((int) jsonObject.get("order"));
			if (jsonObject.containsKey("type"))
				step.setType((String) jsonObject.get("type"));
			if (jsonObject.containsKey("inputs"))
				step.setInputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("inputs")));
			if (jsonObject.containsKey("outputs"))
				step.setOutputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("outputs")));
			if (jsonObject.containsKey("serviceDescription"))
				step.setService(parseJsonToServiceDescription((JSONObject) jsonObject.get("serviceDescription")));
			return step;
		}
		if (type.equals("SequentialStep")) {
			SequentialStep step = new SequentialStep();
			if (jsonObject.containsKey("order"))
				step.setOrder((int) jsonObject.get("order"));
			if (jsonObject.containsKey("type"))
				step.setType((String) jsonObject.get("type"));
			if (jsonObject.containsKey("inputs"))
				step.setInputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("inputs")));
			if (jsonObject.containsKey("outputs"))
				step.setOutputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("outputs")));
			if (jsonObject.containsKey("steps"))
				step.setSteps(parseArrayToStepList((JSONArray) jsonObject.get("steps")));
			return step;
		}
		if (type.equals("ParallelStep")) {
			ParallelStep step = new ParallelStep();
			if (jsonObject.containsKey("order"))
				step.setOrder((int) jsonObject.get("order"));
			if (jsonObject.containsKey("type"))
				step.setType((String) jsonObject.get("type"));
			if (jsonObject.containsKey("inputs"))
				step.setInputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("inputs")));
			if (jsonObject.containsKey("outputs"))
				step.setOutputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("outputs")));
			if (jsonObject.containsKey("steps"))
				step.setSteps(parseArrayToStepList((JSONArray) jsonObject.get("steps")));
			return step;
		}
		return null;
	}

	/**
	 * Parse a json object to a ServiceDescription object
	 * 
	 * @param JSONObject
	 * @return ServiceDescription
	 */
	public static ServiceDescription parseJsonToServiceDescription(JSONObject jsonObject) {
		ServiceDescription service = new ServiceDescription();
		if (jsonObject.containsKey("id"))
			service.setId((String) jsonObject.get("id"));
		if (jsonObject.containsKey("name"))
			service.setName((String) jsonObject.get("name"));
		if (jsonObject.containsKey("state"))
			service.setState((boolean) jsonObject.get("state"));
		if (jsonObject.containsKey("url"))
			service.setUrl((String) jsonObject.get("url"));
		if (jsonObject.containsKey("negotiable"))
			service.setNegotiable((String) jsonObject.get("negotiable"));
		if (jsonObject.containsKey("supportSLA"))
			service.setSupportSLA((String) jsonObject.get("supportSLA"));
		if (jsonObject.containsKey("description"))
			service.setDescription((String) jsonObject.get("description"));
		if (jsonObject.containsKey("provider"))
			service.setProvider(parseJsonToProvider((JSONObject) jsonObject.get("provider")));
		if (jsonObject.containsKey("type"))
			service.setType(parseJsonToType((JSONObject) jsonObject.get("type")));
		if (jsonObject.containsKey("samplingPeriods"))
			service.setSamplingPeriods(parseArrayToSamplingPeriodList((JSONArray) jsonObject.get("samplingPeriods")));
		if (jsonObject.containsKey("temporalities"))
			service.setTemporalities(parseArrayToTemporalityList((JSONArray) jsonObject.get("temporalities")));
		if (jsonObject.containsKey("location"))
			service.setLocation(parseJsonToLocation((JSONObject) jsonObject.get("location")));
		if (jsonObject.containsKey("domains"))
			service.setDomains(parseArrayToDomainList((JSONArray) jsonObject.get("domains")));
		if (jsonObject.containsKey("qosParameters"))
			service.setQosParameters(parseArrayToQoSParameterList((JSONArray) jsonObject.get("qosParameters")));
		if (jsonObject.containsKey("inputs"))
			service.setInputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("inputs")));
		if (jsonObject.containsKey("outputs"))
			service.setOutputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("outputs")));
		if (jsonObject.containsKey("dataReporting"))
			service.setDataReporting(parseArrayToDataReportingList((JSONArray) jsonObject.get("dataReporting")));
		if (jsonObject.containsKey("aggregation"))
			service.setAggregation(parseJsonToAggregation((JSONObject) jsonObject.get("aggregation")));
		return service;
	}

	/**
	 * Parse a json object to a RequestDescription object
	 * 
	 * @param JSONObject
	 * @return RequestDescription
	 */
	public static RequestDescription parseJsonToRequestDescription(JSONObject jsonObject) {
		RequestDescription request = new RequestDescription();
		if (jsonObject.containsKey("userId"))
			request.setUserId((String) jsonObject.get("userId"));
		if (jsonObject.containsKey("location"))
			request.setLocation(parseJsonToLocation((JSONObject) jsonObject.get("location")));
		if (jsonObject.containsKey("domains"))
			request.setDomains(parseArrayToDomainList((JSONArray) jsonObject.get("domains")));
		if (jsonObject.containsKey("qosParameters"))
			request.setQosParameters(parseArrayToQoSParameterList((JSONArray) jsonObject.get("qosParameters")));
		if (jsonObject.containsKey("inputs"))
			request.setInputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("inputs")));
		if (jsonObject.containsKey("outputs"))
			request.setOutputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("outputs")));
		return request;
	}

	/**
	 * Parse a json object to a Plan object
	 * 
	 * @param JSONObject
	 * @return Plan
	 */
	public static Plan parseJsonToPlan(JSONObject jsonObject) {
		Plan plan = new Plan();
		if (jsonObject.containsKey("state"))
			plan.setState((int) jsonObject.get("state"));
		if (jsonObject.containsKey("domains"))
			plan.setDomains(parseArrayToDomainList((JSONArray) jsonObject.get("domains")));
		if (jsonObject.containsKey("inputs"))
			plan.setInputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("inputs")));
		if (jsonObject.containsKey("outputs"))
			plan.setOutputs(parseArrayToServiceParameterList((JSONArray) jsonObject.get("outputs")));
		if (jsonObject.containsKey("steps"))
			plan.setSteps(parseArrayToStepList((JSONArray) jsonObject.get("steps")));
		return plan;
	}

	/**
	 * Parse a json array to an array list of Plan objects
	 * 
	 * @param JSONArray
	 * @return ArrayList<Plan>
	 */
	public static ArrayList<Plan> parseArrayToPlanList(JSONArray jsonArray) {
		ArrayList<Plan> plans = new ArrayList<Plan>();
		for (int i = 0; i < jsonArray.size(); i++) {
			plans.add(parseJsonToPlan((JSONObject) jsonArray.get(i)));
		}
		return plans;
	}

	/**
	 * Parse a Provider object to a json object
	 * 
	 * @param Provider
	 * @return JSONObject
	 */
	public static JSONObject parseProviderToJson(Provider provider) {
		JSONObject jsonObject = new JSONObject();
		if (!provider.getName().isEmpty())
			jsonObject.put("name", provider.getName());
		if (!provider.getProviderId().isEmpty())
			jsonObject.put("providerId", provider.getProviderId());
		if (!provider.getType().isEmpty())
			jsonObject.put("type", provider.getType());
		if (!provider.getUrl().isEmpty())
			jsonObject.put("url", provider.getUrl());
		return jsonObject;
	}

	/**
	 * Parse a Type object to a json object
	 * 
	 * @param Type
	 * @return JSONObject
	 */
	public static JSONObject parseTypeToJson(Type type) {
		JSONObject jsonObject = new JSONObject();
		if (!type.getPhysicalProcess().isEmpty())
			jsonObject.put("physicalProcess", type.getPhysicalProcess());
		if (!type.getDataType().isEmpty())
			jsonObject.put("dataType", type.getDataType());
		if (!type.getDirection().isEmpty())
			jsonObject.put("direction", type.getDirection());
		if (!type.getUnit().isEmpty())
			jsonObject.put("unit", type.getUnit());
		return jsonObject;
	}

	/**
	 * Parse a NegotiationConstraint object to a json object
	 * 
	 * @param NegotiationConstraint
	 * @return JSONObject
	 */
	public static JSONObject parseNegotiationConstraintToJson(NegotiationConstraint negotiationConstraint) {
		JSONObject jsonObject = new JSONObject();
		if (negotiationConstraint.getClass() == Range.class) {
			Range range = (Range) negotiationConstraint;
			jsonObject.put("min", range.getMin());
			jsonObject.put("max", range.getMax());
		} else {
			if (negotiationConstraint.getClass() == Enumeration.class) {
				Enumeration enumeration = (Enumeration) negotiationConstraint;
				JSONArray values = new JSONArray();
				for (int j = 0; j < enumeration.getValues().length; j++) {
					values.add(enumeration.getValues()[j]);
				}
				jsonObject.put("values", values);
			}
		}
		return jsonObject;
	}

	/**
	 * Parse a SamplingPeriod object to a json object
	 * 
	 * @param SamplingPeriod
	 * @return JSONObject
	 */
	public static JSONObject parseSamplingPeriodToJson(SamplingPeriod samplingPeriod) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("value", samplingPeriod.getValue());
		if (!samplingPeriod.getDescription().isEmpty())
			jsonObject.put("description", samplingPeriod.getDescription());
		if (!samplingPeriod.getUnit().isEmpty())
			jsonObject.put("unit", samplingPeriod.getUnit());
		if (!samplingPeriod.getNegotiable().isEmpty())
			jsonObject.put("negotiable", samplingPeriod.getNegotiable());
		if (samplingPeriod.getNegotiationConstraints() != null) {
			jsonObject.put("negotiationConstraints",
					parseNegotiationConstraintToJson(samplingPeriod.getNegotiationConstraints()));
		}
		return jsonObject;
	}

	/**
	 * Parse a SamplingPeriod array list to a json array
	 * 
	 * @param ArrayList<SamplingPeriod>
	 * @return JSONArray
	 */
	public static JSONArray parseSamplingPeriodListToArray(ArrayList<SamplingPeriod> samplingPeriods) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < samplingPeriods.size(); i++) {
			jsonArray.add(parseSamplingPeriodToJson(samplingPeriods.get(i)));
		}
		return jsonArray;
	}

	/**
	 * Parse a Temporality object to a json object
	 * 
	 * @param Temporality
	 * @return JSONObject
	 */
	public static JSONObject parseTemporalityToJson(Temporality temporality) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("start", temporality.getStart());
		jsonObject.put("duration", temporality.getDuration());
		if (!temporality.getUnit().isEmpty())
			jsonObject.put("unit", temporality.getUnit());
		if (!temporality.getNegotiable().isEmpty())
			jsonObject.put("negotiable", temporality.getNegotiable());

		if (temporality.getNegotiationConstraints() != null) {
			jsonObject.put("negotiationConstraints",
					parseNegotiationConstraintToJson(temporality.getNegotiationConstraints()));
		}
		return jsonObject;
	}

	/**
	 * Parse a Temporality array list to a json array
	 * 
	 * @param ArrayList<Temporality>
	 * @return JSONArray
	 */
	public static JSONArray parseTemporalityListToArray(ArrayList<Temporality> temporalities) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < temporalities.size(); i++) {
			jsonArray.add(parseTemporalityToJson(temporalities.get(i)));
		}
		return jsonArray;
	}

	/**
	 * Parse a Coordinate object to a json object
	 * 
	 * @param Coordinate
	 * @return JSONObject
	 */
	public static JSONObject parseCoordinateToJson(Coordinate coordinate) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("x", coordinate.getX());
		jsonObject.put("y", coordinate.getY());
		jsonObject.put("z", coordinate.getZ());
		return jsonObject;
	}


	/**
	 * Parse a Location object to a json object
	 * 
	 * @param Location
	 * @return JSONObject
	 */
	public static JSONObject parseLocationToJson(Location location) {
		JSONObject jsonObject = new JSONObject();
		if (!location.getDescription().isEmpty())
			jsonObject.put("description", location.getDescription());
		if (!location.getNegotiable().isEmpty())
			jsonObject.put("negotiable", location.getNegotiable());
		if (!location.getRegion().isEmpty())
			jsonObject.put("region", location.getRegion());
		if (location.getCoordinate() != null)
			jsonObject.put("coordinates", parseCoordinateToJson(location.getCoordinate()));
		if (location.getNegotiationConstraints() != null)
			jsonObject.put("negotiationConstraints",parseNegotiationConstraintToJson(location.getNegotiationConstraints()));
		return jsonObject;
	}

	/**
	 * Parse a Domain object to a json object
	 * 
	 * @param Domain
	 * @return JSONObject
	 */
	public static JSONObject parseDomainToJson(Domain domain) {
		JSONObject jsonObject = new JSONObject();
		if (!domain.getName().isEmpty())
			jsonObject.put("name", domain.getName());
		if (!domain.getUrl().isEmpty())
			jsonObject.put("url", domain.getUrl());
		return jsonObject;
	}

	/**
	 * Parse a Domain array list to a json array
	 * 
	 * @param ArrayList<Domain>
	 * @return JSONArray
	 */
	public static JSONArray parseDomainListToArray(ArrayList<Domain> domains) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < domains.size(); i++) {
			jsonArray.add(parseDomainToJson(domains.get(i)));
		}
		return jsonArray;
	}

	/**
	 * Parse a QoSParameter object to a json object
	 * 
	 * @param QoSParameter
	 * @return JSONObject
	 */
	public static JSONObject parseQoSParameterToJson(QoSParameter qosParameter) {
		JSONObject jsonObject = new JSONObject();
		if (!qosParameter.getName().isEmpty())
			jsonObject.put("name", qosParameter.getName());
		if (!qosParameter.getMetric().isEmpty())
			jsonObject.put("metric", qosParameter.getMetric());
		if (!qosParameter.getExpression().isEmpty())
			jsonObject.put("expression", qosParameter.getExpression());
		if (!qosParameter.getNegotiable().isEmpty())
			jsonObject.put("negotiable", qosParameter.getNegotiable());
		if (!qosParameter.getUnit().isEmpty())
			jsonObject.put("unit", qosParameter.getUnit());
		jsonObject.put("value", qosParameter.getValue());
		jsonObject.put("weight", qosParameter.getWeight());

		if (qosParameter.getNegotiationConstraints() != null) {
			jsonObject.put("negotiationConstraints",
					parseNegotiationConstraintToJson(qosParameter.getNegotiationConstraints()));
		}
		return jsonObject;
	}

	/**
	 * Parse a QoSParameter array list to a json array
	 * 
	 * @param ArrayList<QoSParameter>
	 * @return JSONArray
	 */
	public static JSONArray parseQoSParameterListToArray(ArrayList<QoSParameter> qosParameters) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < qosParameters.size(); i++) {
			jsonArray.add(parseQoSParameterToJson(qosParameters.get(i)));
		}
		return jsonArray;
	}

	/**
	 * Parse a ServiceParameter object to a json object
	 * 
	 * @param ServiceParameter
	 * @return JSONObject
	 */
	public static JSONObject parseServiceParameterToJson(ServiceParameter serviceParameter) {
		JSONObject jsonObject = new JSONObject();
		if (!serviceParameter.getName().isEmpty())
			jsonObject.put("name", serviceParameter.getName());
		if (!serviceParameter.getType().isEmpty())
			jsonObject.put("type", serviceParameter.getType());
		if (!serviceParameter.getDescription().isEmpty())
			jsonObject.put("description", serviceParameter.getDescription());
		if (!serviceParameter.getUnit().isEmpty())
			jsonObject.put("unit", serviceParameter.getUnit());
		if (!serviceParameter.getValue().isEmpty())
			jsonObject.put("value", serviceParameter.getValue());
		return jsonObject;
	}

	/**
	 * Parse a ServiceParameter array list to a json array
	 * 
	 * @param ArrayList<ServiceParameter>
	 * @return JSONArray
	 */
	public static JSONArray parseServiceParameterListToArray(ArrayList<ServiceParameter> serviceParameters) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < serviceParameters.size(); i++) {
			jsonArray.add(parseServiceParameterToJson(serviceParameters.get(i)));
		}
		return jsonArray;
	}

	/**
	 * Parse a Condition object to a json object
	 * 
	 * @param Condition
	 * @return JSONObject
	 */
	public static JSONObject parseConditionToJson(Condition condition) {
		JSONObject jsonObject = new JSONObject();
		if (!condition.getType().isEmpty())
			jsonObject.put("type", condition.getType());
		if (!condition.getThreshold().isEmpty())
			jsonObject.put("threshold", condition.getThreshold());
		return jsonObject;
	}

	/**
	 * Parse a DataReporting object to a json object
	 * 
	 * @param DataReporting
	 * @return JSONObject
	 */
	public static JSONObject parseDataReportingToJson(DataReporting dataReporting) {
		JSONObject jsonObject = new JSONObject();
		if (!dataReporting.getDataPeriod().isEmpty())
			jsonObject.put("dataPeriod", dataReporting.getDataPeriod());
		if (!dataReporting.getCondition().getType().isEmpty()
				|| !dataReporting.getCondition().getThreshold().isEmpty()) {
			jsonObject.put("condition", parseConditionToJson(dataReporting.getCondition()));
		}
		return jsonObject;
	}

	/**
	 * Parse a DataReporting array list to a json array
	 * 
	 * @param ArrayList<DataReporting>
	 * @return JSONArray
	 */
	public static JSONArray parseDataReportingListToArray(ArrayList<DataReporting> dataReportings) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < dataReportings.size(); i++) {
			jsonArray.add(parseDataReportingToJson(dataReportings.get(i)));
		}
		return jsonArray;
	}

	/**
	 * Parse a Aggregation object to a json object
	 * 
	 * @param Aggregation
	 * @return JSONObject
	 */
	public static JSONObject parseAggregationToJson(Aggregation aggregation) {
		JSONObject jsonObject = new JSONObject();
		if (!aggregation.getFunction().isEmpty())
			jsonObject.put("function", aggregation.getFunction());
		if (!aggregation.getPerNode().isEmpty())
			jsonObject.put("perNode", aggregation.getPerNode());
		if (!aggregation.getWindow().isEmpty())
			jsonObject.put("window", aggregation.getWindow());
		return jsonObject;
	}

	/**
	 * Parse a ServiceDescription object to a json object
	 * 
	 * @param ServiceDescription
	 * @return JSONObject
	 */
	public static JSONObject parseServiceDescriptionToJson(ServiceDescription service) {
		JSONObject jsonObject = new JSONObject();
		if (!service.getId().isEmpty())
			jsonObject.put("id", service.getId());
		if (!service.getName().isEmpty())
			jsonObject.put("name", service.getName());
		jsonObject.put("state", service.isState());
		if (!service.getUrl().isEmpty())
			jsonObject.put("url", service.getUrl());
		if (!service.getNegotiable().isEmpty())
			jsonObject.put("negotiable", service.getNegotiable());
		if (!service.getSupportSLA().isEmpty())
			jsonObject.put("supportSLA", service.getSupportSLA());
		if (!service.getDescription().isEmpty())
			jsonObject.put("description", service.getDescription());
		if (!service.getProvider().getName().isEmpty() || !service.getProvider().getProviderId().isEmpty()
				|| !service.getProvider().getType().isEmpty() || !service.getProvider().getUrl().isEmpty())
			jsonObject.put("provider", parseProviderToJson(service.getProvider()));
		if (!service.getType().getPhysicalProcess().isEmpty() || !service.getType().getDataType().isEmpty()
				|| !service.getType().getDirection().isEmpty() || !service.getType().getUnit().isEmpty())
			jsonObject.put("type", parseTypeToJson(service.getType()));
		if (!service.getSamplingPeriods().isEmpty())
			jsonObject.put("samplingPeriods", parseSamplingPeriodListToArray(service.getSamplingPeriods()));
		if (!service.getTemporalities().isEmpty())
			jsonObject.put("temporalities", parseTemporalityListToArray(service.getTemporalities()));
		if (!service.getLocation().getDescription().isEmpty() || !service.getLocation().getNegotiable().isEmpty()
				|| !service.getLocation().getRegion().isEmpty())
			jsonObject.put("location", parseLocationToJson(service.getLocation()));
		if (!service.getDomains().isEmpty())
			jsonObject.put("domains", parseDomainListToArray(service.getDomains()));
		if (!service.getQosParameters().isEmpty())
			jsonObject.put("qosParameters", parseQoSParameterListToArray(service.getQosParameters()));
		if (!service.getInputs().isEmpty())
			jsonObject.put("inputs", parseServiceParameterListToArray(service.getInputs()));
		if (!service.getOutputs().isEmpty())
			jsonObject.put("outputs", parseServiceParameterListToArray(service.getOutputs()));
		if (!service.getDataReporting().isEmpty()) 
			jsonObject.put("dataReportings", parseDataReportingListToArray(service.getDataReporting()));
				if (!service.getAggregation().getFunction().isEmpty() || !service.getAggregation().getPerNode().isEmpty()
				|| !service.getAggregation().getWindow().isEmpty())
			jsonObject.put("aggregation", parseAggregationToJson(service.getAggregation()));
		return jsonObject;
	}

	/**
	 * Parse a RequestDescription object to a json object
	 * 
	 * @param RequestDescription
	 * @return JSONObject
	 */
	public static JSONObject parseRequestDescriptionToJson(RequestDescription request) {
		JSONObject jsonObject = new JSONObject();
		if (!request.getUserId().isEmpty())
			jsonObject.put("userId", request.getUserId());
		if (!request.getLocation().getDescription().isEmpty() || !request.getLocation().getNegotiable().isEmpty()
				|| !request.getLocation().getRegion().isEmpty())
			jsonObject.put("location", request.getLocation());
		if (!request.getDomains().isEmpty())
			jsonObject.put("domains", parseDomainListToArray(request.getDomains()));
		if (!request.getQosParameters().isEmpty())
			jsonObject.put("qosParameters", parseQoSParameterListToArray(request.getQosParameters()));
		if (!request.getInputs().isEmpty())
			jsonObject.put("inputs", parseServiceParameterListToArray(request.getInputs()));
		if (!request.getOutputs().isEmpty())
			jsonObject.put("outputs", parseServiceParameterListToArray(request.getOutputs()));
		return jsonObject;
	}

	/**
	 * Get steps in a JSON format from a list of steps
	 */
	public static JSONArray parseStepListtoJSON(ArrayList<Step> steps) {
		JSONArray stepsJSON = new JSONArray();
		for (int j = 0; j < steps.size(); j++) {
			if (steps.get(j).getClass() == SimpleStep.class) {
				JSONObject simpleStep = new JSONObject();
				simpleStep.put("order", steps.get(j).getOrder());
				simpleStep.put("type", steps.get(j).getType());
				if (!steps.get(j).getInputs().isEmpty())
					simpleStep.put("inputs", parseServiceParameterListToArray(steps.get(j).getInputs()));
				if (!steps.get(j).getOutputs().isEmpty())
					simpleStep.put("outputs", parseServiceParameterListToArray(steps.get(j).getOutputs()));
				ServiceDescription service = ((SimpleStep) steps.get(j)).getService();
				simpleStep.put("serviceDescription", parseServiceDescriptionToJson(service));
				stepsJSON.add(simpleStep);
			}

			if (steps.get(j).getClass() == SequentialStep.class) {
				SequentialStep composedStep = (SequentialStep) steps.get(j);
				JSONObject sequentialStep = new JSONObject();
				sequentialStep.put("order", steps.get(j).getOrder());
				sequentialStep.put("type", steps.get(j).getType());
				if (!steps.get(j).getInputs().isEmpty())
					sequentialStep.put("inputs", parseServiceParameterListToArray(composedStep.getInputs()));
				if (!steps.get(j).getOutputs().isEmpty())
					sequentialStep.put("outputs", parseServiceParameterListToArray(composedStep.getOutputs()));
				JSONArray subSteps = parseStepListtoJSON(composedStep.getSteps());
				sequentialStep.put("steps", subSteps);
				stepsJSON.add(sequentialStep);
			}

			if (steps.get(j).getClass() == ParallelStep.class) {
				ParallelStep composedStep = (ParallelStep) steps.get(j);
				JSONObject parallelStep = new JSONObject();
				parallelStep.put("order", steps.get(j).getOrder());
				parallelStep.put("type", steps.get(j).getType());
				if (!steps.get(j).getInputs().isEmpty())
					parallelStep.put("inputs", parseServiceParameterListToArray(composedStep.getInputs()));
				if (!steps.get(j).getOutputs().isEmpty())
					parallelStep.put("outputs", parseServiceParameterListToArray(composedStep.getOutputs()));
				JSONArray subSteps = parseStepListtoJSON(composedStep.getSteps());
				parallelStep.put("steps", subSteps);
				stepsJSON.add(parallelStep);
			}
		}
		return stepsJSON;
	}

	/**
	 * Parse a Plan object to a json object
	 * 
	 * @param Plan
	 * @return JSONObject
	 */
	public static JSONObject parsePlanToJson(Plan plan) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("state", plan.getState());
		if (!plan.getInputs().isEmpty())
			jsonObject.put("inputs", parseServiceParameterListToArray(plan.getInputs()));
		if (!plan.getOutputs().isEmpty())
			jsonObject.put("outputs", parseServiceParameterListToArray(plan.getOutputs()));
		if (!plan.getDomains().isEmpty())
			jsonObject.put("domains", parseDomainListToArray(plan.getDomains()));
		if (!plan.getSteps().isEmpty())
			jsonObject.put("steps", parseStepListtoJSON(plan.getSteps()));
		return jsonObject;
	}


	/**
	 * Parses a list of plans to a JSONArray
	 */
	public static JSONObject parsePlanListtoJSON(ArrayList<Plan> plans) {
		JSONObject response = new JSONObject();
		JSONArray plansResponse = new JSONArray();
		for (int i = 0; i < plans.size(); i++) {
			plansResponse.add(parsePlanToJson(plans.get(i)));
		}
		response.put("plans", plansResponse);
		return response;
	}

	/**
	 * Backward planning execution time model
	 * 
	 * services * counter, where services is the size of the search space (i.e.,
	 * number of services) and counter is the times that the recursion was
	 * executed
	 */
	public static long backwardPlanningTime(int services, int counter) {
		return ((281 * services) + 71) * counter;
	}

	/**
	 * Orders a map according to its values
	 */
	public static Map<String, Double> sortByValue(Map<String, Double> merged) {

		List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(merged.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
			public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
		for (Map.Entry<String, Double> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	public static PlanGraph copyPlan(PlanGraph plan) {
		PlanGraph p = new PlanGraph();
		
		JSONArray currentInputs = copyJSONArray(plan.getCurrentInputs());
		p.setCurrentInputs(currentInputs);
		
		JSONArray domains = copyJSONArray(plan.getDomains());
		p.setDomains(domains);
		
		ArrayList<Edge> edges = new ArrayList<>();
		for(int i=0; i<plan.getEdges().size();i++)
			edges.add(copyEdge(plan.getEdges().get(i)));
		p.setEdges(edges);
		
		JSONArray inputs = copyJSONArray(plan.getInputs());
		p.setInputs(inputs);
		
		JSONArray outputs = copyJSONArray(plan.getOutputs());
		p.setOutputs(outputs);
		
		
		Map<String,Vertex> vertices = new HashMap<>();
		for(Map.Entry<String, Vertex> entry : plan.getVertices().entrySet())
			vertices.put(entry.getKey(), copyVertex(entry.getValue()));
		p.setVertices(vertices);
		
		p.setMark(plan.getMark());
		p.setState(plan.isState());
		
		return p;
	}

	public static Vertex copyVertex(Vertex vertex) {
		Vertex v = new Vertex();
		
		JSONArray domains = new JSONArray();
		for(int i=0; i<vertex.getDomains().size();i++){
			domains.add(vertex.getDomains().get(i));
		}
		v.setDomains(domains);
		
		JSONArray inputs = new JSONArray();
		for(int i=0; i<vertex.getInputs().size();i++){
			inputs.add(vertex.getInputs().get(i));
		}
		v.setInputs(inputs);
		
		JSONArray remainingInputs = new JSONArray();
		for(int i=0; i<vertex.getRemainingInputs().size();i++){
			remainingInputs.add(vertex.getRemainingInputs().get(i));
		}
		v.setRemainingInputs(remainingInputs);
		
		JSONArray outputs = new JSONArray();
		for(int i=0; i<vertex.getOutputs().size();i++){
			outputs.add(vertex.getOutputs().get(i));
		}
		v.setOutputs(outputs);
		
		v.setName(vertex.getName());
		v.setService(vertex.getService());
		v.setState(vertex.isState());
		v.setType(vertex.getType());
		
		return v;
	}
	
	public static Edge copyEdge(Edge edge) {
		Edge e = new Edge();
		
		Vertex source = copyVertex(edge.getSource()); 
		e.setSource(source);
		Vertex destination = copyVertex(edge.getDestination());
		e.setDestination(destination);
		e.setDegree(edge.getDegree());
		e.setLink(edge.getLink());
		
		return e;
	}

	public static JSONArray copyJSONArray(JSONArray original) {
		JSONArray copy = new JSONArray();
		for(int i=0;i<original.size();i++)
			copy.add(original.get(i));
		return copy;
	}
}
