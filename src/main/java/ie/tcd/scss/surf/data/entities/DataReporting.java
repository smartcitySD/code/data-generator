/**
 * Author: Christian Cabrera
 * Class that represents the data reporting of a service
 */

package ie.tcd.scss.surf.data.entities;

public class DataReporting {
	private String dataPeriod;
	private Condition condition;

	public DataReporting() {
		this.setDataPeriod("");
		this.setCondition(new Condition());
	}

	public DataReporting(String dataPeriod, Condition condition) {
		this.setDataPeriod("");
		this.setCondition(condition);
	}

	public String getDataPeriod() {
		return dataPeriod;
	}

	public void setDataPeriod(String dataPeriod) {
		this.dataPeriod = dataPeriod;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

}
