/**
 * Author: Christian Cabrera
 * Class that represents a service provider
 */

package ie.tcd.scss.surf.data.entities;

public class Provider {
	private String providerId;
	private String name;
	private String url;
	private String type;

	public Provider(String providerId, String name, String url, String type) {
		this.setProviderId(providerId);
		this.setName(name);
		this.setUrl(url);
		this.setType(type);
	}

	public Provider() {
		this.setProviderId("");
		this.setName("");
		this.setUrl("");
		this.setType("");
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
