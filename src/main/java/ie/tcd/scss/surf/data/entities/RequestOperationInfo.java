/**
 * Author: Christian Cabrera
 * This entity represents a request and stores the related data including response time
 */

package ie.tcd.scss.surf.data.entities;

import java.util.ArrayList;

import org.json.simple.JSONObject;

public class RequestOperationInfo {
	
	private long id;
	private RequestDescription request;
	private ArrayList<String> expectedResponse;
	private boolean state;
	private JSONObject jsonRequest;
	private ArrayList<JSONObject> responses;
	private ArrayList<Long> endTimes;
	private long startTime;
	
	public RequestOperationInfo(){
		setId(0);
		setExpectedResponse(new ArrayList<String>());
		setRequest(new RequestDescription());
		setState(false);
		setResponses(new ArrayList<JSONObject>());
		setEndTimes(new ArrayList<Long>());
		setStartTime(0);
		jsonRequest = new JSONObject();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public RequestDescription getRequest() {
		return request;
	}

	public void setRequest(RequestDescription request) {
		this.request = request;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public ArrayList<String> getExpectedResponse() {
		return expectedResponse;
	}

	public void setExpectedResponse(ArrayList<String> expectedResponse) {
		this.expectedResponse = expectedResponse;
	}

	public ArrayList<JSONObject> getResponses() {
		return responses;
	}

	public void setResponses(ArrayList<JSONObject> responses) {
		this.responses = responses;
	}

	public ArrayList<Long> getEndTimes() {
		return endTimes;
	}

	public void setEndTimes(ArrayList<Long> endTimes) {
		this.endTimes = endTimes;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public JSONObject getJsonRequest() {
		return jsonRequest;
	}

	public void setJsonRequest(JSONObject jsonRequest) {
		this.jsonRequest = jsonRequest;
	}

}
