/**
 * Author: Christian Cabrera
 * This class represents a sequential step in a plan
 */

package ie.tcd.scss.surf.data.entities;

import java.util.ArrayList;

public class SequentialStep extends ComposedStep {

	public SequentialStep() {
		super();
	}

	public SequentialStep(ArrayList<ServiceParameter> inputs, ArrayList<ServiceParameter> outputs, int order,
			ArrayList<Step> steps) {
		super(inputs, outputs, order, "SequentialStep", steps);
	}
}
