/**
 * Author: Christian Cabrera
 * Class that represents the condition for data reporting
 */

package ie.tcd.scss.surf.data.entities;

public class Condition {
	private String type;
	private String threshold;

	public Condition() {
		this.setType("");
		this.setThreshold("");
	}

	public Condition(String type, String threshold) {
		this.setType(type);
		this.setThreshold(threshold);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getThreshold() {
		return threshold;
	}

	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}

}
