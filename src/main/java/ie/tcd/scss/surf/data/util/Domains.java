/*
 * Copyright (c) 2005-2010 KOM - Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package ie.tcd.scss.surf.data.util;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;

public class Domains {
	private static Map<String,ArrayList<String>> domains;
	public Domains(){
		setDomains(new HashMap<String,ArrayList<String>>());
		this.loadDomains();
	}
	public static Map<String,ArrayList<String>> getDomains() {
		return domains;
	}
	public static void setDomains(Map<String,ArrayList<String>> domains) {
		Domains.domains = domains;
	}
	public void loadDomains(){
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		try{
			FileInputStream inputStream;
			inputStream = new FileInputStream("../../SmartCitySD/Data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
			m.read(inputStream,null);
			inputStream.close();
			OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
			ExtendedIterator<OntClass> ds = domain.listSubClasses();
			List<OntClass> domainsOntology = ds.toList();
			for(int w=0;w<domainsOntology.size();w++){
				OntClass d = domainsOntology.get(w);
				ExtendedIterator<OntResource> ins = (ExtendedIterator<OntResource>) d.listInstances(true);
				List<OntResource> domainInstances = ins.toList();
				for(int h=0;h<domainInstances.size();h++){
					if(!this.getDomains().containsKey(domainInstances.get(h).getURI())){
						ArrayList<String> superDomains = new ArrayList<>();
						ExtendedIterator<OntClass> superClasses = d.listSuperClasses();
						List<OntClass> superClassesDomain = superClasses.toList();
						for(int i=0; i<superClassesDomain.size();i++){
							if(!superDomains.contains(superClassesDomain.get(i).getURI()) && !superClassesDomain.get(i).getURI().equals("http://www.surf.scss.tcd.ie/cityOntology/#Domain") && !superClassesDomain.get(i).getURI().equals("http://www.w3.org/2000/01/rdf-schema#Resource"))
								superDomains.add(superClassesDomain.get(i).getURI());
						}
						superDomains.add(d.getURI());
						this.getDomains().put(domainInstances.get(h).getURI(),superDomains);
					}else{
						ArrayList<String> superDomains = this.getDomains().get(domainInstances.get(h).getURI());
						OntClass superClasses = d.getSuperClass();
						ExtendedIterator<OntClass> scs = superClasses.listSubClasses();
						List<OntClass> superClassesDomain = scs.toList();
						for(int i=0; i<superClassesDomain.size();i++){
							if(!superDomains.contains(superClassesDomain.get(i).getURI())){
								superDomains.add(superClassesDomain.get(i).getURI());
							}
						}
						if(!superDomains.contains(d.getURI())){ 
							superDomains.add(d.getURI());
						}
						this.getDomains().remove(domainInstances.get(h).getURI());
						this.getDomains().put(domainInstances.get(h).getURI(), superDomains);
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
