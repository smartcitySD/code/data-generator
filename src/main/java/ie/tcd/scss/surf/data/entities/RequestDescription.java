/**
 * Author: Christian Cabrera
 * This entity represents a request description in the data model
 *  
 */

package ie.tcd.scss.surf.data.entities;

import java.util.ArrayList;

public class RequestDescription {

	private String jobId;
	private String userId;
	private int opId;
	private ArrayList<ServiceParameter> inputs;
	private ArrayList<ServiceParameter> outputs;
	private ArrayList<Domain> domains;
	private ArrayList<QoSParameter> qosParameters;
	private Location location;

	public RequestDescription() {
		this.setJobId("");
		this.setInputs(new ArrayList<ServiceParameter>());
		this.setOutputs(new ArrayList<ServiceParameter>());
		this.setDomains(new ArrayList<Domain>());
		this.setQosParameters(new ArrayList<QoSParameter>());
		this.setUserId("");
		this.setOpId(0);
		this.setLocation(new Location());
	}

	public RequestDescription(String jobId, ArrayList<ServiceParameter> inputs, ArrayList<ServiceParameter> outputs,
			ArrayList<Domain> domains, ArrayList<QoSParameter> qosParameters, String userId, int opId,
			Location location) {
		this.setJobId(jobId);
		this.setInputs(inputs);
		this.setOutputs(outputs);
		this.setDomains(domains);
		this.setQosParameters(qosParameters);
		this.setUserId(userId);
		this.setOpId(opId);
		this.setLocation(location);
	}

	public ArrayList<ServiceParameter> getInputs() {
		return inputs;
	}

	public void setInputs(ArrayList<ServiceParameter> inputs) {
		this.inputs = inputs;
	}

	public ArrayList<ServiceParameter> getOutputs() {
		return outputs;
	}

	public void setOutputs(ArrayList<ServiceParameter> outputs) {
		this.outputs = outputs;
	}

	public ArrayList<QoSParameter> getQosParameters() {
		return qosParameters;
	}

	public void setQosParameters(ArrayList<QoSParameter> qosParameters) {
		this.qosParameters = qosParameters;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobId() {
		return jobId;
	}

	public ArrayList<Domain> getDomains() {
		return domains;
	}

	public void setDomains(ArrayList<Domain> domains) {
		this.domains = domains;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getOpId() {
		return opId;
	}

	public void setOpId(int opId) {
		this.opId = opId;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

}
