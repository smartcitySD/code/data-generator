/**
 * Author: Christian Cabrera
 * This entity represents a service description in the service model
 */

package ie.tcd.scss.surf.data.entities;

import java.util.ArrayList;

public class ServiceDescription {

	private String id;
	private String url;
	private String name;
	private String description;
	private Provider provider;
	private boolean state;
	private String negotiable;
	private String supportSLA;
	private Type type;
	private ArrayList<SamplingPeriod> samplingPeriods;
	private ArrayList<Temporality> temporalities;
	private Location location;
	private ArrayList<Domain> domains;
	private ArrayList<ServiceParameter> inputs;
	private ArrayList<ServiceParameter> outputs;
	private ArrayList<QoSParameter> qosParameters;
	private Aggregation aggregation;
	private ArrayList<DataReporting> dataReporting;

	public ServiceDescription() {
		this.setId("");
		this.setUrl("");
		this.setName("");
		this.setProvider(new Provider());
		this.setState(false);
		this.setNegotiable("");
		this.setSupportSLA("");
		this.setType(new Type());
		this.setSamplingPeriods(new ArrayList<SamplingPeriod>());
		this.setTemporalities(new ArrayList<Temporality>());
		this.setLocation(new Location());
		this.setInputs(new ArrayList<ServiceParameter>());
		this.setOutputs(new ArrayList<ServiceParameter>());
		this.setDomains(new ArrayList<Domain>());
		this.setQosParameters(new ArrayList<QoSParameter>());
		this.setAggregation(new Aggregation());
		this.setDataReporting(new ArrayList<DataReporting>());
	}

	public ServiceDescription(String id, Type type, String url, String name, String description, Provider provider,
			boolean state, String negotiable, String supportSLA, ArrayList<SamplingPeriod> samplingPeriods,
			ArrayList<Temporality> temporalities, Location location, ArrayList<ServiceParameter> inputs,
			ArrayList<ServiceParameter> outputs, ArrayList<Domain> domains, ArrayList<QoSParameter> qosParameters,
			Aggregation aggregation, ArrayList<DataReporting> dataReporting) {
		this.setId(id);
		this.setType(type);
		this.setUrl(url);
		this.setName(name);
		this.setDescription(description);
		this.setProvider(provider);
		this.setState(state);
		this.setNegotiable(negotiable);
		this.setSupportSLA(supportSLA);
		this.setSamplingPeriods(samplingPeriods);
		this.setTemporalities(temporalities);
		this.setLocation(location);
		this.setInputs(inputs);
		this.setOutputs(outputs);
		this.setDomains(domains);
		this.setQosParameters(qosParameters);
		this.setAggregation(aggregation);
		this.setDataReporting(dataReporting);
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public ArrayList<ServiceParameter> getInputs() {
		return inputs;
	}

	public void setInputs(ArrayList<ServiceParameter> inputs) {
		this.inputs = inputs;
	}

	public ArrayList<Domain> getDomains() {
		return domains;
	}

	public void setDomains(ArrayList<Domain> domains) {
		this.domains = domains;
	}

	public ArrayList<ServiceParameter> getOutputs() {
		return outputs;
	}

	public void setOutputs(ArrayList<ServiceParameter> outputs) {
		this.outputs = outputs;
	}

	public ArrayList<QoSParameter> getQosParameters() {
		return qosParameters;
	}

	public void setQosParameters(ArrayList<QoSParameter> qosParameters) {
		this.qosParameters = qosParameters;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public String getNegotiable() {
		return negotiable;
	}

	public void setNegotiable(String negotiable) {
		this.negotiable = negotiable;
	}

	public String getSupportSLA() {
		return supportSLA;
	}

	public void setSupportSLA(String supportSLA) {
		this.supportSLA = supportSLA;
	}

	public ArrayList<SamplingPeriod> getSamplingPeriods() {
		return samplingPeriods;
	}

	public void setSamplingPeriods(ArrayList<SamplingPeriod> samplingPeriods) {
		this.samplingPeriods = samplingPeriods;
	}

	public ArrayList<Temporality> getTemporalities() {
		return temporalities;
	}

	public void setTemporalities(ArrayList<Temporality> temporalities) {
		this.temporalities = temporalities;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Aggregation getAggregation() {
		return aggregation;
	}

	public void setAggregation(Aggregation aggregation) {
		this.aggregation = aggregation;
	}

	public ArrayList<DataReporting> getDataReporting() {
		return dataReporting;
	}

	public void setDataReporting(ArrayList<DataReporting> dataReporting) {
		this.dataReporting = dataReporting;
	}

}
