package ie.tcd.scss.surf.data.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ServiceDescriptionsGenerator {
	
	@SuppressWarnings("unchecked")
	public static void generateFromOWLS(){
		File folder = new File("../../Data/services-dataset/OWLS-TC4_SWRL/htdocs/domains/1.1/");
		File[] folders = folder.listFiles();
		int numberOfServices = 0;
		for(int i=0;i<folders.length;i++){
			System.out.println("Reading folder for domain: " + folders[i].getName());
			File[] files = folders[i].listFiles();
			System.out.println("Number of Files: " + files.length);
			for(int j=0;j<files.length;j++){
				System.out.println("Reading file: " + files[j].getName());
				File file = files[j];
				JSONObject serviceDescription = new JSONObject();
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db;
				try {
					db = dbf.newDocumentBuilder();
					Document document = db.parse(file);
					NodeList rdfNodes = document.getChildNodes();
					serviceDescription.put("file",files[j].getName());
					String id="";
					String url="";
					String name="";
					String desc="";
					JSONArray inputs = new JSONArray();
					JSONArray outputs = new JSONArray();
					
					for(int x=0; x<rdfNodes.getLength(); x++){
						NodeList nodes = rdfNodes.item(x).getChildNodes();
						
						for(int y=0; y<nodes.getLength();y++){
							Node node = nodes.item(y);
							if(node.getNodeName().equals("service:Service")){
								NamedNodeMap attributes = node.getAttributes();
								Node attribute = attributes.getNamedItem("rdf:ID");
								id = folders[i].getName()+"/"+attribute.getNodeValue().toLowerCase();
								url = "http://surf.scss.tcd.ie/_provider_/"+folders[i].getName()+"/"+attribute.getNodeValue().toLowerCase();
							}
							if(node.getNodeName().equals("profile:Profile")){
								NodeList ns = node.getChildNodes();
								for(int k=0; k<ns.getLength();k++){
									Node n = ns.item(k);
									if(n.getNodeName().equals("profile:serviceName")){
										name = n.getTextContent().replace("\n", "");;
									}
									if(n.getNodeName().equals("profile:textDescription")){
										desc = n.getTextContent().replace("\n", "");;
									}
								}
							}
							if(node.getNodeName().equals("process:Input")){
								NamedNodeMap attributes = node.getAttributes();
								Node attribute = attributes.getNamedItem("rdf:ID");
								JSONObject input = new JSONObject();
								input.put("name", attribute.getNodeValue());
								NodeList ns = node.getChildNodes();
								for(int k=0; k<ns.getLength();k++){
									Node n = ns.item(k);
									if(n.getNodeName().equals("process:parameterType")){
										input.put("type", n.getTextContent().replace("\n", ""));
									}
								}
								input.put("desc", "Input of service " + name);
								inputs.add(input);
							}
							if(node.getNodeName().equals("process:Output")){
								NamedNodeMap attributes = node.getAttributes();
								Node attribute = attributes.getNamedItem("rdf:ID");
								JSONObject output = new JSONObject();
								output.put("name", attribute.getNodeValue());
								NodeList ns = node.getChildNodes();
								for(int k=0; k<ns.getLength();k++){
									Node n = ns.item(k);
									if(n.getNodeName().equals("process:parameterType")){
										output.put("type", n.getTextContent().replace("\n", ""));
									}
								}
								output.put("desc", "Output of service " + name);
								outputs.add(output);
							}
						}
					}
					if(inputs.size()>0 && outputs.size()>0){
						serviceDescription.put("id", id);
						Double type = Math.random();
						JSONObject provider = new JSONObject();
						if(type<0.3333333){
							provider.put("type", "webservice");
						}
						if(type<0.6666666 && type>=0.3333333){
							provider.put("type", "asp");
						}
						if(type<1 && type>=0.6666666){
							provider.put("type", "wsdn");
						}
						serviceDescription.put("url", url);
						serviceDescription.put("name", name);
						serviceDescription.put("description", desc);
						serviceDescription.put("provider", provider);
						serviceDescription.put("state", true);
						serviceDescription.put("capability", "atomic");
						
						OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
						FileInputStream inputStream;
						inputStream = new FileInputStream("../../Data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
						m.read(inputStream,null);
						inputStream.close();
						switch (folders[i].getName()){
						case "communication":
							JSONArray domainsArray = new JSONArray();
							JSONObject d = new JSONObject();
							d.put("name", "Communications");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Communications");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "education":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Educational");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Educational");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "medical":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Health");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Health");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "simulation":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Buildings");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Buildings");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "weapon":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Government_and_Planning");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Government_and_Planning");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "food":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Food_and_Drinks");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Food_and_Drinks");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "geography":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Geography");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Geography");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "travel":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Travel");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Travel");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "vehicle":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Vehicle_Sale_and_Rent");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Vehicle_Sale_and_Rent");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "books":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Entertainment_Products");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Entertainment_Products");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "clothing":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Clothing");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Clothing");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "payment":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Payment");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Payment");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						case "technology":
							domainsArray = new JSONArray();
							d = new JSONObject();
							d.put("name", "Technology");
							d.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Technology");
							domainsArray.add(d);
							serviceDescription.put("domains", domainsArray);
							break;
						}
						serviceDescription.put("inputs", inputs);
						serviceDescription.put("outputs", outputs);
						/*JSONObject qosParameter = new JSONObject();
						Double qos = Math.random();
						if(qos<0.25){
							qosParameter.put("name", "availability");
							qosParameter.put("unit", "%");
							qosParameter.put("value", Math.random());
						}
						if(qos<0.5 && qos>=0.25){
							qosParameter.put("name", "responseTime");
							qosParameter.put("unit", "sec");
							qosParameter.put("value", Math.random() * 100);
						}
						if(qos<0.75 && qos>=0.5){
							qosParameter.put("name", "accuracy");
							qosParameter.put("unit", "%");
							qosParameter.put("value", Math.random());
						}
						if(qos<1 && qos>=0.75){
							qosParameter.put("name", "reliability");
							qosParameter.put("unit", "%");
							qosParameter.put("value", Math.random());
						}*/
						try{
							String newName = files[j].getName().replace("owls", "json");
							File fl = new File("../../Data/services-dataset/json-services/"+folders[i].getName()+"/"+newName);
							fl.getParentFile().mkdirs();
							FileWriter f = new FileWriter(fl.getAbsoluteFile());
							try (Writer writer = f) {
								Gson gson = new GsonBuilder().setPrettyPrinting().create();
								gson.toJson(serviceDescription, writer);
							}
							File fl2 = new File("../../Data/services-dataset/json-services/all/"+newName);
							fl2.getParentFile().mkdirs();
							FileWriter f2 = new FileWriter(fl2.getAbsoluteFile());
							try (Writer writer = f2) {
								Gson gson = new GsonBuilder().setPrettyPrinting().create();
								gson.toJson(serviceDescription, writer);
							}
							numberOfServices++;
							System.out.println("Successfully written service description file: " + newName);
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
				}
				catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		System.out.println("Number of Services: " + numberOfServices);
	}
	
	@SuppressWarnings("unchecked")
	public static void generateIoTServices(){
		int complete = 0;
		int incomplete = 0;
		int moreData = 0;
		int lines = 0;
	    try {
	    	BufferedReader br = new BufferedReader(new FileReader("../../Data/right-place-dataset/services.txt"));
			String line = br.readLine();
			while (line != null) {
		    	lines ++;
		    	String [] parts = line.split(";");
		    	if(parts.length==5){
		    		JSONObject serviceDescription = new JSONObject();
		    		String name = parts[0];
		    		String url = "http://surf.scss.tcd.ie/_provider_/"+name.toLowerCase();
		    		String desc = "This is the service " + name.toLowerCase();
		    		String id = lines + "/" + name;
		    		serviceDescription.put("id", id);
		    		serviceDescription.put("name", name);
		    		serviceDescription.put("url", url);
					serviceDescription.put("description", desc);
					JSONObject provider = new JSONObject();
					provider.put("type", parts[4]);
					serviceDescription.put("provider", provider);
					serviceDescription.put("state", true);
					
					String domains = parts[1];
		    		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
					FileInputStream inputStream;
					inputStream = new FileInputStream("../../Data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
					m.read(inputStream,null);
					inputStream.close();
					
					OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
					ExtendedIterator<OntResource> inds = (ExtendedIterator<OntResource>) domain.listInstances();
					List<OntResource> listDomains = inds.toList();
					
					JSONArray jsonDomains = new JSONArray();
					if(domains.contains(",")){
		    			String [] ds = domains.split(",");
		    			for(int i=0; i<ds.length;i++){
		    				for(int j=0;j<listDomains.size();j++){
			    				if(ds[i].equals(listDomains.get(j).getLocalName())){
			    					JSONObject d = new JSONObject();
			    					d.put("name", listDomains.get(j).getLocalName());
			    					d.put("url", listDomains.get(j).getURI());
			    					jsonDomains.add(d);
			    				}
			    			}
		    			}
		    		}
		    		else{
		    			for(int j=0;j<listDomains.size();j++){
		    				if(domains.equals(listDomains.get(j).getLocalName())){
		    					JSONObject d = new JSONObject();
		    					d.put("name", listDomains.get(j).getLocalName());
		    					d.put("url", listDomains.get(j).getURI());
		    					jsonDomains.add(d);
		    				}
		    			}
		    		}
		    		serviceDescription.put("domains", jsonDomains);
		    		
		    		String inputs = parts[2];
		    		JSONArray jsonInputs = new JSONArray(); 
		    		if(inputs.contains(",")){
		    			String [] ins = inputs.split(",");
		    			for(int i=0;i<ins.length;i++){
		    				if(ins[i].contains("-")){
		    					String[] input = ins[i].split("-");
		    					String inputName=input[0];
		    					String inputType=input[1];
		    					JSONObject jsonInput = new JSONObject();
		    					jsonInput.put("name", inputName);
		    					jsonInput.put("type", inputType);
		    					jsonInputs.add(jsonInput);
		    				}
		    				else{
		    					System.out.println("Wrong inputs:" + line);
		    				}
		    			}
		    		}
		    		else{
		    			if(inputs.contains("-")){
		    				String[] input = inputs.split("-");
		    				String inputName=input[0];
	    					String inputType=input[1];
	    					JSONObject jsonInput = new JSONObject();
	    					jsonInput.put("name", inputName);
	    					jsonInput.put("type", inputType);
	    					jsonInputs.add(jsonInput);
		    			}
		    			else{
		    				System.out.println("Wrong inputs:" + line);
		    			}
		    		}
		    		serviceDescription.put("inputs", jsonInputs);
		    		
		    		String outputs = parts[3];
		    		JSONArray jsonOutputs = new JSONArray();
		    		if(outputs.contains(",")){
		    			String [] outs = outputs.split(",");
		    			for(int i=0;i<outs.length;i++){
		    				if(outs[i].contains("-")){
		    					String[] output = outs[i].split("-");
		    					String outputName=output[0];
		    					String outputType=output[1];
		    					JSONObject jsonOutput = new JSONObject();
		    					jsonOutput.put("name", outputName);
		    					jsonOutput.put("type", outputType);
		    					jsonOutputs.add(jsonOutput);
		    				}
		    				else{
		    					System.out.println("Wrong outputs:" + line);
		    				}
		    			}
		    		}
		    		else{
		    			if(outputs.contains("-")){
		    				String[] output = outputs.split("-");
	    					String outputName=output[0];
	    					String outputType=output[1];
	    					JSONObject jsonOutput = new JSONObject();
	    					jsonOutput.put("name", outputName);
	    					jsonOutput.put("type", outputType);
	    					jsonOutputs.add(jsonOutput);
		    			}
		    			else{
		    				System.out.println("Wrong outputs:" + line);
		    			}
		    		}
		    		serviceDescription.put("outputs", jsonOutputs);
		    		
		    		System.out.println("Writing file");
			    	try{
			    		String newName = lines+"_"+(String) serviceDescription.get("name") + ".json";
			    		FileWriter f = new FileWriter("../../Data/right-place-dataset/experiments-dataset/IoT-services/"+newName);
						try (Writer writer = f) {
							Gson gson = new GsonBuilder().setPrettyPrinting().create();
							gson.toJson(serviceDescription , writer);
						}
						System.out.println("Successfully written service description file: " + newName);
					}catch(Exception ex){
						ex.printStackTrace();
					}
		    		complete ++;
		    	}else if(parts.length<4){
		            	System.out.println("Incomplete:" + line);
		            	incomplete ++;
		        }else if(parts.length>5){
	            	System.out.println("More:" + line);
	            	moreData++;
		        }
		    	
		    	line = br.readLine();
		    }
		    br.close();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		System.out.println(complete + " complete of " + lines);
		System.out.println(incomplete + " incomplete of " + lines);
		System.out.println(moreData + " have more data of " + lines);
	}
	

	/**
	 * 
	 * Creates the names file that is used to read the service at registration time
	 */
	@SuppressWarnings("unchecked")
	public static void generateFileNames() {
		File folderServices = new File("../../Data/services-dataset/10000-services/");
		File[] files = folderServices.listFiles();
		JSONArray actualNames = new JSONArray();
		JSONArray dummyNames = new JSONArray();
		for (int i = 0; i < files.length; i++) {
			if(files[i].getName().contains("Dummy")) {
				dummyNames.add(files[i].getName());
			}else {
				actualNames.add(files[i].getName());
			}
		}
		JSONObject allNames = new JSONObject();
		try {
			FileWriter f = new FileWriter("../../Data/services-dataset/dummy-services-names.json");
			allNames.put("names", dummyNames);
			try (Writer writer = f) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(allNames, writer);
			}
			f = new FileWriter("../../Data/services-dataset/iot-services-names.json");
			allNames = new JSONObject();
			allNames.put("names", actualNames);
			try (Writer writer = f) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(allNames, writer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		File folderRequests = new File("../../Data/requests-dataset/json-requests/");
		File[] folders = folderRequests.listFiles();
		JSONArray requestsNames = new JSONArray();
		JSONParser parser = new JSONParser();
		for(int i=0; i<folders.length; i++) {
			files = folders[i].listFiles();
			for(int j=0;j<files.length;j++) {
				try{
					File file = files[j];
					String f = folders[i].getName()+"/"+file.getName();
					Object obj = parser.parse(new FileReader(file.getAbsolutePath()));
					JSONObject jsonObject = (JSONObject) obj; 
					JSONArray domains = (JSONArray) jsonObject.get("domains");
					String d = "";
					for(int k=0; k<domains.size();k++){
						JSONObject domain = (JSONObject) domains.get(k);
						d = d + "," + domain.get("url");
					}
					f = f + "," + d;
					requestsNames.add(f);
				}catch(Exception ex){
					
				}
			}
		}
		
		allNames = new JSONObject();
		try {
			FileWriter f = new FileWriter("../../Data/requests-dataset/requests-names.json");
			allNames.put("names", requestsNames);
			try (Writer writer = f) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(allNames, writer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * Creates the services in our dataset of 10000 services
	 */
	public static void generateDummyTemplates() {

		File folderDummy = new File("../../Data/right-place-dataset/experiments-dataset/json-dummy-services");

		File[] files = folderDummy.listFiles();
		JSONParser parser = new JSONParser();
		for (int i = 0; i < 8918; i++) {
			try {
				int x = (int) (Math.random() * 300);
				Object obj = parser.parse(new FileReader(files[x].getAbsolutePath()));
				JSONObject jsonObject = (JSONObject) obj;
				FileWriter f = new FileWriter("../../Data/services-dataset/10000-services/"+ "Dummy_"+i+"_"+ files[x].getName());
				try (Writer writer = f) {
					Gson gson = new GsonBuilder().setPrettyPrinting().create();
					gson.toJson(jsonObject, writer);
					System.out.println("Copied Dummy: " + (i + 1));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * Creates the file names that is used to read the service at registration time
	 */
	@SuppressWarnings("unchecked")
	public static void generateFileNamesDomains() {
		
		File folderServices = new File("../../Data/services-dataset/full-dataset/all");
		File[] files = folderServices.listFiles();
		JSONParser parser = new JSONParser();
		JSONObject allNames = new JSONObject();
		
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		try{
			FileInputStream inputStream;
			inputStream = new FileInputStream("../../Data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
			m.read(inputStream,null);
			inputStream.close();
			OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
			ExtendedIterator<OntClass> ds = domain.listSubClasses();
			List<OntClass> domainsOntology = ds.toList();
			for(int w=0;w<domainsOntology.size();w++){
				ArrayList<String> individualsDomains = new ArrayList<String>();
				ExtendedIterator<OntResource> ins = (ExtendedIterator<OntResource>) domainsOntology.get(w).listInstances();
				List<OntResource> domainInstances = ins.toList();
				for(int h=0;h<domainInstances.size();h++){
					individualsDomains.add(domainInstances.get(h).getURI());
				}
				JSONArray names=new JSONArray();
				OntClass d = domainsOntology.get(w);
				for (int i = 0; i < files.length; i++) {
					Object obj = parser.parse(new FileReader(files[i].getAbsolutePath()));
					JSONObject jsonObject = (JSONObject) obj;
					JSONArray serviceDomains = (JSONArray)jsonObject.get("domains");
					for(int x =0;x<serviceDomains.size();x++){
						JSONObject dss = (JSONObject) serviceDomains.get(x);
						String url = (String) dss.get("url");
						if(individualsDomains.contains(url)){
							names.add(files[i].getName());
							break;
						}
					}
				}
				allNames.put(d.getURI(), names);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		try {
			FileWriter f = new FileWriter("../../Data/services-dataset/iot-services-names-domains.json");
			try (Writer writer = f) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(allNames, writer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** Annotates OWL services and IoT services with the right domain and I/O information
	 * It annotates  1082 services in total
	 * It also generates the ontology text for the IoT services I/O**/
	@SuppressWarnings("unchecked")
	public static void addDomainsAndFixIO() {
		File folder = new File("../../Data/services-dataset/json-services/");
		File[] folders = folder.listFiles();
		int numberOfServices = 0;
		ArrayList<String> concepts = new ArrayList<>();
		for(int i=0;i<folders.length;i++){
			System.out.println("Reading folder for domain: " + folders[i].getName());
			File[] files = folders[i].listFiles();
			System.out.println("Number of Files: " + files.length);
			for(int j=0;j<files.length;j++){
				boolean newDomain=false;
				System.out.println("Reading file: " + files[j].getName());
				File file = files[j];
				JSONObject serviceDescription = new JSONObject();
				JSONParser parser = new JSONParser();
				try {
					Object obj = parser.parse(new FileReader(file));
					serviceDescription = (JSONObject) obj;
					
					String id = folders[i].getName() + "/" + file.getName().replaceAll(".json", "");
					serviceDescription.put("id", id);
					
					String url = "http://surf.scss.tcd.ie/_provider_/" + folders[i].getName() + "/" + file.getName().replaceAll(".json", "");
					serviceDescription.put("url", url);
					
					JSONArray inputs = (JSONArray) serviceDescription.get("inputs");
					JSONArray newInputs = new JSONArray();
					for(int x=0; x<inputs.size();x++) {
						JSONObject input = (JSONObject) inputs.get(x);
						if(!input.containsKey("desc")) {
							String name = (String) input.get("name");
							input.put("type", "http://www.surf.scss.tcd.ie/IoTservices.owl#" + name);
							input.put("desc", "Input of service " + serviceDescription.get("name"));
							input.put("name", "_"+name.toUpperCase());
							if(!concepts.contains(name)) 
								concepts.add(name);
						}
						newInputs.add(input);
					}
					serviceDescription.put("inputs", newInputs);
					
					
					JSONArray outputs = (JSONArray) serviceDescription.get("outputs");
					JSONArray newOutputs = new JSONArray();
					for(int x=0; x<outputs.size();x++) {
						JSONObject output = (JSONObject) outputs.get(x);
						if(!output.containsKey("desc")) {
							String name = (String) output.get("name");
							output.put("type", "http://www.surf.scss.tcd.ie/IoTservices.owl#" + name);
							output.put("desc", "Output of service " + serviceDescription.get("name"));
							output.put("name", "_"+name.toUpperCase());
							if(!concepts.contains(name)) 
								concepts.add(name);
						}
						newOutputs.add(output);
					}
					serviceDescription.put("outputs", newOutputs);
					
					if(!serviceDescription.containsKey("domains")) {
						serviceDescription.put("domains", new JSONArray());
					}
					
					switch (folders[i].getName()){
					case "accommodation":
						JSONArray domains = new JSONArray();
						JSONObject domain = new JSONObject();
						domain.put("name", "Accommodation");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Accommodation");
						domains.add(domain);
						JSONArray domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "activity":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Activity");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Activity");
						domains.add(domain);
						if(id.contains("sport")) {
							domain = new JSONObject();
							domain.put("name", "Sport");
							domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Sport");
							domains.add(domain);
							domain = new JSONObject();
							domain.put("name", "Leisure");
							domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Leisure");
							domains.add(domain);
						}
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "books":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Educational");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Educational");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Entertainment_Products");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Entertainment_Products");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "buildings":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Buildings");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Buildings");
						domains.add(domain);
						if(id.contains("energy")) {
							domain = new JSONObject();
							domain.put("name", "Energy");
							domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Energy");
							domains.add(domain);
						}
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "clothing":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Clothing");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Clothing");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "entertainment":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Entertainment_Products");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Entertainment_Products");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "construction":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Construction");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Construction");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Safety_and_Emergencies");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Safety_and_Emergencies");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "education":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Educational");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Educational");
						domains.add(domain);
						if(id.contains("research")) {
							domain = new JSONObject();
							domain.put("name", "Research_and_Science");
							domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Research_and_Science");
							domains.add(domain);
						}
						if(id.contains("government") || id.contains("Gov")) {
							domain = new JSONObject();
							domain.put("name", "Government_and_Planning");
							domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Government_and_Planning");
							domains.add(domain);
						}
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "energy":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Energy");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Energy");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "finance":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Finance");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Finance");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Safety_and_Emergencies");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Safety_and_Emergencies");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "food":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Food_and_Drinks");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Food_and_Drinks");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "furniture":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Furniture_and_Housing");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Furniture_and_Housing");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "defense":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Defense");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Defense");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x=0;x<domainsArray.size();x++) {
							if(((JSONObject)domainsArray.get(x)).get("name").equals("Educational")) {
								domainsArray.remove(x);
							}
							if(((JSONObject)domainsArray.get(x)).get("name").equals("Government_and_Planning")) {
								domainsArray.remove(x);
							}
						}
						serviceDescription.put("domains", domainsArray);
						domainsArray = (JSONArray) serviceDescription.get("domains");
						
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "governmentandplanning":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Government_and_Planning");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Government_and_Planning");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x=0;x<domainsArray.size();x++) {
							if(((JSONObject)domainsArray.get(x)).get("name").equals("Educational")) {
								domainsArray.remove(x);
							}
						}
						serviceDescription.put("domains", domainsArray);
						domainsArray = (JSONArray) serviceDescription.get("domains");
						
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "health":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Health");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Health");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Safety_and_Emergencies");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Safety_and_Emergencies");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "industry":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Industry");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Industry");
						domains.add(domain);
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x=0;x<domainsArray.size();x++) {
							if(((JSONObject)domainsArray.get(x)).get("name").equals("Educational")) {
								domainsArray.remove(x);
							}
						}
						serviceDescription.put("domains", domainsArray);
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "logistic":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Logistic");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Logistic");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Communications");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Communications");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "medical":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Health");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Health");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "nature":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Nature");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Nature");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "payment":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Payment_and_Delivery");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Payment_and_Delivery");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "researchandscience":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Educational");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Educational");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Research_and_Science");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Research_and_Science");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "safetyandemergencies":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Safety_and_Emergencies");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Safety_and_Emergencies");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "sport":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Sport");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Sport");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Leisure");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Leisure");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Activity");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Activity");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "technology":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Technology");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Technology");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "tools":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Tools");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Tools");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "touristicplace":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Touristic_Place");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Touristic_Place");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "transportandmobility":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Transport_and_Mobility");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Transport_and_Mobility");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "travel":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Travel");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Travel");
						domains.add(domain);
						domain = new JSONObject();
						domain.put("name", "Geography");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Geography");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "vehicle":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Vehicle_Sale_and_Rent");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Vehicle_Sale_and_Rent");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "waste":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Waste");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Waste");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					case "weather":
						domains = new JSONArray();
						domain = new JSONObject();
						domain.put("name", "Weather");
						domain.put("url", "http://www.surf.scss.tcd.ie/cityOntology/#Weather");
						domains.add(domain);
						
						domainsArray = (JSONArray) serviceDescription.get("domains");
						for(int x = 0; x<domains.size();x++) {
							boolean add = true;
							domain = (JSONObject) domains.get(x);
							for(int da = 0; da<domainsArray.size();da++) {
								JSONObject domainArray = (JSONObject) domainsArray.get(da);
								if(domain.get("name").equals(domainArray.get("name"))){
									add = false;
									break;
								}
							}
							if(add) {
								newDomain = true;
								domainsArray.add(domain);
							}
						}
						serviceDescription.put("domains", domainsArray);
					break;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try{
					String newName = files[j].getName();
					System.out.println("Saving service: " + newName);
					if(newDomain)
						System.out.println("Modified domains");
					else
						System.out.println("Not Modified domains");
					File fl = new File("../../Data/services-dataset/full-dataset/"+folders[i].getName()+"/"+newName);
					fl.getParentFile().mkdirs();
					FileWriter f = new FileWriter(fl.getAbsoluteFile());
					try (Writer writer = f) {
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
						gson.toJson(serviceDescription, writer);
					}
					File fl2 = new File("../../Data/services-dataset/full-dataset/all/"+newName);
					fl2.getParentFile().mkdirs();
					FileWriter f2 = new FileWriter(fl2.getAbsoluteFile());
					try (Writer writer = f2) {
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
						gson.toJson(serviceDescription, writer);
					}
					numberOfServices++;
					System.out.println("Successfully written service description file: " + newName);
				}catch(Exception ex){
					ex.printStackTrace();
				}
				
			}
		}
		System.out.println("Number of Services: " + numberOfServices);
		System.out.println("New Concepts for IoTServices ontology");
		
		for(int i=0;i<concepts.size();i++) {
			System.out.println("<Declaration>");
			System.out.println("<Class IRI=\""+concepts.get(i)+"\"/>");
			System.out.println("</Declaration>");
			
			System.out.println("<SubClassOf>");
			System.out.println("<Class IRI=\""+concepts.get(i)+"\"/>");
			System.out.println("<Class IRI=\"ServiceParameter\"/>");
			
			System.out.println("</SubClassOf>");
		}
	}
}