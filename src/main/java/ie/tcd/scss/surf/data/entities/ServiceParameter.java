/**
 * Author: Christian Cabrera
 * This entity represents a service parameter (i.e., input or output) in the service description
 */

package ie.tcd.scss.surf.data.entities;

public class ServiceParameter {
	private String name;
	private String type;
	private String description;
	private String unit;
	private String value;

	public ServiceParameter() {
		this.setName("");
		this.setType("");
		this.setDescription("");
		this.setUnit("");
		this.setValue("");
	}

	public ServiceParameter(String name, String type, String description, String unit, String value) {
		this.setName(name);
		this.setType(type);
		this.setDescription(description);
		this.setUnit(unit);
		this.setValue(value);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
