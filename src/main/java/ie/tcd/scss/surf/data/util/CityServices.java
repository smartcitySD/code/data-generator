/*package ie.tcd.scss.surf.data.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.omg.CORBA.portable.InputStream;

import ie.tcd.scss.surf.simonstrator.entities.CityService;

public class CityServices {
	private static ArrayList<CityService> cityServices;

	public CityServices() {
		cityServices = new ArrayList();
		loadCityServices();
	}

	public static ArrayList<CityService> getCityServices() {
		return cityServices;
	}

	public static void setCityServices(ArrayList<CityService> cityServices) {
		CityServices.cityServices = cityServices;
	}

	public void loadCityServices() {
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		try {
			FileInputStream inputStream;
			inputStream = new FileInputStream("../../SmartCitySD/Data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
			m.read(inputStream,null);
			inputStream.close();
			OntClass cityService = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#CityService");
			ExtendedIterator<OntResource> is = (ExtendedIterator<OntResource>) cityService.listInstances(true);
			List<OntResource> instances = is.toList();
			if (instances.size() > 0) {
				for (int i = 0; i < instances.size(); i++) {
					if (instances.get(i).asIndividual().getOntClass().getURI()
							.equals("http://www.surf.scss.tcd.ie/cityOntology/#CityService")) {
						CityService cs = new CityService();
						cs.setUri(instances.get(i).getURI());
						StmtIterator statIt = instances.get(i).listProperties();
						List<Statement> statements = statIt.toList();
						for (int z = 0; z < statements.size(); z++) {
							Statement statement = statements.get(z);
							if (statement.getPredicate().getLocalName().equals("hasDomain")) {
								cs.getDomains().add(statement.getResource().getURI());

							}
						}
						cs.loadSuperDomains();
						cityServices.add(cs);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}*/
