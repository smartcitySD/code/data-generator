package ie.tcd.scss.surf.data.entities;

import org.json.simple.JSONObject;

public class Edge {
	
	private Vertex source;
	private Vertex destination;
	private JSONObject link;
	private double degree;
	
	public Edge() {
		this.setSource(new Vertex());
		this.setDestination(new Vertex());
		this.setLink(new JSONObject());
		this.setDegree(0);
	}
	
	public Edge(Vertex source, Vertex destination, JSONObject link, double degree) {
		this.setSource(source);
		this.setDestination(destination);
		this.setLink(link);
		this.setDegree(degree);
	}

	public Vertex getSource() {
		return source;
	}

	public void setSource(Vertex source) {
		this.source = source;
	}

	public Vertex getDestination() {
		return destination;
	}

	public void setDestination(Vertex destination) {
		this.destination = destination;
	}

	public JSONObject getLink() {
		return link;
	}

	public void setLink(JSONObject link) {
		this.link = link;
	}

	public double getDegree() {
		return degree;
	}

	public void setDegree(double degree) {
		this.degree = degree;
	}
}
